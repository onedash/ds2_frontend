package edu.utah.cs.gauss.ds2.core
import org.scalatest._
import prop._

/**
 * @author <br>
 *	Mohammed S. Al-Mahfoudh <br/>
 *	mahfoudh@cs.utah.edu <br/>
 *	SoC - Gauss Group <br/>
 * This is the test spec used in all the tests. They basically specify what scalatest styles we use.
 */
trait MyTestSpecs extends FunSuite with PropertyChecks with Matchers{
  import Matchers._

}

trait MySimpleSpecs extends FunSuite with Matchers {
  import Matchers._
}
package edu.utah.cs.gauss.ds2.core.ir.datastructures

import edu.utah.cs.gauss.ds2.core.MyTestSpecs
import edu.utah.cs.gauss.ds2.core.ir.datastructures.Statement.Kind._
import edu.utah.cs.gauss.ds2.core.ir.datastructures.Fixtures._
import org.scalatest.Ignore
import edu.utah.cs.gauss.ds2.core.schedulers.algorithms.BasicScheduler
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler
import java.util.concurrent.ScheduledExecutorService

class StatementInstrumentation extends MyTestSpecs {
  info("==================================")
  info("Statement Instrumentation tests")
  info("==================================")

  test("SEND") {
    val ds = distributedSystemWithSchedulerInstance
    ds.unlock(ds.get("server"))

    //    val src = agentInstance
    //    val dst = agentInstance

    val s = Statement(SEND, ds.get("client"), messageInstance, ds.get("server"))
    s.link(ds)
    assert(null != s.srcAgent, "srcAgent is NULL!")
    assert(null != s.dstAgent, "dstAgent is NULL!")
    assert(null != s.msgOut, "msgOut is NULL!")

    s.apply

    assert(ds.get("server").q.size == 1, "Server didn't receive the message!")

  }


  test("ASK") {
    val ds = distributedSystemWithSchedulerInstance
    ds.unlock(ds.get("server"))

    //    val src = agentInstance
    //    val dst = agentInstance

    var s = Statement(ASK, ds.get("client"), messageInstance, ds.get("server"))
    s.link(ds)
    assert(null != s.srcAgent, "srcAgent is NULL!")
    assert(null != s.dstAgent, "dstAgent is NULL!")
    assert(null != s.msgOut, "msgOut is NULL!")


    s.apply

    assert(null != s.future && s.future.isInstanceOf[DummyFuture], "There isn't future returned!")

    assert(ds.get("server").q.size == 1, "Server didn't receive the message!")
    //    assert(s_pre._2.get.promisedBy.name == "server","How come the server didn't promise a thing!" )
    //    assert(s_pre._2.get.waitingFor.name ==  "client","How come the client wasn't promised a thing!" )

  }
  
  test("DistributedSystem - SEND"){
    val ds = new DistributedSystem("Test")
    ds.scheduler = Scheduler(ds, Scheduler.Basic)
    
    var sender = new Agent("dude1")
    var receiver = new Agent("dude2")

    val st = Statement(SEND,sender,new Start,receiver)
    
    val act = new Action
    act.stmts = act.stmts :+ st
    
    sender.specialReactions + (new Start,st)
//    sender.specialReactions  += (new Start, act)

    println("BEFORE picking: "+  sender.specialReactions(new Start).stmts.size)
    
    
    ds + receiver + sender // add them to the distributed system
    ds.refresh
    
    // hacking them to make it short
    receiver.locked = false
    sender.locked = false
    ds.bootStrap(sender) // invokes the start action  
    println("=====================================================")
    println(sender)
    println(receiver)
    println(ds.scheduler)
    println("=====================================================")
    ds.scheduler.schedule(ds.scheduler.pick(sender))
    println("=====================================================")
    println(sender)
    println(receiver)
    println(ds.scheduler)
    println("=====================================================")
    
    ds.scheduler.consume
    
    println("=====================================================")
    println(sender)
    println(receiver)
    println(ds.scheduler)
    println("=====================================================")
    
    ds.scheduler.execute(1)
    
    println("=====================================================")
    println(sender)
    println(receiver)
    println(ds.scheduler)
    println("=====================================================")
    
    
    
    println("first: "+receiver.q.size)
    
    receiver.q.size should be (1) // should pass
    
    
    // =======================
    // SECOND PART (copying)
    // =======================    
    val schState = ds.scheduler.copyState
    ds.scheduler.restoreStateTo(schState)
    val ds2 = schState.ds
    
    sender = ds2.get("dude1")
    receiver = ds2.get("dude2")
    
    ds2.bootStrap(sender) // invokes the start action
    
    ds.scheduler.schedule(ds.scheduler.pick(sender))
    ds.scheduler.consume
    ds.scheduler.execute(1)
    
    println("Second: "+receiver.q.size)
    
    receiver.q.size should be (2) // should pass
  }

}


package edu.utah.cs.gauss.ds2.core.integration

import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler
import java.util.concurrent.ScheduledExecutorService
import edu.utah.cs.gauss.ds2.core.ir.datastructures.Statement.Kind._
/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 */
object TestBeds {
  /*
   * for example:
   * 1- Echo Server and Client interacting with SEND
   * 2- Echo Server and Client but interacting with ASK
   * 
   *  Try to add more examples.
   */

  def echoServerInstance: DistributedSystem = {

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    //    ds.enableTracing
    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)
    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    //    //------------------------------
    //    // Overriding the start action 
    //    // of both Server and Client
    //    //------------------------------
    //
    //    // the start action shared by both server and client (unlocking by default for starting)
    //
    //    val startAction = new Action
    //    val code = (m: Message, a: Agent) => {
    //      ds.unlock(a)
    //    }
    //    val startStmt = Statement(code)
    //    startAction.stmts = Seq(startStmt)
    //
    //    c.specialReactions += (new Start -> startAction.copy)
    //    s.specialReactions += (new Start -> startAction.copy)
    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode = (m: Message, a: Agent) => {
      println("--------------------------OUTPUT---------------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------OUTPUT---------------------------")
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction + stmtErr

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //    // NEVER EVER forget to refresh the Agent before you use it, strange things can and will happen.
    //    s.refresh

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg
    val code2 = (m: Message, a: Agent) => { ds.send(a, clientMsg, s) }
    val cAction = new Action
    cAction + Statement(code2)

    c.specialReactions += (new Start -> cAction)

    //    c.refresh

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh
    ds
  }

  def echoServerInstanceCopy: DistributedSystem = {
    // this test-bed is primarily to make sure the two-phase 
    // copy-link process works as expected

    import Statement.Kind._

    // creating a Distributed System
    var ds = new DistributedSystem("echo")
    //    ds.enableTracing
    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)
    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    //    //------------------------------
    //    // Overriding the start action 
    //    // of both Server and Client
    //    //------------------------------
    //
    //    // the start action shared by both server and client (unlocking by default for starting)
    //
    //    val startAction = new Action
    //    val code = (m: Message, a: Agent) => {
    //      ds.unlock(a)
    //    }
    //    val startStmt = Statement(code)
    //    startAction.stmts = Seq(startStmt)
    //
    //    c.specialReactions += (new Start -> startAction.copy)
    //    s.specialReactions += (new Start -> startAction.copy)
    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode = (m: Message, a: Agent) => {
      println("--------------------------OUTPUT---------------------------")
      println(m.sender.name +
        " sent a request to print: "
        + m.payload.mkString)
      println("--------------------------OUTPUT---------------------------")
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction + stmtErr

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //    // NEVER EVER forget to refresh the Agent before you use it, strange things can and will happen.
    //    s.refresh

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg
    // non-instrumented way of doing it
    //    val code2 = (m: Message, a: Agent) => { ds.send(a, clientMsg, s) }
    val cAction = new Action

    // non instrumented
    //    cAction + Statement(code2)

    // instrumented!
    cAction + Statement(SEND, c, clientMsg, s)

    c.specialReactions += (new Start, cAction)

    //    c.refresh

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh

    // only following changed from original test-bed

    //    val newDS = ds.copy // this copy is to be disposed
    //    val sched = ds.scheduler // because this scheduler will be used to
    val state = ds.scheduler.copyState // create a new copy of the distributed system by the SchedulerState.copy
    ds.scheduler.restoreStateTo(state) // then we will restore AND link both DS first, then state next
    ds.scheduler.ds // return the NEW COPY of the DS
  }

  def echoServerWithAskInstance: DistributedSystem = {

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    //    ds.enableTracing

    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)

    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    // i just needed to initialize it anyways ... good question:
    // why would I make this thing global if it can live inside 
    // the agent's state? Ans: because I don't have to cast it back from
    // Any to DummyFuture. (in case we need to investigate the internal
    // localState of the agent to do additional analysis, then it is 
    // better to add it inside the agent).
    var future: DummyFuture = new DummyFuture(false, 0, s, c)

    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode1 = (m: Message, a: Agent) => {
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      // now let the client, IN ANOTHER STATEMENT to support unblocking through another scheduler.tick
    }

    val sCode2 = (m: Message, a: Agent) => {
      ds.resolve(future, true)
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode1) + Statement(sCode2)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction.stmts = Seq(stmtErr)

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg

    val ccode1 = (m: Message, a: Agent) => {
      future = ds.ask(a, clientMsg, s)
      // i can put c instead of a, a will be set anyways to the containing agent which is c in this case.
    }
    val ccode2 = (m: Message, a: Agent) => {
      // blocking get (not timing out).
      ds.get(c, future, c.specialReactions(new Start)) // note we didn't store it to a local variable!
      // one question: then how can we say someVar = f.value as it may happen in code we parse?
      // ans: add one more entry to local state with (someVar.toString -> localState(f.id.toString)).

      // note the future when gotten (of course if it is resolved first), it gets stored 
      // in the agent's localState

      /*
       * NOTE:
       * 1- the get(future) (or timing out variation of it) need to be last to achieve block/resume of the action.
       * 2- why do we need the "cAction" in the get method?
       *    Ans: because we need to know in which action this get-call is happening, again to support block and resume
       *    of the action.
       */

    }

    val ccode3 = (m: Message, a: Agent) => {
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
      // now we resume (also we need that future again!) -- you see how we saved a lot of coding by making it global "future".
      a.localState(future.id.toString) match {
        case true => println("GREAT!!! I got a reply: my message was shown to the world")
        case _ => println("SAD!!! I will stay alone: no one saw my message!")
      }
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
    }

    val action = c.specialReactions(new Start)

    action + Statement(ccode1) + Statement(ccode2) + Statement(ccode3)

    c.specialReactions += (new Start -> action)

    /*
     * did you notice yet that c doesn't have a default or current reactions? this is intentional
     * but when you do it for real distributed systems, this is semantic error. Every one should 
     * have a brain :)
     */

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh
    ds
  }

  def echoServerWithAskInstanceCopy: DistributedSystem = {
    // this test-bed is primarily to make sure the two-phase 
    // copy-link process works as expected

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    //    ds.enableTracing

    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)

    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    // i just needed to initialize it anyways ... good question:
    // why would I make this thing global if it can live inside 
    // the agent's state? Ans: because I don't have to cast it back from
    // Any to DummyFuture. (in case we need to investigate the internal
    // localState of the agent to do additional analysis, then it is 
    // better to add it inside the agent).
//    var future: DummyFuture = new DummyFuture(false, 0, s, c)

    val future = "future[]DummyFuture"
    
    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode1 = (m: Message, a: Agent) => {
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      // now let the client, IN ANOTHER STATEMENT to support unblocking through another scheduler.tick
    }

    //    val sCode2 = (m: Message, a: Agent) => {
    //      ds.resolve(future, true)
    //    }

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg
    val stmt1 = Statement(ASK, c, clientMsg, s, future)
    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    //    sAction + Statement(sCode1) + Statement(sCode2)
//    sAction + Statement(sCode1) + Statement(future, true) // explicit resolve, old
    // sAction + Statement(sCode1) + Statement(RESOLVE,future, true) // explicit resolve, old
    Statement.apply(SEND, s, new Message(future), c)


    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction.stmts = Seq(stmtErr)

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions


    
    val stmt2 = Statement(GET, c, future) // a GET statement, unique signature so no need for the GET tag
    //    val stmt2 = Statement(c, stmt1.future)

    val ccode3 = (m: Message, a: Agent) => {
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
      // now we resume (also we need that future again!) -- you see how we saved a lot of coding by making it global "future".
      a.getVariable(future).asInstanceOf[DummyFuture].value match {
        case true => println("GREAT!!! I got a reply: my message was shown to the world")
        case _ => println("SAD!!! I will stay alone: no one saw my message!")
      }
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
    }

    val action = c.specialReactions(new Start)

    //    action + Statement(ccode1) + Statement(ccode2) + Statement(ccode3)
    action + stmt1 + stmt2 + Statement(ccode3)

    c.specialReactions += (new Start -> action)

    /*
     * did you notice yet that c doesn't have a default or current reactions? this is intentional
     * but when you do it for real distributed systems, this is semantic error. Every one should 
     * have a brain :)
     */

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh

    // This is the copying part
    val sh = ds.scheduler // because this scheduler will be used to
    val state = sh.copyState // create a new copy of the distributed system by the SchedulerState.copy
    sh.restoreStateTo(state) // then we will restore AND link both DS first, then state next
    sh.ds // return the NEW COPY of the DS
  }

  def echoServerWithAskInstanceWithTimedGet: DistributedSystem = {

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    //    ds.enableTracing

    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)

    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    // i just needed to initialize it anyways ... good question:
    // why would I make this thing global if it can live inside 
    // the agent's state? Ans: because I don't have to cast it back from
    // Any to DummyFuture. (in case we need to investigate the internal
    // localState of the agent to do additional analysis, then it is 
    // better to add it inside the agent).
    var future: DummyFuture = new DummyFuture(false, 0, s, c)

    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode1 = (m: Message, a: Agent) => {
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      // now let the client, IN ANOTHER STATEMENT to support unblocking through another scheduler.tick
    }

    val sCode2 = (m: Message, a: Agent) => {
      // do not resolve, make it time out

      //      ds.resolve(future, true)
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode1) + Statement(sCode2)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction.stmts = Seq(stmtErr)

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg

    val ccode1 = (m: Message, a: Agent) => {
      future = ds.ask(a, clientMsg, s)
      // i can put c instead of a, a will be set anyways to the containing agent which is c in this case.
    }
    val ccode2 = (m: Message, a: Agent) => {
      // blocking get (not timing out).
      //      ds.get(c, future, c.specialReactions(new Start)) // note we didn't store it to a local variable!

      ds.get(c, future, 10, c.specialReactions(new Start))

      // one question: then how can we say someVar = f.value as it may happen in code we parse?
      // ans: add one more entry to local state with (someVar.toString -> localState(f.id.toString)).

      // note the future when gotten (of course if it is resolved first), it gets stored 
      // in the agent's localState

      /*
       * NOTE:
       * 1- the get(future) (or timing out variation of it) need to be last to achieve block/resume of the action.
       * 2- why do we need the "cAction" in the get method?
       *    Ans: because we need to know in which action this get-call is happening, again to support block and resume
       *    of the action.
       */

    }

    val ccode3 = (m: Message, a: Agent) => {
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
      // now we resume (also we need that future again!) -- you see how we saved a lot of coding by making it global "future".
      a.localState(future.id.toString) match {
        case true => println("GREAT!!! I got a reply: my message was shown to the world")
        case _ => println("SAD!!! I will stay alone: no one saw/replied-to my message!")
      }
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
    }

    val action = c.specialReactions(new Start)

    action + Statement(ccode1) + Statement(ccode2) + Statement(ccode3)

    c.specialReactions += (new Start -> action)

    /*
     * did you notice yet that c doesn't have a default or current reactions? this is intentional
     * but when you do it for real distributed systems, this is semantic error. Every one should 
     * have a brain :)
     */

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh
    ds

  }

  def echoServerWithAskInstanceWithTimedGetCopy: DistributedSystem = {

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    //    ds.enableTracing

    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)

    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    // i just needed to initialize it anyways ... good question:
    // why would I make this thing global if it can live inside 
    // the agent's state? Ans: because I don't have to cast it back from
    // Any to DummyFuture. (in case we need to investigate the internal
    // localState of the agent to do additional analysis, then it is 
    // better to add it inside the agent).
//    var future: DummyFuture = new DummyFuture(false, 0, s, c)

    val future = "future[]DummyFuture"
    
    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode1 = (m: Message, a: Agent) => {
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      // now let the client, IN ANOTHER STATEMENT to support unblocking through another scheduler.tick
    }

    val sCode2 = (m: Message, a: Agent) => {
      // do not resolve, make it time out

      //      ds.resolve(future, true)
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode1) + Statement(sCode2)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction.stmts = Seq(stmtErr)

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg

    //    val ccode1 = (m: Message, a: Agent) => {
    //      future = ds.ask(a, clientMsg, s)
    //      // i can put c instead of a, a will be set anyways to the containing agent which is c in this case.
    //    }
    //    
    val ccode1 = Statement(ASK, c, clientMsg, s, future)
    //    future = ccode1.future

    //    val ccode2 = (m: Message, a: Agent) => {
    //      // blocking get (not timing out).
    //      //      ds.get(c, future, c.specialReactions(new Start)) // note we didn't store it to a local variable!
    //
    //      ds.get(c, future, 10, c.specialReactions(new Start))
    //
    //      // one question: then how can we say someVar = f.value as it may happen in code we parse?
    //      // ans: add one more entry to local state with (someVar.toString -> localState(f.id.toString)).
    //
    //      // note the future when gotten (of course if it is resolved first), it gets stored 
    //      // in the agent's localState
    //
    //      /*
    //       * NOTE:
    //       * 1- the get(future) (or timing out variation of it) need to be last to achieve block/resume of the action.
    //       * 2- why do we need the "cAction" in the get method?
    //       *    Ans: because we need to know in which action this get-call is happening, again to support block and resume
    //       *    of the action.
    //       */
    //
    //    }
    //    
    val ccode2 = Statement(c, future, 10)

    val ccode3 = (m: Message, a: Agent) => {
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
      // now we resume (also we need that future again!) -- you see how we saved a lot of coding by making it global "future".
      a.getVariable(future).asInstanceOf[DummyFuture].value match {
        case true => println("GREAT!!! I got a reply: my message was shown to the world")
        case _ => println("SAD!!! I will stay alone: no one saw/replied-to my message!")
      }
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
    }

    val action = c.specialReactions(new Start)

    action + ccode1 + ccode2 + Statement(ccode3)

    c.specialReactions += (new Start -> action)

    /*
     * did you notice yet that c doesn't have a default or current reactions? this is intentional
     * but when you do it for real distributed systems, this is semantic error. Every one should 
     * have a brain :)
     */

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh

    val sh = ds.scheduler // because this scheduler will be used to
    val state = sh.copyState // create a new copy of the distributed system by the SchedulerState.copy
    sh.restoreStateTo(state) // then we will restore AND link both DS first, then state next
    sh.ds // return the NEW COPY of the DS
  }

  def echoServerInstanceWithTracing: DistributedSystem = {

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    ds.enableTracing
    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)
    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    //    //------------------------------
    //    // Overriding the start action 
    //    // of both Server and Client
    //    //------------------------------
    //
    //    // the start action shared by both server and client (unlocking by default for starting)
    //
    //    val startAction = new Action
    //    val code = (m: Message, a: Agent) => {
    //      ds.unlock(a)
    //    }
    //    val startStmt = Statement(code)
    //    startAction.stmts = Seq(startStmt)
    //
    //    c.specialReactions += (new Start -> startAction.copy)
    //    s.specialReactions += (new Start -> startAction.copy)
    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode = (m: Message, a: Agent) => {
      println("--------------------------OUTPUT---------------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------OUTPUT---------------------------")
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction + stmtErr

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //    // NEVER EVER forget to refresh the Agent before you use it, strange things can and will happen.
    //    s.refresh

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg
    val code2 = (m: Message, a: Agent) => { ds.send(a, clientMsg, s) }
    val cAction = new Action
    cAction + Statement(code2)

    c.specialReactions += (new Start, cAction)

    //    c.refresh

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh
    ds
  }

  def echoServerInstanceWithTracingCopy: DistributedSystem = {
    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    ds.enableTracing
    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.Basic)
    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    //    //------------------------------
    //    // Overriding the start action 
    //    // of both Server and Client
    //    //------------------------------
    //
    //    // the start action shared by both server and client (unlocking by default for starting)
    //
    //    val startAction = new Action
    //    val code = (m: Message, a: Agent) => {
    //      ds.unlock(a)
    //    }
    //    val startStmt = Statement(code)
    //    startAction.stmts = Seq(startStmt)
    //
    //    c.specialReactions += (new Start -> startAction.copy)
    //    s.specialReactions += (new Start -> startAction.copy)
    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode = (m: Message, a: Agent) => {
      println("--------------------------OUTPUT---------------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------OUTPUT---------------------------")
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction + stmtErr

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //    // NEVER EVER forget to refresh the Agent before you use it, strange things can and will happen.
    //    s.refresh

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg
    //    val code2 = (m: Message, a: Agent) => { ds.send(a, clientMsg, s) }

    val cAction = new Action
    cAction + Statement.apply(SEND, c, clientMsg, s)

    c.specialReactions += (new Start, cAction)

    //    c.refresh

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh
    val sh = ds.scheduler // because this scheduler will be used to
    val state = sh.copyState // create a new copy of the distributed system by the SchedulerState.copy
    sh.restoreStateTo(state) // then we will restore AND link both DS first, then state next
    sh.ds // return the NEW COPY of the DS
  }

  def deadlock1: DistributedSystem = {

    // FIXME change this test bed to reflect which statement is what.

    // creating a Distributed System
    val ds = new DistributedSystem("echo")
    //    ds.enableTracing

    //------------------------------
    // Link a new scheduler to DS
    //------------------------------

    // Select a scheduler, now will default to basic
    // note ds and scheduler both call-back to each other.
    ds.scheduler = Scheduler(ds, Scheduler.DeadlockDetectionScheduler)

    // creating agents and setting their attributes, overrides, ..., etc
    val c = new Agent("client")
    val s = new Agent("server")

    //NOTE: add ActionsToSends manually (as it is part of SA done by the front end).

    // i just needed to initialize it anyways ... good question:
    // why would I make this thing global if it can live inside 
    // the agent's state? Ans: because I don't have to cast it back from
    // Any to DummyFuture. (in case we need to investigate the internal
    // localState of the agent to do additional analysis, then it is 
    // better to add it inside the agent).
    var future: DummyFuture = new DummyFuture(false, 0, s, c)

    //------------------------------
    // SERVER setup
    //------------------------------

    val sCode1 = (m: Message, a: Agent) => {
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      println(m.sender.name + " sent a request to print: " + m.payload.mkString)
      println("--------------------------SERVER-OUTPUT-ASK----------------------")
      // now let the client, IN ANOTHER STATEMENT to support unblocking through another scheduler.tick
    }

    val sCode2 = (m: Message, a: Agent) => {
      // do not resolve, make it time out

      //      ds.resolve(future, true)
    }

    // create an action
    val sAction = new Action
    // add the statement(s) to the action
    sAction + Statement(sCode1) + Statement(sCode2)

    // create another statement
    val stmtErr = Statement(
      (m: Message, a: Agent) => {
        if (m.name != "Show")
          println("Agent(" + a.name + ") sent an unknown request")
      })
    // create another action
    val sErrorAction = new Action
    // add statement(s) to it
    sErrorAction.stmts = Seq(stmtErr)

    val sReaction = (new Message("Show"), sAction)
    val sErrReaction = (new Message, sErrorAction)

    val reactions = new Behavior("default")
    reactions += sReaction
    reactions += sErrReaction

    s.defaultBehavior = reactions
    s.reactions = reactions

    //------------------------------
    // CLIENT setup
    //------------------------------

    val clientMsg = new Message("Show", "Whatever to show")
    // don't forget to define it in the DistributedSystem
    ds.messages += clientMsg

    val ccode1 = (m: Message, a: Agent) => {
      future = ds.ask(a, clientMsg, s)
      // i can put c instead of a, a will be set anyways to the containing agent which is c in this case.
    }
    val ccode2 = (m: Message, a: Agent) => {
      // blocking get (not timing out).
      ds.get(c, future, c.specialReactions(new Start)) // note we didn't store it to a local variable!

      //      ds.get(c, future, 10, c.specialReactions(new Start))

      // one question: then how can we say someVar = f.value as it may happen in code we parse?
      // ans: add one more entry to local state with (someVar.toString -> localState(f.id.toString)).

      // note the future when gotten (of course if it is resolved first), it gets stored 
      // in the agent's localState

      /*
       * NOTE:
       * 1- the get(future) (or timing out variation of it) need to be last to achieve block/resume of the action.
       * 2- why do we need the "cAction" in the get method?
       *    Ans: because we need to know in which action this get-call is happening, again to support block and resume
       *    of the action.
       */

    }

    val ccode3 = (m: Message, a: Agent) => {
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
      // now we resume (also we need that future again!) -- you see how we saved a lot of coding by making it global "future".
      a.localState(future.id.toString) match {
        case true => println("GREAT!!! I got a reply: my message was shown to the world")
        case _ => println("SAD!!! I will stay alone: no one saw/replied-to my message!")
      }
      println("--------------------------CLIENT-OUTPUT-ASK----------------------")
    }

    val action = c.specialReactions(new Start)

    action + Statement(ccode1) + Statement(ccode2) + Statement(ccode3)

    c.specialReactions += (new Start -> action)

    /*
     * did you notice yet that c doesn't have a default or current reactions? this is intentional
     * but when you do it for real distributed systems, this is semantic error. Every one should 
     * have a brain :)
     */

    //------------------------------
    // Add agents to DS
    //------------------------------

    // add the agents to the Distributed system
    ds + s + c

    //------------------------------
    // DS ready for exploration
    //------------------------------

    // returning the distributed system, most likely for a scheduler to explore
    ds.refresh
    ds

  }

}

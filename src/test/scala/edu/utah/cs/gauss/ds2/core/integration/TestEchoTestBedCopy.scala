package edu.utah.cs.gauss.ds2.core.integration

import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import edu.utah.cs.gauss.ds2.core.MyTestSpecs
import org.scalatest.Ignore

/**
 * @author <br>
 * 	Mohammed S. Al-Mahfoudh <br/>
 * 	mahfoudh@cs.utah.edu <br/>
 * 	SoC - Gauss Group <br/>
 */


class TestEchoTestBedCopy extends MyTestSpecs {
  info("""=================================================================""")
  info("""This test suite is an integration test suite. It starts with a 
    fresh instance of a distributed system (an Echo server and client) 
    and ending with testing the whole pipeline of the runtime""")
  info("""=================================================================""")

  val myDS = TestBeds.echoServerInstanceCopy
  
  
  
  test("Boot strapping a server") {
    // starting server, which dows nothing in our test bed since it basically sends s, then j.
    myDS.bootStrap(myDS.get("server"))
    myDS.get("server").q.head should be(new Start)
    myDS.get("server").locked should be(true)
    myDS.get("server").q.head.sender should equal(myDS.bootStraper)
    myDS.get("server").q.head.sendMethod should be(false)
  }

  test("Boot strapping client") {
    // start client, which will send a Show message to server, the server in turn will print it
    myDS.bootStrap(myDS.get("client"))

    myDS.get("client").q.head should be(new Start)
    myDS.get("client").locked should be(true)
    myDS.get("client").q.head.sender should equal(myDS.bootStraper)
    myDS.get("client").q.head.sendMethod should be(false)
  }

  val scheduler = myDS.scheduler

  test("DistributedSystem and Schedulers know each other") {

    scheduler shouldNot be(null)
    scheduler should be(myDS.scheduler)
    myDS.scheduler should be(scheduler)
    scheduler.taskQ should be(empty)
    scheduler.consumeQ should be(empty)
  }

  test("Scheduling all server tasks then all client's tasks") {
    // server has to be started so we schedule it twice (for start and for join)
    scheduler.schedule(scheduler.pick(myDS.get("server")))
    scheduler.taskQ.size should be(1)
    //then schedule anything (in this case it is certainly the client)
    scheduler.schedule(scheduler.pick(myDS.get("client"))) // the start ===> generates another message to server
    scheduler.taskQ.size should be(2) // containing 4 SuspendableTasks
  }

  test("Consuming all tasks results in placing them in the consumeQ") {
    scheduler.consume
    scheduler.consume

    scheduler.taskQ should be(empty) // contains no more tasks
    scheduler.consumeQ.size should be(2) // containing 4 SuspendableTasks
  }

  test("Executing each task") {

    // the server's start
    scheduler.execute(1)
    scheduler.consumeQ.size should be(1)

    // the client's start
    scheduler.execute(1)
    scheduler.consumeQ should be(empty)
  }

  test("Scheduling the last pending task in server sent by client") {
	  myDS.hasWork should be(Set(myDS.get("server")))
	  myDS.get("server").q.head.name should be("Show")
	  myDS.get("server").q.size should be(1)
    scheduler.schedule(scheduler.pick(myDS.get("server")))
    myDS.get("server").q should be(empty)
    scheduler.taskQ.size should be(1)
  }

  test("Consuming and executing the 'Show' task") {
    scheduler.consumeQ should be(empty)
    scheduler.consume
    scheduler.consumeQ.size should be(1)

    scheduler.execute(1)
    scheduler.consumeQ should be(empty)
  }

  // now we don't have a guaranteed-to-terminate DistributedSystem.stop method.
  // soon we will.

  test("Shutting down the system") {
    myDS.isShutdown should be(false)
    myDS.shutdownSystem
    myDS.isShutdown should be(true)
  }
}
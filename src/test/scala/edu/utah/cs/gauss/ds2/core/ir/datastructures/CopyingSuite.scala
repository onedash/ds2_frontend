package edu.utah.cs.gauss.ds2.core.ir.datastructures

import edu.utah.cs.gauss.ds2.core.MyTestSpecs

// to avoid creating ir data structures
import edu.utah.cs.gauss.ds2.core.integration.TestBeds.echoServerInstance

class CopyingSuite extends MyTestSpecs {

  import edu.utah.cs.gauss.ds2.core.ir.datastructures.Fixtures._

  //=================================
  // IR Datastructures
  //=================================

  info("=======================================")
  info("Data structures part of Copying Suite")
  info("=======================================")

  test("Action") {
    val act = new Action
    val code = (m: Message, a: Agent) => {}

    act + Statement(code) + Statement(code)
    act.reset

    act.executeOne

    //    val copy = act.copy
    val copy = act.runtimeCopy
    copy should equal(act)
  }

  test("Action2") {
    val a = actionInstance2
    //    val c = a.copy
    val c = a.runtimeCopy
    c should equal(a)
  }

  test("Action3") {
    val a = actionInstance
    //    val c = a.copy

    val c = a.runtimeCopy

    // DEBUG
    //    val h1 = a.hashCode()
    //    val h2 = c.hashCode()
    //    
    //    assert(h1 == h2)

    c should equal(a)
  }

  test("Agent") {
    val a = agentInstance
    val copy = a.copy

    assert(a.hashCode() == copy.hashCode())

    copy should equal(a)
  }

  test("Behavior") {

    val b = behaviorInstance
    val copy = b.copy

    copy should equal(b)

  }

  test("Message") {

    // test 1
    val m = new Message
    val copy = m.copy

    assert(m.hashCode == copy.hashCode)

  }

  test("DummyFuture") {
    val f = futureInstance
    val c = f.copy

    assert(c.hashCode == f.hashCode)

    f should equal(c)

  }

  test("RuntimeTimedAction") {
    val ta = timedActionInstance
    val c = ta.copy

    ta should equal(ta)

  }

  test("Statement") {
    // Note that the 'code' need not be copied, it stays the same all the time! so we reference it instead
    // any Unit in that matter

    val s = statementInstance
    //    s.link(distributedSystemWithSchedulerInstance)
    val c = s.copy

    c should equal(s)

  }
  //
  test("SuspendableTask") {
    val t = taskInstance
    val c = t.copy

    c should equal(t)
  }

  test("DistributedSystem-1") {

    val ds = distributedSystemWithSchedulerInstance

    val ds2 = ds.copy

    ds2 should equal(ds)

    //DEBUG

    // (==) fails
    //    assert(ds2 is ds, "Distributed systems are not the same")
    //    assert(ds2 == ds, "Distributed systems are not the same")
    //    
    //    // (==) fails
    //    assert(ds.agents forall { x => x in ds2 }, "One of the Agents isn't the same as the other")
    //    assert(ds.agents forall { x => x == ds2.get(x.name) }, "One of the Agents isn't the same as the other")
    //
    //    // (==) fails
    //    assert(ds.messages forall { x => x in ds2 }, "One of the Messages isn't the same as the other")
    //    assert(ds.messages forall { x => x == ds2 }, "One of the Messages isn't the same as the other")
    //
    //    // both pass
    //    assert(ds.actions forall { x => x == ds2 }, "One of the Actions isn't the same as the other")
    //    assert(ds.actions forall { x => x in ds2 }, "One of the Actions isn't the same as the other")
    //
    //    // (==) fails
    //    assert(ds.behaviors forall { x => x._2 in ds2 }, "One of the Behaviors isn't the same as the other")
    //    assert(ds.behaviors forall { x => x._2 == ds2 }, "One of the Behaviors isn't the same as the other")
  }

  test("DistributedSystem-2") {
    val ds1 = echoServerInstance
    val ds2 = ds1.copy
    ds2.link // after this step, ds1 and ds2 must be identical

    assert(ds1 == ds2, "the two distributed systems were NOT equal")

  }

  //=================================
  // Scheduler-related
  //=================================

  info("Scheduler part of Copying Suite")

  test("BlockingTasksManager") {
    val btm = distributedSystemWithSchedulerInstance.scheduler.blockingMgr

    val c = btm.copy

    c should equal(btm)
  }

  test("Copying a TimedActionsTracker") {
    val tat = distributedSystemWithSchedulerInstance.scheduler.timedActionsTracker
    val c = tat.copy

    c should equal(tat)
  }

  test("SchedulerState-1") {
    // just its inner state, no need for the algorithm copying
    val scheduler = distributedSystemWithSchedulerInstance.scheduler

    val state = scheduler.snapshot
    val copy = state.copy

    copy should equal(state)
  }

  //  test("SchedulerState-2") {
  //    val sch1 = echoServerInstance.scheduler
  //
  //    val sch2 = echoServerInstance.scheduler
  //    val stateCopy = sch1.copyState
  //    sch2.restoreStateTo(stateCopy)
  //
  //    // TODO complete the SchedulerState Copy test
  //    assert(sch1 == sch2, "the two schedulers were NOT equal")
  //  }

  test("LocalState") {
    val ls = localStateInstance("coolest")

    val ds = distributedSystemWithSchedulerInstance

    val lsCopy = ls.copy
    lsCopy.link(ds)

    lsCopy should equal (ls)
  }

}

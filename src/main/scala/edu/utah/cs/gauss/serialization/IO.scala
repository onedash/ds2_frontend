package edu.utah.cs.gauss.serialization

import java.io._
import edu.utah.cs.gauss.ds2.core.ir.datastructures.Message
import edu.utah.cs.gauss.ds2.core.ir.datastructures.Agent
import scala.Function2
/**
 * @author  	Mohammed S. Al-Mahfoudh
 * 		   	mahfoudh@cs.utah.edu
 * 		   	Gauss Group - SoC
 * 		   	The University of Utah
 */
object IO {

  def saveObjToFile(obj: Any, file: File): Unit = {
    val fos = new FileOutputStream(file.getAbsolutePath)
    val oos = new ObjectOutputStream(fos)
    oos.writeObject(obj)
    oos.flush()
    oos.close()
    fos.close()
  }

  def readObjFromFile(file: File): Option[Any] = {
    
    val fis = new FileInputStream(file.getAbsolutePath)
    val ois = new ObjectInputStream(fis)
    val ob = ois.readObject()
    ois.close()
    fis.close()

    if (ob != -1)
      Some(ob)
    else
      None
  }

  def toBytes(obj: Any): Seq[Byte] = {
    val baos = new ByteArrayOutputStream() 
    val oos = new ObjectOutputStream(baos)
    oos.writeObject(obj)
    oos.flush()
    oos.close()
    val byteArr = baos.toByteArray()
    baos.flush()
    baos.close()
    byteArr
  }
  
  def fromBytes[T](bytes: Seq[Byte]): T = {
    fromBytes[T](bytes.toArray)
  }
  
  def fromBytes[T](bytes: Array[Byte]): T = {
    val bais = new ByteArrayInputStream(bytes)
    val ois = new ObjectInputStream(bais)
    val obj = ois.readObject()
    ois.close()
    bais.close()
    obj.asInstanceOf[T]
  }

}

object IOTest extends App{

    import IO._
    
//    @SerialVersionUID(100L)
//    class Whatever(some:String = "Whatever text") extends Serializable{
//      override def toString = s"Whatever($some)"
//    }
//    
//    val bytes = toBytes(new Whatever)
//    
//    println(bytes)   
//    println(fromBytes(bytes.toArray))
//    println(fromBytes(bytes.toSeq))
    
    println("awesome Scala")
    val code = (m:Message, a:Agent) => {println("GOOD!")}
    
    println(toBytes(code))
    
    fromBytes[Function2[Message,Agent,Unit]](toBytes(code))(new Message("hey"),new Agent("cool"))
//    
//    saveObjToFile(new Whatever, new File("output.dat"))
//    
//    println(readObjFromFile(new File("output.dat")).get)
}

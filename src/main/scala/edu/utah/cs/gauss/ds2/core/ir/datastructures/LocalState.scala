package edu.utah.cs.gauss.ds2.core.ir.datastructures

import java.util.UUID

import scala.collection.mutable.{Map => MMap}

import net.liftweb.json._
import net.liftweb.json.JsonDSL._

import edu.utah.cs.gauss.serialization.IO.{fromBytes, toBytes}
import scala.util.Try
import scala.util.Success

/**
 * @author  	Mohammed S. Al-Mahfoudh <br/>
 * 		   	mahfoudh@cs.utah.edu <br/>
 * 		   	Gauss Group - SoC <br/>
 * 		   	The University of Utah <br/>
 *
 * @author  	Heath French<br/>
 * 		   	Gauss Group - SoC <br/>
 * 		   	The University of Utah <br/>
 */

@SerialVersionUID(1600)
class LocalState(var agentName: String)  extends Serializable{

  val varToMem = MMap[String, UUID]()
  val memToMem = MMap[UUID, UUID]()
  val memToVal = MMap[UUID, Any]()

  val garbageCollection = MMap[UUID, BigInt]()

  //----------------------------------------
  // Decoration/proxy methods
  //----------------------------------------
  def apply(varName: String): Any = {
    getVal(varName)
  }

  /**
   * This update method is the same as "=" in mutable maps.
   * It maps <code>varName</code> to <code>value</code>.
   * @param varName the variable name to be assigned a value
   * @param value the value to be assigned to the varName
   */
  def update(varName: String, value: Any): Unit = {
    setVar(varName, value)
  }

  def +=(entry: (String, Any)): Unit = {
    setVar(entry._1, entry._2)
    }

  def +(entry: (String, Any)): LocalState = {
    +=(entry)
    this
    }

  //----------------------------------------
  def setVar(varName: String, value: Any): Unit = {
    /*
     * search for var.
     * 
     * not-exists? create new 
     * 
     * exists? modify it
     */

    if (!varToMem.contains(varName)) {
      // allocate new memory and bind var-to-val
      val id1 = UUID.randomUUID()
      val id2 = UUID.randomUUID()

      varToMem(varName) = id1
      memToMem(id1) = id2
      memToVal(id2) = value

      // count references
      garbageCollection(id1) = 1
    } else if (varToMem.contains(varName) &&
      memToVal(memToMem(varToMem(varName))) != value) {

      // allocate new memory and bind var-to-val
      val id1 = UUID.randomUUID()
      val id2 = UUID.randomUUID()

      val oldmemToMemID = varToMem(varName)

      varToMem(varName) = id1
      memToMem(id1) = id2
      memToVal(id2) = value

      // count references
      garbageCollection(id1) = 1

      // older ref to-de-ref GC count update
      garbageCollection(oldmemToMemID) = garbageCollection(oldmemToMemID) - 1

      garbageCollect(varName)

    } else // just update the binding from var-to-val
    {

      // finally do it
      memToVal(memToMem(varToMem(varName))) = value

    }
  }

  def getVal(varName: String): Any = {
    require(varToMem.contains(varName), "LocalState.getVal - variable \"" + varName + "\" is not declared yet.")

    memToVal(memToMem(varToMem(varName)))
  }

  def setRef(varNameToSet: String, varNameToRef: String): Unit = {

    require(varToMem.contains(varNameToSet), "LocalState.setRef - variable to set \"" + varNameToSet + "\" is not declared.")
    require(varToMem.contains(varNameToRef), "LocalState.setRef - variable to reference \"" + varNameToRef + "\" is not declared.")
    // require(varNameToSet.split(LocalState.DELIM))
    // garbage collect/update reference counts
    // to-set GC count update
    garbageCollection(varToMem(varNameToSet)) = garbageCollection(varToMem(varNameToSet)) - 1
    // to-ref GC count update
    garbageCollection(varToMem(varNameToRef)) = garbageCollection(varToMem(varNameToRef)) + 1

    // collect garbage
    garbageCollect(varNameToSet) // varNameToRef always increases

    // do it
    varToMem(varNameToSet) = varToMem(varNameToRef)

  }

  private def garbageCollect(varName: String): Unit = {
    val refCount = garbageCollection(varToMem(varName))
    if (refCount <= 0) {
      // remove from all maps
      val id1 = varToMem(varName)
      memToVal -= memToMem(id1) // get rid of the value
      memToMem -= id1 // get rid of the mem-to-mem mapping
      // variable can not be un-declared, only non-referenced memory is freed

      garbageCollection -= id1 // even garbage collection is garbage collected :)
    }
  }

  private def garbageCollect(memToMemID: UUID) = {
    val refCount = garbageCollection(memToMemID)
    if (refCount <= 0) {
      // remove from all maps
      val id1 = memToMemID //varToMem(varName)
      memToVal -= memToMem(id1) // get rid of the value
      memToMem -= id1 // get rid of the mem-to-mem mapping
      // variable can not be un-declared, only non-referenced memory is freed

      garbageCollection -= id1 // even garbage collection is garbage collected :)
    }
  }

  //  private def getRef(varName: String): String = {
  //    require(varToMem.contains(varName), "LocalState.getRef - variable \"" + varName + "\" is not declared yet.")
  //    ???
  //  }
  //  private def removeRef(varName: String): Unit = {
  //    require(varToMem.contains(varName), "LocalState.getRef - variable \"" + varName + "\" is not declared yet.")
  //    ???
  //  }

  //  def serialize(value: Any): Try[Any] = {
  //    
  //  }

  def toJson: JValue = {
    (getClass.getSimpleName ->
      ("agentName" -> agentName) ~
      ("varToMem" -> varToMem.map { case (x: String, y: UUID) => JField(x, y.toString) }) ~
      ("memToMem" -> memToMem.map { case (x, y) => JField(x.toString, y.toString) }) ~
      ("memToVal" -> memToVal.map { case (x, y) => JField(x.toString, LocalState.serialize(y)) }) ~
      ("garbageCollection" -> garbageCollection.map { case (x, y) => JField(x.toString, toBytes(y) map { z => JInt(z) }) }))
  }

  def traceCopy: LocalState = {
    LocalState.fromJson(toJson)
  }

  def copy: LocalState = {
    val newOne = new LocalState(agentName)

    // Mo: Do we really need to copy UUID's while they never change (maybe to run on a different JVM?)
    varToMem map { p: (String, UUID) => newOne.varToMem += (p._1 -> UUID.fromString(p._2.toString)) }
    memToMem map { p: (UUID, UUID) => newOne.memToMem += (UUID.fromString(p._1.toString) -> UUID.fromString(p._2.toString)) }
    memToVal map { p: (UUID, Any) => newOne.memToVal += (UUID.fromString(p._1.toString) -> copySpecific(p._2)) }
    garbageCollection map { p: (UUID, BigInt) => newOne.garbageCollection += (UUID.fromString(p._1.toString) -> copySpecific(p._2).asInstanceOf[BigInt]) }

    newOne
  }

  def link(ds: DistributedSystem): Unit = {
    memToVal map { linkSpecific(ds, _) }
  }

  private def linkSpecific(ds: DistributedSystem, toCopy: Any): Any = {
    toCopy match {
      case a: Agent => a.link(ds)
      case f: DummyFuture => f.link(ds)
      case a: Action => a.link(ds)
      case _ => ; // do nothing
    }
  }

  private def copySpecific(toCopy: Any): Any = {
    toCopy match {
      case a: Agent => a.copy
      case f: DummyFuture => f.copy
      case a: Action => a.copy
      case _ => fromBytes(toBytes(toCopy))
    }
  }

  //----------------------------------------
  //  Utility functions
  //----------------------------------------
  override def hashCode: Int = {
    varToMem.hashCode +
      memToMem.hashCode +
      memToVal.hashCode +
      garbageCollection.hashCode
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }

}

object LocalState {
  val DELIM = "$$"
  def fromJson(js: JValue): LocalState = {
    implicit val formats = net.liftweb.json.DefaultFormats

//    println("LocalState Obj: " + pretty(render(js \ "LocalState" \ "agentName")))

    val agentName = js \ "LocalState" \ "agentName" match {
      case JString(x) => x
      case null => null
      case JNothing => null
      case _ => throw new Error("LocalState.fromJson - No agentName OR no JObject representing LocalState passed.")
    }

    val localState = new LocalState(agentName)

    js \ "LocalState" \ "varToMem" match {
      case JArray(listOfPairs) => listOfPairs map {
        case JField(x, y) => localState.varToMem += (x.toString -> UUID.fromString(y.extract[String]))
        case _ => throw new Error("matching varToMem pairs yielded an outlier")
      }
      case _ => throw new Error("LocalState.fromJson - can't restore varToMem map")
    }

    js \ "LocalState" \ "memToMem" match {
      case JArray(listOfPairs) => listOfPairs map {
        case JField(x, y) => localState.memToMem += (UUID.fromString(x.extract[String]) -> UUID.fromString(y.extract[String]))
        case _ => throw new Error("matching memToMem pairs yielded an outlier")
      }
      case _ => throw new Error("LocalState.fromJson - can't restore memToMem map")
    }

    js \ "LocalState" \ "memToVal" match {
      case JArray(listOfPairs) => listOfPairs map {
        case JField(x, y) => localState.memToVal += (UUID.fromString(x.extract[String]) -> LocalState.deSerialize(y))
        case _ => throw new Error("matching memToVal pairs yielded an outlier")
      }
      case _ => throw new Error("LocalState.fromJson - can't restore memToVal map")
    }

    js \ "LocalState" \ "garbageCollection" match {
      case JArray(listOfPairs) => listOfPairs map {
        case JField(x, y) => localState.garbageCollection += (UUID.fromString(x.extract[String]) -> LocalState.deSerialize(y).asInstanceOf[BigInt])
        case _ => throw new Error("matching garbageCollection pairs yielded an outlier")
      }
      case _ => throw new Error("LocalState.fromJson - can't restore garbageCollection map")
    }

    localState
  }

  def serialize(value: Any): JValue = {
    value match {
      case x: Agent => x.toJson
      case f: DummyFuture => f.toJson
      case a: Action => a.toJson
      case _ => toBytes(value) map { x => JInt(x) }
    }
  }

  def deSerialize(js: JValue): Any = {

    js match {
      case JObject(List(JField("Agent", JObject(list)))) => Agent.fromJson(js)
      case JObject(List(JField("DummyFuture", JObject(list)))) => DummyFuture.fromJson(js)
      case JObject(List(JField("Action", JObject(list)))) => Action.fromJson(js)
      //      case _ => throw new Error("LocalState.deSerialize - un-handled case exception")
      case JArray(x) =>
        val bytesArray: Seq[Byte] = (x map {
                                       case JInt(y) => y toByte
                                       case _ => throw new Error("LocalState.deSerialize - yielded an outlier while matching a JInt()")
                                     })
        fromBytes(bytesArray)
      case _ => throw new Error("LocalState.deSerialize - unknown case")
    }
  }
}

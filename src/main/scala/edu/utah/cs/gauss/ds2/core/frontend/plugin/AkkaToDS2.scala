package edu.utah.cs.gauss.ds2.core.frontend.plugin

import java.io.FileOutputStream
import java.io.ObjectOutputStream
import edu._
import edu.utah.cs.gauss.ds2.core.schedulers.algorithms._
import edu.utah.cs.gauss.ds2.core.tracing._
import edu.utah.cs.gauss.serialization._
import edu.utah.cs.gauss.ds2.core.schedulers._
import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import edu.utah.cs.gauss.ds2.core.frontend.akka._
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import scala.tools.nsc
import scala.reflect.internal.Trees
import nsc.Global
import nsc.Phase
import nsc.plugins.Plugin
import nsc.plugins.PluginComponent
import scala.reflect.runtime.universe._
import scala.tools.reflect.ToolBox
import java.io.File

class AkkaToDS2(val global: Global) extends Plugin {
  import global._

  //println("Initializing AkkaToDS2")
  val name = "akkatods2"
  val description = "attempts to convert an akka distributed system into a ds2 version"
  var dumpLocation: File = null
  val components = List[PluginComponent](Component)
  
  /*
   * method is used to determine where DS2 agents should be stored for use
   * by other programs.
   */
  override def processOptions(options: List[String], error: String => Unit) {
    if(!options.isEmpty){
      dumpLocation = new File(options(0))
      println("Set Folder location to: " + dumpLocation.getAbsolutePath)
    }
  }
  
  /*
   * Object that is used by the compiler when the plugin is used.
   */
  private object Component extends PluginComponent {

    val global: AkkaToDS2.this.global.type = AkkaToDS2.this.global
    val runsAfter = List[String]("uncurry");
    val phaseName = AkkaToDS2.this.name
    val tb = runtimeMirror(getClass.getClassLoader).mkToolBox()
    
    def newPhase(_prev: Phase) = new AkkaToDS2Phase(_prev)
    
    class AkkaToDS2Phase(prev: Phase) extends StdPhase(prev) {
      
      override def name = AkkaToDS2.this.name
      def apply(unit: CompilationUnit) {
        for ( tree <- unit.body) {
          tree match{
            case ClassDef(mods, name, tparams, impl) => {
              /*
              println(name)
              println("Raw Version: ")
              println(showRaw(impl.parents))
              println("Normal Version: ")
              println(impl.parents)
              */
              
              var correctExtension: Boolean = false;
              for(x <- impl.parents){
                if(x.toString().compareTo("akka.actor.Actor") == 0){
                  correctExtension = true;
                }
              }
              
              /*
               * if the class is determined to inherit from akka.actor.Actor
               * then the plugin will proceed to generate a DS2 Agent for that
               * class.
               */
              if(correctExtension){
                val agent: Agent = new Agent(name.toString())
                for(currentNode <- impl.body){
                  currentNode match {
                    case DefDef(_, _, _, _, _ , _) => {}
                    case app @ ValDef(mods, name, tpt, rhs) => {
                      val temp = tb.parse("new Agent(\"test\")")
                      //println(app.substituteSymbols(List(tpt.symbol), List(temp.tree.symbol)))
                      println("Old Value")
                      println("Temp is: " + temp)
                      println("Raw temp is: " + showRaw(temp))
                      println("Temp type is: " + temp.tpe)
                      println("Normal type is: " + app)
                      println("Raw type is: " + showRaw(app))
                      println(tpt.id)
                      println(tpt.canHaveAttrs)
                      println(tpt)
                      println(tpt.hasSymbolField)
                      println(tpt.isType)
                      println(tpt.pos)
                      println(tpt.symbol)
                      println(tpt.tpe)
                      println()
                      
                      
                      
                      //Select(Select(Select(Select(Select(Select(Select(Select(Ident(edu), edu.utah), edu.utah.cs), edu.utah.cs.gauss), edu.utah.cs.gauss.ds2), edu.utah.cs.gauss.ds2.core), edu.utah.cs.gauss.ds2.core.ir), edu.utah.cs.gauss.ds2.core.ir.datastructures), edu.utah.cs.gauss.ds2.core.ir.datastructures.Agent)
                      
                      //println("New Normal type is: " + tpt)
                      //println("New Raw type is: " + showRaw(tpt))
                    }
                    case x => {
                      println(x)
                      println(showRaw(x))
                      }
                  }
                }
                
                /*
                 * Once the DS2 agent is generated, the program will now store
                 * said agent as a serialized file for later use.
                 */
                try {
                  if(dumpLocation != null && dumpLocation.isDirectory()){
                    val agentFile: File = new File(dumpLocation.getCanonicalPath + "\\" + name.toString() + ".agent") 
                    agentFile.createNewFile()
                    println(agentFile.getCanonicalFile)
                    println("Generating new ds2 agent")
                    val fos = new FileOutputStream(agentFile)
                    val oos = new ObjectOutputStream(fos)
                    oos.writeObject(agent)
                    oos.close
                  }
                }
                catch{
                  case e: Exception => {
                    println(e.toString())
                  }
                }
              } 
            }
            case _ => {}
          }
        }
        /*
        println("Raw Version")
        println(showRaw(unit.body))
        println("Normal Version: ")
        println(unit.body)
        */
      }
    }
  }
}
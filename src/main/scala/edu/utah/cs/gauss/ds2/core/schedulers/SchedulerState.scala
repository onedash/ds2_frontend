package edu.utah.cs.gauss.ds2.core.schedulers

import edu.utah.cs.gauss.ds2.core.ir.datastructures.DistributedSystem
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler._
import edu.utah.cs.gauss.ds2.core.ir.datastructures.SuspendableTask
import edu.utah.cs.gauss.ds2.core.tracing.TraceManager

/**
 * @author <br>
 * 	Mohammed S. Al-Mahfoudh <br/>
 * 	mahfoudh@cs.utah.edu <br/>
 * 	SoC - Gauss Group <br/>
 *
 * This class is a container for a Scheduler object state in order
 * to support back-tracking algorithms.
 *
 * It contains all the state that constitutes a scheduler restart from
 * some point in time.
 *
 */
class SchedulerState {
  var ds: DistributedSystem = _
  var clk: BigInt = 0
  var taskQ = Seq[SuspendableTask]()
  var consumeQ = Seq[SuspendableTask]()
  var blockingMgr: BlockedTasksManager = _
  var timedActionsTracker: TimedActionsTracker = _
  // traceMAnager isn't supposed to be copied. So it is linked
  var traceManager: TraceManager = _
  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    val dsHash = if (null == ds) 0 else ds.hashCode
    val blockingMgrHash = if (null == blockingMgr) 0 else blockingMgr.hashCode
    val timedActionsTrackerHash = if (null == timedActionsTracker) 0 else timedActionsTracker.hashCode

    dsHash +
      clk.hashCode +
      taskQ.hashCode +
      consumeQ.hashCode +
      blockingMgrHash +
      timedActionsTrackerHash
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }
  //==============================

  def copy: SchedulerState = {
    val copy = new SchedulerState

    // DistributedSystem copy+linking (linking is done inside its copy())
    copy.ds = ds.copy

    val clkCopy = clk
    // clk copy
    copy.clk = clkCopy

    // taskQ copy+linking
    copy.taskQ = taskQ map { x =>
      val t = x.copy
      t
    }

    // consumeQ copy+linking
    copy.consumeQ = consumeQ map { x =>
      val t = x.copy
      t
    }

    // blocking manager copy+linking
    copy.blockingMgr = blockingMgr.copy

    // timed actions copy+linking
    copy.timedActionsTracker = timedActionsTracker.copy

    //trace manager is linked only
    copy.traceManager = traceManager

    copy
  }

  def link(sch: Scheduler): Unit = {

    /* - ds.link
     * - ds.scheduler = sch
     * - the remaining is linked against this.ds (which is already done
     */

    ds.link // link against itself

    ds.scheduler = sch

    sch.ds = ds

    taskQ map { x => x.link(ds) }

    consumeQ map { x => x.link(ds) }

    blockingMgr.link(ds)

    timedActionsTracker.link(ds)

    // no need to do anything for traceManager
  }

  def restore(scheduler: Scheduler): Unit = {

    import scala.collection.mutable.Seq

    scheduler.ds = ds

    // link entities of the DS together
    scheduler.ds.link

    // link this SchedulerState entities to the newly linked DS
    link(scheduler)

    scheduler.clk = clk
    scheduler.taskQ = Seq(taskQ: _*) // :_* splices it there

    scheduler.consumeQ = Seq(consumeQ: _*)
    scheduler.blockingMgr = blockingMgr
    scheduler.timedActionsTracker = timedActionsTracker
    // trace manager is linked only
    scheduler.traceManager = traceManager

    // this is identical to what you see in the Scheduler companion object when it
    // instantiates a scheduler and links both scheduler and distributed systems 
    // with each others.
    scheduler.ds = ds
    scheduler.blockingMgr.ds = ds
    scheduler.blockingMgr.scheduler = scheduler

    ds.scheduler = scheduler
  }
}

object SchedulerState {

  def snapshotState(scheduler: Scheduler): SchedulerState = {
    val state = new SchedulerState
    state.ds = scheduler.ds.copy
    state.ds.link

    state.clk = scheduler.clk
    state.taskQ = scheduler.taskQ map { x =>
      val t = x.copy
      t.link(state.ds)
      t
    }

    state.consumeQ = scheduler.consumeQ map { x =>
      val t = x.copy
      t.link(state.ds)
      t
    }

    state.blockingMgr = scheduler.blockingMgr.copy
    state.blockingMgr.link(state.ds)

    state.timedActionsTracker = scheduler.timedActionsTracker.copy
    state.timedActionsTracker.link(state.ds)

    //trace manager is linked only
    state.traceManager = scheduler.traceManager

    state
  }

}

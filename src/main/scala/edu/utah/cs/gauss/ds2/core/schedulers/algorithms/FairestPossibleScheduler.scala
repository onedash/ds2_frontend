package edu.utah.cs.gauss.ds2.core.schedulers.algorithms

import edu.utah.cs.gauss.ds2.core.ir.datastructures.DistributedSystem
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler
import edu.utah.cs.gauss.ds2.core.tracing.TraceEntry
import edu.utah.cs.gauss.ds2.core.tracing.Trace
/**
 * @author
 *        	Mohammed S. Al-Mahfoudh <p>
 * 		   	mahfoudh@cs.utah.edu <p>
 * 		   	Gauss Group - SoC <p>
 * 		   	The University of Utah <p>
 */
class FairestPossibleScheduler(ds: DistributedSystem) extends Scheduler {
  import scala.collection.mutable.Seq
  override def explore(): Seq[Trace] = {
    
    ???
    Seq[Trace]()
  }
}
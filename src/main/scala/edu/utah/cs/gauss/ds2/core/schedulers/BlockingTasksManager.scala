package edu.utah.cs.gauss.ds2.core.schedulers

import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import scala.collection.GenTraversableOnce
import scala.collection.parallel.immutable.ParSet
import sun.management.resources.agent
import scala.collection.GenTraversable
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 *
 * As the name implies, it manages agents blocking and unblocking on future objects.
 *
 */

@SerialVersionUID(1100)
class BlockedTasksManager(implicit var ds: DistributedSystem, implicit var scheduler: Scheduler) extends Serializable {

  var blockedTasks = Set[SuspendableTask]()

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    blockedTasks.hashCode
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }
  //==============================

  def copy: BlockedTasksManager = {
    val btm = new BlockedTasksManager()(ds, scheduler)
    btm.blockedTasks = blockedTasks map { x => x.copy }
    btm
  }

  def link(ds: DistributedSystem): Unit = {
    /*
     * Items to cross-reference/refresh in each BlockedTask:
     * - ds
     * - scheduler
     * - task
     * - task.a.blockedOn
     */

    this.ds = ds
    this.scheduler = ds.scheduler

    blockedTasks map { task =>

      task.link(ds)

      val asker: Option[Agent] = ds.agents.find { x => x.name == task.a.name}
      val promiser: Option[Agent] = ds.agents.find {x => x.name == ds.get(asker.get.name).blockedOn.promisedBy.name}

      promiser match {
        case Some(x) => x.futuresPromised(task.a.blockedOn.id) = task.a.blockedOn
        case _       => throw new Error("BlockedTasksManager.link - how could there be a future in the blocking manager with no promiser?!")
      }

      asker match {
        case Some(x) => x.futuresWaitingFor(task.a.blockedOn.id) = task.a.blockedOn ; x.blockedOn = task.a.blockedOn 
        case _       => throw new Error("BlockedTasksManager.link - how could there be a future in the blocking manager with no asker?!")
      }

      // a statement that blocked this task is known, so it should know what it brought to the action of suffering
      task.action.executed.last.action = task.action
      
      
      // left just in case
      //      // now cross referencing task.a.blockedOn
      //      var future = task.a.blockedOn
      //
      //      if (null != future) {
      //        // promisedBy
      //        future.promisedBy =
      //          if (null != future.promisedBy && ds.contains(future.promisedBy.name)) ds.get(future.promisedBy.name)
      //          else if (null != future.promisedByName && ds.contains(future.promisedByName)) ds.get(future.promisedByName)
      //          else null
      //        // waitingFor
      //        future.waitingFor =
      //          if (null != future.waitingFor && ds.contains(future.waitingFor.name)) ds.get(future.waitingFor.name)
      //          else if (null != future.waitingForName && ds.contains(future.waitingForName)) ds.get(future.waitingForName)
      //          else null
      //
      //        // update the NEW agent from the NEW DistributedSystem that was already linked with this future
      //        if (future.waitingFor != null) {
      //          future.waitingFor.blocked = true // because that agent is already in the blocking manager, duh
      //          future.waitingFor.blockedOn = future
      //        }
      //        
      //      }
      //
      //      task.suspend
      //      block(task)
    }

  }

  def getSuspendedTask(a: Agent): Option[SuspendableTask] = {
    blockedTasks.find { x => x.a.name == a.name }
  }

  def blocked: Set[SuspendableTask] = {
    this.synchronized {
      blockedTasks.filter { x => x.isSuspended }
    }
  }

  def removeUnblocked: Unit = {
    this.synchronized {
      blockedTasks = blockedTasks.filter { _.isSuspended }
    }
  }

  def unblocked: Set[SuspendableTask] = {
    ds.synchronized {
      this.synchronized {
        blockedTasks.filter { x => !x.isSuspended }
      }
    }
  }

  def unblock(a: Agent): Unit =
    {
      require(null != a && ds.agents.contains(a), "Unblock method - either agent is null or is not part of the distributed system")
      //      require(blocked.contains(a), "Unblock method - can't unblock an agent that is NOT blocked")

      // lock the DistributedSystem
      ds.synchronized {
        //lock the blockedAgents set
        this.synchronized {
          var task = blockedTasks.find { x => x.a.name == a.name }
          if (task != None && !a.blocked) {
            blockedTasks map { x => if (x.a.name == a.name) { x.resume } }
            unblocked map { x => scheduler.schedule(x) }
            removeUnblocked
          }
        }
      }
    }

  def setResolved(df: DummyFuture): Unit = {
    require(null != df, "DistributedSystem.resolve(future) method - Can't resolve a null future")
    this.synchronized {
      val thisFutureBlockedTasks = blockedTasks.filter { x => x.a == df.waitingFor }
      val futures = thisFutureBlockedTasks.filter { x => x.a.blockedOn == df }.map { x => x.a.blockedOn }
      futures.map { x => x.resolve(df.promisedBy, df.value) }
    }
  }

  def unblock(agents: Set[Agent]): Unit = {

    require(null != agents)
    // synchronization happens in unblock-method
    agents.map { x => unblock(x) }
  }

  //  def block(a: Agent, action: Action, f: DummyFuture): Unit = {
  //    require(ds.agents.contains(a), "Block method - can't block an agent that is NOT part of the distributed system")
  //    ds.synchronized {
  //      this.synchronized {
  //        val ag = ds.agents.find { x => a.name == x.name }
  //        val agentToBlock = ag.get
  //        if (ag != None) {
  //          agentToBlock.blocked = true
  //          agentToBlock.blockedOn = f
  //          blockedTasks += new SuspendableTask(agentToBlock, action)
  //        }
  //      }
  //    }
  //  }
  //  
  def block(task: SuspendableTask): Unit = {
    require(ds.agents.contains(task.a), "Block method - can't block a task whose agent is NOT part of the distributed system")
    ds.synchronized {
      this.synchronized {
        val ag = ds.agents.find { x => task.a.name == x.name }
        val agentToBlock = ag.get
        if (ag != None) {
          // blocking the agent should be done in the DS.get method
          blockedTasks += task
        }
      }
    }
  }

  def block(tasks: Set[SuspendableTask]): Unit = {
    // synchronization is taken care of in the block() method
    tasks.map { x => block(x) }
  }

  def traceCopy: BlockedTasksManager = {
    BlockedTasksManager.fromJson(toJson)
  }

  def toJson: JValue = {
    (getClass.getSimpleName ->
      ("blockedTasks" -> blockedTasks.map { x => x.toJson }))

    // we don't care about DS nor Scheduler in this stage as they are the "containing" objects.
    // this is to avoid dependency cycles too.
  }
}

object BlockedTasksManager {
  def fromJson(js: JValue): BlockedTasksManager = {
    implicit val formats = DefaultFormats
    //    js.extract[BlockedTasksManager]

    // note that the DS and scheduler will be added by Scheduler.fromJson.
    val btm = new BlockedTasksManager()(null, null)

    val blockedTasks = js \\ "blockedTasks" match {
      case JArray(x) => x map { z => SuspendableTask.fromJson(z) }
      case _         => throw new Error("BlockedTasksManager.fromJson - can't extract 'blockedTasks'")
    }

    btm.blockedTasks = blockedTasks.toSet

    btm
  }
}
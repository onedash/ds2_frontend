package edu.utah.cs.gauss.ds2.core.ir.datastructures

import scala.collection.mutable.{
  Map => MMap,
  Seq => MSeq,
  Set => MSet,
  Stack
}
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler
import java.util.UUID
import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._
import edu.utah.cs.gauss.serialization.IO.{ toBytes, fromBytes }
import breeze.linalg.split
/**
 * @author
 *        	Mohammed S. Al-Mahfoudh <p>
 * 		   	mahfoudh@cs.utah.edu <p>
 * 		   	Gauss Group - SoC <p>
 * 		   	The University of Utah <p>
 *
 * This is the main Entity in any distributed system. A communicating
 * process with encapsulated state that is divided into 3 categories:
 * 1- Internal State (e.g. localState field)
 * 2- Communication state (e.g. the stash and the incoming queue)
 * 3- Special reactions state (e.g. onStart, onJoin, ...etc).
 *
 * Note that to override the special behaviors (3), you need to assign
 * Actions to the corresponding key in the
 * <code>specialBehaviors</code> Map[Message,Action].
 *
 * Almpst all important constraints and functions are implemented in
 * the containing DistributedSystem's class.  That includes the
 * tracing functionality, correctness checks, and updating most of
 * agent's state.
 *
 */

@SerialVersionUID(300)
class Agent(var name: String) extends Serializable {

  var q: Seq[Message] = Seq() // incoming message queue, I am using Seq since it provides more flexibility that a Queue 

  var defaultBehavior: Behavior = new Behavior("default") // theta, the default behavior

  var stash: Seq[Message] = Seq() // S, the stash. I am using Seq since it provides more flexibility than a Queue

  var reactions: Behavior = defaultBehavior // R, initially set to 'defaultBehavior'

  var behaviors: Map[String, Behavior] = Map() // B, the set of behaviors the agent is allowed to exhibit

  var specialReactions: Behavior = new Behavior("special") // sigma, a set of special reactions (Message, Action) pairs, initially empty. Override-able by users.

  // not used, no plans to use any time soon, maybe later.
  //  var timedActions: Set[RuntimeTimedAction] = Set() // scheduled/periodic actions

  var oldBehaviors: Stack[String] = Stack() // beta, a stack of old behaviors, initially empty

  var scheduler: Scheduler = null
  // this is checked by send methods to "enqueue" messages in the 'q'
  var locked = true

  var blocked = false
  var blockedOn: DummyFuture = null
  // this is checked by the scheduler to "dequeue" tasks from the 'q'
  // it is necessary for become/unbecome atomicity
  var consuming = true

  // InternalState
  // var localState = MMap[String, Any]() // The old way of doing it
  var localState = new LocalState(name) // the new way

  var futuresPromised = MMap[UUID, DummyFuture]()
  var futuresWaitingFor = MMap[UUID, DummyFuture]()

  //=================================
  // Helper construct for local state
  //=================================
//  
//  case class Pointer(var value: String) {
//    
//    def getValue: Any = {
//      getVariable(value) match {
//        case x:Pointer => x.getValue  
//        case x => x
//      }
//    }
//    
//    def setValue(value: Any): Unit = {
//      
//      
//    }
//  }
  
  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {

    val blockedOnHash = if (null == blockedOn) 0 else blockedOn.hashCode

    q.hashCode +
      defaultBehavior.hashCode +
      stash.hashCode +
      reactions.hashCode +
      behaviors.hashCode +
      specialReactions.hashCode +
      oldBehaviors.hashCode +
      locked.hashCode +
      blocked.hashCode +
      blockedOnHash +
      consuming.hashCode +
      localState.hashCode +
      futuresPromised.hashCode +
      futuresWaitingFor.hashCode +
      name.hashCode
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }

  //=======================
  // Initialization
  //=======================  
  def refresh: Unit = { // this method is the easiest way to set all contained state message+Agent parameters of e.g. behaviors
    // setting Agent in all executable code
    defaultBehavior.setAgent(this)
    reactions.setAgent(this)
    behaviors = behaviors.map { x => val beh = x._2; beh.setAgent(this); (x._1, beh) }
    specialReactions.setAgent(this)

    // setting toExecute statements in all actions 
    defaultBehavior.resetActions
    reactions.resetActions
    specialReactions.resetActions
    behaviors.values.map { x => x.resetActions }
  }

  //=======================
  // Future methods
  //=======================
  def promise(m: Message): DummyFuture = {
    // When overridden:
    // possibly has to do some work here
    scheduler.blockingMgr.synchronized {
      val f = new DummyFuture(false, scheduler.clock(), this, m.sender)
//      futuresPromised(f.id) = f
      m.sender.futuresWaitingFor(f.id) = f
      f
    }

    // possibly more work here? so far NOT
  }

  //=======================
  // Special rections (user override-able)
  //=======================
  if (specialReactions.reactions.isEmpty) {
    val sAc = new Action

    // FIXME this needs to change to "instrumented" unlock statement
    //    sAc + Statement((m: Message, a: Agent) => scheduler.ds.unlock(this))
    sAc + Statement(Statement.Kind.UNLOCK, this)
    val jAc = new Action

    val rjAc = new Action

    val lAc = new Action

    val dAc = new Action

    val lfAc = new Action

    val stopAc = new Action

    val b = new Behavior("special")

    b.reactions = Map(
      (new Start -> sAc),
      (new Stop -> stopAc),
      (new Join -> jAc),
      (new ReJoin -> rjAc),
      (new Leave -> lAc),
      (new Demise -> dAc),
      (new LinkFailed -> lfAc))
    b.setAgent(this)
    specialReactions = b
  }
  def onStart(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }

  }

  def onStop(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }
  }

  def onJoin(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }
  }

  def onReJoin(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }
  }

  def onLeave(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }
  }

  def onDemise(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }
  }

  def onLinkeFailure(m: Message): Action = {
    // users override this one, we provide the stubs only
    specialReactions.synchronized {
      specialReactions(m)
    }
  }

  def setSpecialReaction(m: Message, action: Action): Unit = {
    require(m in specialReactions, "Agent.setSpecialReaction method - you can't add a reaction whose trigger isn't one of the special messages")
    action.setAgent(this)
    action.setMessage(m)
    specialReactions += (m, action)
  }

  //==================================
  // Tracing facilities
  //==================================
  def copy: Agent = {
    //    Agent.fromJson(toJson)

    // constant name referenced
    val agent = new Agent(name)

    // have to copy
    agent.q = q map { x => x.copy }

    // can copy or reference, will copy just in case
    agent.defaultBehavior = defaultBehavior.copy

    // copied
    agent.stash = stash map { x => x.copy }

    // can referenced or copied, I copied
    agent.reactions = reactions.copy

    // can referenced or copied
    agent.behaviors = behaviors map { x => (x._1 -> x._2.copy) }

    // can reference or copy
    agent.specialReactions = specialReactions.copy

    // setting the special reaction to refer to the new agent copy reference

    //    val actTMP = agent.specialReactions(new Start)

    // removes last statement from action
    //    actTMP - ()
    //    // change this statement to "instrumented" unlcok statement
    ////    actTMP + Statement((m: Message, a: Agent) => { agent.scheduler.ds.unlock(agent) })
    ////    actTMP + Statement(Statement.Kind.UNLOCK,agent)
    ////    
    //    
    //    actTMP.setAgent(agent)
    //    actTMP.reset
    //    agent.specialReactions += (new Start -> actTMP)

    // copied (it changes)
    //    agent.timedActions = timedActions map { x => x.copy }

    // copied, since it changes, but Strings are constants
    agent.oldBehaviors = oldBehaviors map { x => x }

    //    agent.name = name

    // referenced, we can't copy the "runtime", only save some of its state
    agent.scheduler = scheduler
    // this is checked by send methods to "enqueue" messages in the 'q'
    agent.locked = if (locked) true else false

    //DEBUG
    //    agent.locked == false

    agent.blocked = if (blocked) true else false

    // copied, it changes with time
    agent.blockedOn = if (null == blockedOn) null else blockedOn.copy

    // this is checked by the scheduler to "dequeue" tasks from the 'q'
    // it is necessary for become/unbecome atomicity
    agent.consuming = if (consuming) true else false

    // InternalState (copied with each object also copied)
//    agent.localState = localState map { x => (x._1, fromBytes[Any](toBytes(x._2))) }

    // agent.localState = copyLocalState
    agent.localState = localState.copy

    agent.futuresPromised = futuresPromised map { x => val f = x._2.copy; (f.id, f) }
    agent.futuresWaitingFor = futuresWaitingFor map { x => val f = x._2.copy; (f.id, f) }

    agent
  }
  
  // private def copyLocalState: MMap[String,Any] = {
    
    
    
    // localState map{
    //   x =>
    //     val (varName, value) = x
        
    //     val segments = varName.split("\\[\\]")
        
    //     segments(1) match {
    //       case "Message" => val m = value.asInstanceOf[Message].copy; (varName,m)
    //       case "DummyFuture" => val df = value.asInstanceOf[DummyFuture].copy; (varName,df)
    //       case "Agent" => (varName,value)
    //       case _ => (varName,fromBytes[Any](toBytes(value)))
    //     }
    // }
  // }

  //----------------------------------------
  // OLD
  //----------------------------------------

  //  private def linkLocalState(ds: DistributedSystem): MMap[String,Any] = {
    
  //   localState map{
  //     x =>
  //       val (varName, value) = x
        
  //       val segments = varName.split("\\[\\]")
        
  //       segments(1) match {
  //         case "Message" => val m = value.asInstanceOf[Message].link(ds); (varName,m)
  //         case "DummyFuture" => val df = value.asInstanceOf[DummyFuture].link(ds); (varName,df)
  //         case "Agent" => 
  //           val agentName = value.asInstanceOf[Agent].name 
  //           (varName,ds.get(agentName))
  //         case _ => (varName,fromBytes[Any](toBytes(value)))
  //       }
  //   }
  // }
  

  /**
   * This is a utility method not intended to be used by user's code.
   *
   * It takes care of cross-references between agents and their contained
   * data structures after copying agents.
   *
   * This is to be called by the fresh copy of the distributed system copy method.
   *
   * @param ds the fresh copy of the Distributed system
   */
  def link(ds: DistributedSystem): Unit = {

    /*
     * To refresh
     * - q
     * - stash
     * - blockedOn
     * - futuresPRomised
     * - futuresWaitingFor
     * - behaviors all of them since all of them are taken care of in the "refresh" call above  
     */

    this.scheduler = ds.scheduler

    
    q map { x => x.link(ds) }

    stash map { x => x.link(ds) }

    futuresPromised map (x => x._2.link(ds))
    futuresWaitingFor map (x => x._2.link(ds))

    defaultBehavior.link(ds)
    reactions.link(ds)
    behaviors map { x => x._2.link(ds) }
    specialReactions.link(ds)

     // OLD
    // linkLocalState(ds)

    // new OO way
    localState.link(ds)

    //    oldBehaviors

    // do this at the blocking manager level
    //    if (blocked && null != blockedOn) blockedOn = futuresWaitingFor(blockedOn.id) // linking to the newly linked futures
    //    else throw new Error("Agent.link() - can't link 'blockedOn'")
  }

  /**
   *  Used only to get future objects from the local state proxy variable.
   * 
 * @param varName the variable name that serves as a proxy to our future object
 * @return the future
 */
def parseAndGet(varName: String): DummyFuture = {
  // TODO remove when not needed anymore.
  
    require(varName != null, "Agent.parseAndGet() - can't have null parameter")

  // TODO change it to use LocalState.DELIM
  // also add LocalStateInstance.apply()
    var identifiers = localState(varName).toString.split("\\[\\]")
    
    var theBehavior = identifiers(0) match {
      case "default" => defaultBehavior
      case "special" => specialReactions
      case x => behaviors(x)
    }
    
    var action = theBehavior(new Message(identifiers(1)))
    
    var theStatement: Option[Statement] = action.stmts find { x => x.id.toString == identifiers(2) }
    
    theStatement.get.future
  }



//----------------------------------------
// Now get/set vars are handled inside the
// LocalState.scala
//----------------------------------------
//   /**
//    * If there is a variable in the agent's local state, returns its value. Otherwise, throw an exception/Error
//    * @param variableName The variable whose value we need be returned by the method
//    * @return the value of the 'variableName'
//    */
  def getVariable(variableName: String): Any = {
    
//    require(localState.contains(variableName), "Agent.getVariable() - localState doesn't contain the variable name: "+variableName)
		  localState(variableName)
    
    
//		  var segments = variableName.split("\\[\\]")
    
//    segments(1) match {
//      case "DummyFuture" => parseAndGet(variableName)
//      case _ => localState(variableName)
//    }
  }




  /**
   * Adds or modifies an already
   *
   * @param variableName the variable name appended with its type using the '[]' separator
   * @param value any object of type Any
   */
  def setVariable(variableName: String, value: Any): Unit = {
    require(variableName != null, "Agent.setVariable() - variable name is null!")
    
   // val segments = variableName.split("\\[\\]")
   
   // segments(1) match {
   //   case "Seq[Agent]" =>
   //     val actualAgentSeq = value.asInstanceOf[Seq[Agent]]
   //     val agentNames =  actualAgentSeq map { x => x.name }
   //     localState(variableName) = agentNames 
       
   //   case _ =>  
   // }
   //  localState(variableName) = value

    localState.setVar(variableName, value)
    
  }
  


  //----------------------------------------
  // This is none-sense
  //----------------------------------------
  //   /**
//    * Drops a variable from local state of the agent.
//    * 
//  * @param variableName the variable name to drop
//  */
// def dropVariable(variableName:String): Unit = {
//   // TODO add '-' method to drop a key and its value from LocalState.
//     localState = localState - (variableName)
//   }
  

  def traceCopy: Agent = {
    Agent.fromJson(toJson)
  }

  def toJson: JValue = {
    import edu.utah.cs.gauss.serialization.IO.toBytes
    
    val blkdOn = if (null == blockedOn) null else blockedOn.toJson

    (getClass.getSimpleName ->
      ("name" -> name) ~
      ("q" -> q.map { x => x.toJson }) ~
      ("defaultBehavior" -> defaultBehavior.toJson) ~
      ("stash" -> stash.map { x => x.toJson }) ~
      ("reactions" -> reactions.toJson) ~
      // x._1 is the name of the behavior, x._2 is the Behavior object
      ("behaviors" -> behaviors.map { x => JField(x._1, x._2.toJson) }) ~
      ("specialReactions" -> specialReactions.toJson) ~
      //      ("timedActions" -> timedActions.map { x => x.toJson }) ~
      ("oldBehaviors" -> oldBehaviors) ~
      ("localState" -> localState.toJson) ~
      ("locked" -> locked) ~
      ("consuming" -> consuming) ~
      ("blocked" -> blocked) ~
      ("blockedOn" -> blkdOn) ~
      ("futuresPromised" -> futuresPromised.map { x => x._2.toJson }) ~
      ("futuresWaitingFor" -> futuresWaitingFor.map { x => x._2.toJson }))
  }
  //==================================
  // Utilities
  //==================================
  override def toString: String = {
    name + ": queue = " + q.mkString(",")
  }
  def in(agents: Set[Agent]): Boolean = {
    require(agents != null, "Agent.in() method - doesn't accept null arguments")
    agents.contains(this)
  }

  def in(ds: DistributedSystem): Boolean = {
    require(ds != null, "Agent.in(ds) method - doesn't accept null arguments")
    ds.agents.exists { x => x.name == name }
  }

  def is(that: Agent): Boolean = {
    if (null == that)
      false
    else
      name == that.name

  }
}

object Agent {
  def fromJson(js: JValue): Agent = {
    import edu.utah.cs.gauss.serialization.IO.fromBytes
    implicit val formats = net.liftweb.json.DefaultFormats
    //    js.extract[Agent]

    val ajs = js \ "Agent"

    val name = ajs \ "name" match {
      case JString(x) => x
      case _          => throw new Error("Agent.fromJson - can't extract 'name'")
    }

    val q: scala.collection.Seq[Message] = ajs \ "q" match {
      case JArray(x) => x map { z => Message.fromJson(z) }
      case _         => throw new Error("Agent.fromJson - can't extract 'q'")
    }

    val defaultBehavior = ajs \ "defaultBehavior" match {
      case x: JObject => Behavior.fromJson(x)
      case _          => throw new Error("Agent.fromJson - can't extract 'defaultBehavior'")
    }

    val stash = ajs \ "stash" match {
      case JArray(x) => x map { z => Message.fromJson(z) }
      case _         => throw new Error("Agent.fromJson - can't extract 'stash'")
    }

    val reactions = ajs \ "reactions" match {
      case x: JObject => Behavior.fromJson(x)
      case _          => throw new Error("Agent.fromJson - can't extract 'reactions'")
    }

    // extracting behaviors and names
    val behaviors: Map[String, Behavior] = ajs \ "behaviors" match {
      case JArray(listOfPairs) =>
        listOfPairs map {
          case JField(bName, b) => (bName -> Behavior.fromJson(b))
          case _                => throw new Error("Agent.fromJson - can't extract a pair from 'behaviors'")
        } toMap
      case _ =>
        println(ajs \ "behaviors")
        throw new Error("Agent.fromJson - can't extract 'behaviors'")
    }

    // extracting special reactions
    val specialReactions = ajs \ "specialReactions" match {
      case x: JObject => Behavior.fromJson(x)
      case _          => throw new Error("Agent.fromJson - can't extract 'specialReactions'")
    }

    //    val timedActions = ajs \ "timedActions" match {
    //      case JArray(listOfTA) => listOfTA map { tajs => RuntimeTimedAction.fromJson(tajs) }
    //      case _                => throw new Error("Agent.fromJson - can't extract an enabled behavior")
    //    }

    val oldBehaviorsSeq = ajs \ "oldBehaviors" match {
      case JArray(x) =>
        x map {
          case JString(z) => z
          case _          => throw new Error("Agent.fromJson - can't extract an old behavior")
        }
      case _ => throw new Error("Agent.fromJson - can't extract 'oldBehaviors'")
    }
    val oldBehaviors = Stack(oldBehaviorsSeq: _*) // splice values of the sequence in a Stack

    // DEBUG
    //        println(ajs \ "localState")
    // extracting localState map
    //----------------------------------------
    // The following code was replaced by one line down there
    //----------------------------------------
    // val localStatePairs = ajs \ "localState" match {
    //   case JArray(List()) => Seq[(JValue, JValue)]()
    //   case JArray(x)      => x
    //   case _              => throw new Error("Agent.fromJson - can't extract a pair from 'localState'")
    // }
    // var localState = Map[String, Any]()
    // localStatePairs map {
    //   case JField(varName, varJArrayBytes) =>
    //     val valBytes = varJArrayBytes.extract[Seq[Byte]]
    //     localState = localState + (varName -> fromBytes[Any](valBytes))
    //   case _ => throw new Error("Agent.fromJson - can't extract a 'localState' entry")
    // }

    val locked = ajs \ "locked" match {
      case JBool(x) => x
      case _        => throw new Error("Agent.fromJson - can't extract 'locked'")
    }

    val consuming = ajs \ "consuming" match {
      case JBool(x) => x
      case _        => throw new Error("Agent.fromJson - can't extract 'consuming'")
    }

    val blocked = ajs \ "blocked" match {
      case JBool(x) => x
      case _        => throw new Error("Agent.fromJson - can't extract 'blocked'")
    }

    //DEBUG
    //    println(ajs \ "blockedOn")
    val blockedOn = ajs \ "blockedOn" match {
      case null       => null
      case x: JObject => DummyFuture.fromJson(x)
      case _          => throw new Error("Agent.fromJson - can't extract 'blockedOn'")
    }

    // extracting futures promised
    val futuresPromisedList = ajs \ "futuresPromised" match {
      case JArray(listOfFutures) => listOfFutures map { x => DummyFuture.fromJson(x) }
      case _                     => throw new Error("Agent.fromJson - can't extract 'futuresPromised' list")
    }

    //extracting futures waitingFor
    val futuresWaitingForList = ajs \ "futuresWaitingFor" match {
      case JArray(listOfFutures) => listOfFutures map { x => DummyFuture.fromJson(x) }
      case _                     => throw new Error("Agent.fromJson - can't extract 'futuresWaitingFor' list")
    }

    // populataing both futures promised and waitingFor
    var futuresPromised = scala.collection.mutable.Map[UUID, DummyFuture]()
    futuresPromisedList map { x => futuresPromised(x.id) = x }

    var futuresWaitingFor = scala.collection.mutable.Map[UUID, DummyFuture]()
    futuresWaitingForList map { x => futuresWaitingFor(x.id) = x }

    // don't forget to assign extracted values to agent returned fields
    val agent = new Agent(name)

    agent.q = q
    agent.defaultBehavior = defaultBehavior
    agent.stash = stash
    agent.reactions = reactions
    agent.behaviors = behaviors
    agent.specialReactions = specialReactions
    //    agent.timedActions = timedActions.toSet
    agent.oldBehaviors = oldBehaviors
    agent.localState = LocalState.fromJson(ajs \ "localState")
    agent.locked = locked
    agent.consuming = consuming
    agent.blocked = blocked
    agent.blockedOn = blockedOn
    agent.futuresPromised = futuresPromised
    agent.futuresWaitingFor = futuresWaitingFor

    agent
  }
}

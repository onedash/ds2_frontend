package edu.utah.cs.gauss.ds2.core.ir.datastructures

import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import edu.utah.cs.gauss.ds2.core.schedulers.TimedActionsTracker

/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 *
 * While TimedActions are perfectly good for compile-time representation,
 * we need more information about TimedActions above during runtime. e.g. if
 * they timed out w.r.t. when they were started, ...etc.
 *
 * This is the class that the scheduler use to actually act on timed actions.
 */

@SerialVersionUID(800)
class RuntimeTimedAction(
    val startLimit: BigInt,
    val endLimit: BigInt,
    val action: Action,
    var agent: Agent,
    val howManyTimes: BigInt,
    val submittedClock: BigInt) extends Serializable {
  var countDown = startLimit // every how many ticks this action is triggered
  var runtimeHowManyTimes: BigInt = howManyTimes // how many times remaining this action runs

  var agentName: String = _

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    val agentHash = if (null == agent) 0 else agent.hashCode
    val agentNameHash = if (null == agentName) 0 else agentName.hashCode

    startLimit.hashCode +
      endLimit.hashCode +
      action.hashCode +
      agentHash +
      howManyTimes.hashCode +
      submittedClock.hashCode +
      countDown.hashCode +
      runtimeHowManyTimes.hashCode +
      agentNameHash
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }
  //==============================

  def reset: Unit = {
    countDown.synchronized {
      countDown = startLimit
    }
  }

  def toJson: JValue = {
    this.synchronized {

      agentName = if (null == agent) null else agent.name

      (getClass.getSimpleName ->
        ("startLimit" -> startLimit) ~
        ("endLimit" -> endLimit) ~
        ("action" -> action.toJson) ~
        ("agent" -> null) ~
        ("agentName" -> agentName) ~
        ("howManyTimes" -> howManyTimes) ~
        ("submittedClock" -> submittedClock) ~
        ("countDown" -> countDown) ~
        ("runtimeHowManyTimes" -> runtimeHowManyTimes))
    }
  }

  def copy: RuntimeTimedAction = {

    val ta = new RuntimeTimedAction(
      startLimit,
      endLimit,
      action.copy,
      agent,
      howManyTimes,
      submittedClock)
    ta.agentName = agentName
    ta.countDown = countDown
    ta.runtimeHowManyTimes = runtimeHowManyTimes
    ta
  }

  def link(ds: DistributedSystem): Unit = {
    
    /*
     * disconnects the old agent 
     * "reference" and replaces it with the new copy of the fresh 
     * distributed system copy
     * 
     */
    
    agent = 
      if(null != agent && ds.contains(agent.name)) ds.get(agent.name) 
      else if (null != agentName && ds.contains(agentName)) ds.get(agentName) 
      else null
    action.link(ds)
  }

  def traceCopy: RuntimeTimedAction = {
    RuntimeTimedAction.fromJson(toJson)
  }

  def in(rttas: Set[RuntimeTimedAction]): Boolean = {
    require(rttas != null, "RuntimeTimedAction.in(rttActions) method - doesn't accept null arguments")
    rttas.exists(x => x == this)
  }

  def in(tat: TimedActionsTracker): Boolean = {
    require(null != tat, "RuntimeTimedAction.in(TimedActionsTracker) method - doesn't accept null arguments")

    // why the circus? to convert from parallel collection to sequential one.
    this.in(tat.timedActions.toList.toSet)
  }

  def is(that: RuntimeTimedAction): Boolean = {

    val agentCondition = if (null == agent) null == that.agent else agent is that.agent

    startLimit == that.startLimit &&
      endLimit == that.endLimit &&
      (action is that.action) &&
      agentCondition &&
      howManyTimes == that.howManyTimes

    // the rest of attributes are just runtime attributes.
  }

  override def toString: String = {
    //    val agentStr = if(null == agent) null else agent.name
    agentName = if (null == agent) null else agent.name

    "start limit = " + startLimit + "\n" +
      "end limit = " + endLimit + "\n" +
      "action = " + action.toExecute.size + " Statement(s)" + "\n" +
      "agent = " + agentName + "\n" +
      "how many times = " + howManyTimes + "\n" +
      "time submitted = " + submittedClock + "\n" +
      "count down = " + countDown + "\n" +
      "runtime how many = " + runtimeHowManyTimes + "\n"
  }

}

object RuntimeTimedAction {
  def fromJson(js: JValue): RuntimeTimedAction = {
    implicit val formats = DefaultFormats
    //    js.extract[RuntimeTimedAction]

    val tajs = js \ "RuntimeTimedAction"

    val startLimit = tajs \ "startLimit" match {
      case JInt(x) => x
      case _       => throw new Error("RuntimeTimedAction.fromJson - can't extract 'startLimit'")
    }

    val endLimit = tajs \ "endLimit" match {
      case JInt(x) => x
      case _       => throw new Error("RuntimeTimedAction.fromJson - can't extract 'endLimit'")
    }

    val action = tajs \ "action" match {
      case x: JObject => Action.fromJson(x)
      case _          => throw new Error("RuntimeTimedAction.fromJson - can't extract 'action'")
    }

    val agent = null

    val agentName = tajs \ "agentName" match {
      case JString(x) => x
      case _          => throw new Error("RuntimeTimedAction.fromJson - can't extract 'agentName'")
    }

    val howManyTimes = tajs \ "howManyTimes" match {
      case JInt(x) => x
      case _       => throw new Error("RuntimeTimedAction.fromJson - can't extract 'howManyTimes'")
    }

    val submittedClock = tajs \ "submittedClock" match {
      case JInt(x) => x
      case _       => throw new Error("RuntimeTimedAction.fromJson - can't extract 'submittedClock'")
    }

    val countdown = tajs \ "countDown" match {
      case JInt(x) => x
      case _       => throw new Error("RuntimeTimedAction.fromJson - can't extract 'countDown'")
    }

    val runtimeHowManyTimes = tajs \ "runtimeHowManyTimes" match {
      case JInt(x) => x
      case _       => throw new Error("RuntimeTimedAction.fromJson - can't extract 'runtimeHowManyTimes'")
    }

    // set the values in the returned action
    val ta = new RuntimeTimedAction(
      startLimit,
      endLimit,
      action,
      agent,
      howManyTimes,
      submittedClock)
    ta.agentName = agentName
    ta.countDown = countdown
    ta.runtimeHowManyTimes = runtimeHowManyTimes

    ta
  }
}

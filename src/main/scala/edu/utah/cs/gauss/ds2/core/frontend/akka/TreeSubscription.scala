package edu.utah.cs.gauss.ds2.core.frontend.akka

import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import scala.collection.mutable.HashMap
import scala.reflect.internal.Trees
import scala.tools.nsc
import nsc.Global
import nsc.CompilationUnits

class TreeSubscription{
  import TreeSubscription._

  def addDS(newDS: DistributedSystem) = constantAddDS(newDS)
  def receiveDS() : DistributedSystem = constantReceiveDS()
}

object TreeSubscription{
  
  println("Initializing subscription object")
  private var storedDS : DistributedSystem = null
  
  def constantAddDS(newDS: DistributedSystem) = {
    storedDS = newDS
  }
  
  def constantReceiveDS() : DistributedSystem = {
     storedDS
  }
}
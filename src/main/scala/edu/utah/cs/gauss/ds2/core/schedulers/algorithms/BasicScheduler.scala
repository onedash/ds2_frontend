package edu.utah.cs.gauss.ds2.core.schedulers.algorithms

import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler
import edu.utah.cs.gauss.ds2.core.ir.datastructures.DistributedSystem
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler.Algorithm
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler.Basic
import edu.utah.cs.gauss.ds2.core.tracing.TraceEntry
import edu.utah.cs.gauss.ds2.core.tracing.Trace
/**
 * @author
 *        Mohammed S. Al-Mahfoudh <p>
 * 		   	mahfoudh@cs.utah.edu <p>
 * 		   	Gauss Group - SoC <p>
 * 		   	The University of Utah <p>
 */
class BasicScheduler(ds1: DistributedSystem) extends Scheduler {
  ds = ds1

  import scala.collection.mutable.Seq
  override def explore(): Seq[Trace] = {
    // this is just a manual scheduler so won't implement
    ???
  }
}
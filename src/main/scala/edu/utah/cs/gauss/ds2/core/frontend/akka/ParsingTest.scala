package edu.utah.cs.gauss.ds2.core.frontend.akka

import java.io.FileInputStream
import java.io.ObjectInputStream
import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import org.apache.commons.cli._
import sys.process._
import java.io.File
import java.io.PrintWriter
import scala.tools.nsc.io.Path
import scala.collection.mutable.ArrayBuffer

object ParsingTest extends App{
  
  override def main(args: Array[String]){
    
    val options: Options = new Options()
    options.addOption("parse", true, "Has the program parse the classes in the given folder.")
    
    val parser: CommandLineParser = new DefaultParser();
    val cmd: CommandLine = parser.parse(options, args);
    
    val pluginLocation = new File(ParsingTest.getClass.getProtectionDomain().getCodeSource().getLocation().toURI())
    println("Current Location:" + pluginLocation.getCanonicalPath)

    if(cmd.hasOption("parse")){
      val arg = cmd.getOptionValue("parse")
      if(arg == null){
        println("Invalid argument received")
      }
      else{
        /*
         * Verifies that the path we were given
         * exists and is a directory.
         */
        val rootFolder: File = new File(arg)
        if(!rootFolder.exists() || rootFolder.isFile()){
          println("Invalid path given.")
          return
        }
        println(rootFolder.getAbsolutePath)
        
        /*
         * Creates an additional sbt file in the selected project
         * that adds the compiler plugin we created to its process.
         */
        val dump: File = new File(rootFolder.getAbsolutePath + "\\DistributedSystemAgents\\")
        val addOn: File = new File(rootFolder.getAbsolutePath + "\\AddOn.sbt")
        addOn.createNewFile()
        if(!dump.exists()){
          dump.mkdir()
        }
        val writer = new PrintWriter(addOn)

        writer.write("scalacOptions += \"-Xplugin:" + pluginLocation.getAbsolutePath.replace("\\", "\\\\") + "\"\r\n")
        writer.write("scalacOptions += \"-P:akkatods2:" + dump.getAbsolutePath.replace("\\", "\\\\") + "\"\r\n")
        writer.close()
        
        
        println("Attempting to rebuild selected project.")
        
        /*
         * Runs sbt on the sbt project path given.
         * The way the external process is called is
         * dependent on if it is a windows operating system
         * or not.
         */
        val subscription: TreeSubscription = new TreeSubscription()
        var result = 1;
        if(System.getProperty("os.name").toLowerCase().contains("windows")){
          result = Process("cmd.exe /C sbt clean compile", rootFolder).!
        }
        else{
          result = Process("sbt clean compile", rootFolder).!
        }
        
        /*
         * Once finised running the external process,
         * the additional file we created earlier is
         * removed and the external process is checked
         * for any exiting errors.
         */
        addOn.delete()
        
        if(result != 0){
          println("Error: Unable to rebuild project.")
          return
        }
        
        /*
         * Once successful, we will now begin to generate
         * the DS2 version of the code given the ASTs
         * we received from our compiler plugin.
         */
        println("Project rebuilt successfully.")
        
        var agents: ArrayBuffer[Agent] = new ArrayBuffer[Agent]()
        for(currentFile <- dump.listFiles()){
          val fis = new FileInputStream(currentFile)
          val ois = new ObjectInputStreamWithCustomClassLoader(fis)
          
          val agent:Agent = ois.readObject().asInstanceOf[Agent]
          ois.close()
          
          agents += agent
          currentFile.delete()
        }
        
        dump.delete()
        
        for(agent <- agents){
          println(agent.toString())
        }
      }
    }

  }
  
}

class ObjectInputStreamWithCustomClassLoader(fileInputStream: FileInputStream) extends ObjectInputStream(fileInputStream) {
  override def resolveClass(desc: java.io.ObjectStreamClass): Class[_] = {
    try { Class.forName(desc.getName, false, getClass.getClassLoader) }
    catch { case ex: ClassNotFoundException => super.resolveClass(desc) }
  }
}
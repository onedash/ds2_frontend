package edu.utah.cs.gauss.ds2.core.ir.datastructures
import edu.utah.cs.gauss.ds2.core.schedulers.BlockedTasksManager
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import java.util.UUID
import edu.utah.cs.gauss.serialization.IO.{ toBytes, fromBytes }
import scala.concurrent.forkjoin.ForkJoinPool.Submitter

/**
 * This DummyFuture implementation is to be managed by the
 * BlockedAgentsManager, not directly by the scheduler.
 *
 * @param <T> The type of value to be resolved by this future.
 */

@SerialVersionUID(600)
class DummyFuture(
    var resolved: Boolean = false,
    var submitedTime: BigInt,
    var promisedBy: Agent,
    var waitingFor: Agent) extends Serializable {

  var value: Any = _
  var id = UUID.randomUUID

  var promisedByName = if (null != promisedBy) promisedBy.name else ""
  var waitingForName = if (null != waitingFor) waitingFor.name else ""

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    val valueHash = if (null == value) 0 else value.hashCode
    val promisedByyName = if (null == promisedBy) "" else promisedBy.name
    val waitingForrName = if (null == waitingFor) "" else waitingFor.name

    resolved.hashCode +
      submitedTime.hashCode +
      promisedByyName.hashCode +
      waitingForrName.hashCode +
      valueHash +
      id.hashCode
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }
  //==============================

  def get: Option[Any] =
    {
      this.synchronized {
        if (resolved)
          Some(value)
        else
          None
      }
    }

  def resolve(a: Agent, value: Any): Unit = {
    this.synchronized {

      this.value = value

      resolved = true
      promisedBy.futuresPromised -= id
      waitingFor.futuresWaitingFor -= id
      waitingFor.localState += (id.toString -> value) // FIXME fix this, now futures can be referred to by "variable" name in the local state. Replace DS.resolve Future arg with future name
      waitingFor.blocked = false

    }
  }

  def toJson: JValue = {

    val promisedByyName =
      if (null != promisedBy) promisedBy.name
      else if (null != promisedByName && "" != promisedByName) promisedByName
      else ""
    val waitingForrName =
      if (null != waitingFor) waitingFor.name
      else if (null != waitingForName && waitingForName != "") waitingForName
      else ""

    val thisVal: JValue = if (null == value) null else toBytes(value) map { x => JInt(x) }

    (getClass.getSimpleName ->
      ("resolved" -> resolved) ~
      ("submittedTime" -> submitedTime) ~
      ("promisedBy" -> null) ~
      ("promisedByName" -> promisedByName) ~
      ("waitingFor" -> null) ~
      ("waitingForName" -> waitingForName) ~
      ("value" -> thisVal) ~
      ("id" -> id.toString))
  }

  def copy: DummyFuture = {

    val sTime: BigInt = submitedTime

    val copy = new DummyFuture(if (resolved) true else false, sTime, promisedBy, waitingFor)
    copy.value = fromBytes[Any](toBytes(value))
    copy.id = id

    copy.promisedBy = promisedBy
    copy.waitingFor = waitingFor

    copy.promisedByName = promisedByName
    copy.waitingForName = waitingForName

    copy
  }

  /**
   * A utility method to be used by DistributedSystem.copy()
   *
   * @param ds the fresh distributed system copy
   */
  def link(ds: DistributedSystem): Unit = {
    /*
     * disconnects the old agents 
     * "references" and replaces them with the new copy of the fresh 
     * distributed system copy
     * 
     */

    promisedBy =
      if (null != promisedBy && ds.contains(promisedBy.name)) {
        ds.get(promisedBy.name).futuresPromised(id) = this
        ds.get(promisedBy.name)
      }
      else if (null != promisedByName && ds.contains(promisedByName)) {
        ds.get(promisedByName).futuresPromised(id) = this
        ds.get(promisedByName)
      }
      else null

    waitingFor =
      if (null != waitingFor && ds.contains(waitingFor.name)) {
        ds.get(waitingFor.name).futuresWaitingFor(id) = this
        ds.get(waitingFor.name)
      }
      else if (null != waitingForName && ds.contains(waitingForName)) {
        ds.get(waitingForName).futuresWaitingFor(id) = this
        ds.get(waitingForName)
      }
      else null
  }

  def traceCopy: DummyFuture = {
    DummyFuture.fromJson(toJson)
  }

  def in(futures: Map[UUID, DummyFuture]): Boolean = {
    require(futures != null, "DummyFuture.in() method - doesn't accept null arguments")
    futures.keySet.contains(id)
  }

  def in(blockingMGR: BlockedTasksManager): Boolean = {
    require(null != blockingMGR, "DummyFuture.in(BlockingManager) method - doesn't accept null arguments")
    blockingMGR.blocked.find { x => this is x.a.blockedOn } != None
  }

  def is(that: DummyFuture): Boolean = {
    id == that.id
  }
  
  override def toString: String = {
    
    s"resolved = ${resolved},\nsubmitted-time = ${submitedTime},\npromised-by = ${promisedBy},\nwaiting-for= ${waitingFor},\nvalue = ${value},\nid = ${id}"
  }

}

object DummyFuture {
  def fromJson(js: JValue): DummyFuture = {
    implicit val formats = DefaultFormats
    //    js.extract[DummyFuture]

    val rslvd = js \ "DummyFuture" \ "resolved" match {
      case JBool(x) => x
      case _ => throw new Error("DummyFuture - can't extract 'resolved'")
    }

    val subTime = js \ "DummyFuture" \ "submittedTime" match {
      case JInt(x) => x
      case _ => throw new Error("DummyFuture - can't extract 'submittedTime'")
    }

    val promiser = null

    val promiserName = js \ "DummyFuture" \ "promisedByName" match {
      case JString(x) => x
      case _ => throw new Error("DummyFuture - can't extract 'promisedByName'")
    }

    val waiter = null

    val waiterName = js \ "DummyFuture" \ "waitingForName" match {
      case JString(x) => x
      case _ => throw new Error("DummyFuture - can't extract 'waitingForName'")
    }

    val value = js \ "DummyFuture" \ "value" match {
      case null => null
      case JArray(x) => fromBytes(x map { x =>
        x match {
          case JInt(x) => x.toByte
          case _ => throw new Error("DummyFuture - can't be the case that a non JInt is in the seq!")
        }
      })
      case _ => throw new Error("DummyFuture - can't extract 'value'")
    }

    val id = js \ "DummyFuture" \ "id" match {
      case JString(x) => UUID.fromString(x)
      case _ => throw new Error("DummyFuture - can't extract 'id'")
    }

    val f = new DummyFuture(rslvd, subTime, promiser, waiter)
    f.promisedByName = promiserName
    f.waitingForName = waiterName

    f.id = id

    f
  }
}

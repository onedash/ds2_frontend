package edu.utah.cs.gauss.ds2.core.schedulers
import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import edu.utah.cs.gauss.ds2.core.schedulers.Scheduler.Algorithm
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import edu.utah.cs.gauss.ds2.core.schedulers.algorithms._
import edu.utah.cs.gauss.ds2.core.schedulers._
import java.util.concurrent.Future
import scala.util.Random
import net.liftweb.json.JsonAST.JObject
import edu.utah.cs.gauss.ds2.core.tracing.TraceEntry
import scala.collection.mutable.Seq
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import edu.utah.cs.gauss.ds2.core.tracing._

/**
 * @author Mohammed S. Al-Mahfoudh
 * 		   mahfoudh@cs.utah.edu
 * 		   Gauss Group - SoC
 * 		   The University of Utah
 * The Scheduler factory used to create different kinds of schedulers.
 *
 */

@SerialVersionUID(1200)
object Scheduler extends Serializable {
  // these may go away, i will keep to remind my self of the queue implementations
  /**
   * Top class that needs be extended by an object. That object is then
   * used to specify which scheduler to create in the factory method
   * <code>apply()</code>.
   */

  @SerialVersionUID(1300)
  trait Algorithm extends Serializable
  case object Basic extends Algorithm
  case object RoundRobin extends Algorithm
  case object LongestNonBlocking extends Algorithm
  case object FairestPossible extends Algorithm
  case object DPOR extends Algorithm
  case object TransDPOR extends Algorithm
  case object OptimalDPOR extends Algorithm
  case object SmartTestGeneration extends Algorithm
  case object DeadlockDetectionScheduler extends Algorithm

  /**
   *  The factory method by which all schedulers are created.
   * @param ds the DistributedSystem object it is going to manipulate.
   * @param alg the Algorithm used to smartly scheduler tasks. An
   * equivalent implementation of the class extending {@Scheduler}
   * must be present or an error is thrown.
   * @return Scheduler object to step the whole distributed system
   * or just run it to completion.
   */
  def apply(ds: DistributedSystem, alg: Algorithm): Scheduler = {
    alg match {
      case Basic =>
        val s = new BasicScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case RoundRobin =>
        val s = new RoundRobinScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case LongestNonBlocking =>
        val s = new LongestNonBlockingScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case FairestPossible =>
        val s = new FairestPossibleScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case DPOR =>
        val s = new DPORScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case TransDPOR =>
        val s = new TransDPORScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case OptimalDPOR =>
        val s = new OptimalDPORScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case SmartTestGeneration =>
        val s = new SmartTestGenerationScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case DeadlockDetectionScheduler =>
        val s = new DeadlockDetectionScheduler(ds); s.ds = ds; s.blockingMgr.ds = ds; ds.scheduler = s; s
      case _ => throw new Error("unknown algorithm for scheduler")
    }
  }

  /**
   * Used by fromJson to return the appropriate algorithm object.
   *
   * @param schedulerName string exact name of the algorithm object
   * @return the Algorithm object
   */
  private def decideAlg(schedulerName: String): Algorithm = {
//    println(schedulerName)
    schedulerName match {
      case "Basic"                      => Basic
      case "RoundRobin"                 => RoundRobin
      case "LongestNonBlocking"         => LongestNonBlocking
      case "FairestPossible"            => FairestPossible
      case "DPOR"                       => DPOR
      case "TransDPOR"                  => TransDPOR
      case "OptimalDPOR"                => OptimalDPOR
      case "SmartTestGeneration"        => SmartTestGeneration
      case "DeadlockDetectionScheduler" => DeadlockDetectionScheduler
      case _                            => throw new Error("Can't figure out the algorithm!")
    }
  }

  def fromJson(js: JValue): Scheduler = {
    implicit val formats = DefaultFormats

    val ds = js \\ "ds" match {
      case x: JObject => DistributedSystem.fromJson(x)
      case _          => throw new Error("Scheduler.fromJson - can't extract 'ds'")
    }

    val numThreads = js \\ "numThreads" match {
      case JInt(x) => x
      case _       => throw new Error("Scheduler.fromJson - can't extract 'numThreads'")
    }

    val scheduelingAlgorithmName = js \\ "scheduingAlgorithm" match {
      case JString(x) => x
      case _          => throw new Error("Scheduler.fromJson - can't extract 'scheduingAlgorithm'")
    }

    val clock = js \\ "clock" match {
      case JInt(x) => x
      case _       => throw new Error("Scheduler.fromJson - can't extract 'clock'")
    }

    val taskQ = js \\ "taskQ" match {
      case JArray(x) => x map { y => SuspendableTask.fromJson(y) }
      case _         => throw new Error("Scheduler.fromJson - can't extract 'clock'")
    }

    val consumeQ = js \\ "consumeQ" match {
      case JArray(x) => x map { y => SuspendableTask.fromJson(y) }
      case _         => throw new Error("Scheduler.fromJson - can't extract 'consumeQ'")
    }

    val blockingMgr = js \\ "blockingMgr" match {
      case x: JObject => BlockedTasksManager.fromJson(x)
      case _          => throw new Error("Scheduler.fromJson - can't extract 'blockingMgr'")
    }

    val timedActionsTracker = js \\ "timedActionsTracker" match {
      case x: JObject => TimedActionsTracker.fromJson(x)
      case _          => throw new Error("Scheduler.fromJson - can't extract 'timedActionsTracker'")
    }

    val traceManager = js \\ "traceManager" match {
      case x: JObject => TraceManager.fromJson(x)
      case _          => throw new Error("Scheduler.fromJson - can't extract 'traceManager'")
    }

    // why the gymnastics? to remove the last character which is '$'
    val scheduler = Scheduler(ds, decideAlg(scheduelingAlgorithmName.slice(0, scheduelingAlgorithmName.size - 1)))

    // ds is set already
    scheduler.numThreads = numThreads.toInt
    // scheduling alg is set already

    scheduler.clk = clock

    scheduler.taskQ = Seq(taskQ: _*)

    scheduler.consumeQ = Seq(consumeQ: _*)

    scheduler.blockingMgr = blockingMgr

    scheduler.timedActionsTracker = timedActionsTracker

    scheduler.traceManager = traceManager

    scheduler
  }

}

/**
 * @author Mohammed S. Al-Mahfoudh
 * 		   mahfoudh@cs.utah.edu
 * 		   Gauss Group - SoC
 * 		   The University of Utah
 *
 *
 *  To use a scheduler, it is important to keep in mind that the following sequence of
 *  methods should be executed <b>in-order</b>:
 *  1. call to pick/pick(agent) to extract a task
 *  2. the task extracted from above should be scheduled using schedule, scheduleOnce
 *    - in case of timed actions schedulePeriodic must be used instead.
 *  3- a call to execute to execute a task from the consumedQ by a thread
 *    - in case of stepping through an action's statements, use execute but schedule the same
 *      <code>StatementExecutionThread</code> more than once. DO NOT however interleave this thread's
 *      statements with other thread's actions/statements, its a violation of the operational semantics
 *      as we don't support fine-grained interleaving of agents' actions. We assume each agent's actions
 *      are atomically executed uninterrupted as a transaction. For blocked agents, this is the only time
 *      where we need to use fine grained interleaving of actions' statements, they can be pre-empted
 *      and their action's statements has to interleave with other agent's in order to resolve the future
 *      they are blocked by.
 */
@SerialVersionUID(1400)
trait Scheduler extends Serializable {

  private implicit val scheduler = this

  import Scheduler._

  // once a thread blocks, update the blocked set, decrement available threads num
  var numThreads: Int = 1 // default scheduler is a single threaded one
  var schedulingAlgorithm: Algorithm = Basic
  var clk: BigInt = 0 // abstract clock
  implicit val clock = () => clk
  implicit var ds: DistributedSystem = null

  // decided to get rid of threadPool as it is useless and introduces some problems
  //  var tp: ExecutorService = Executors.newFixedThreadPool(numThreads)
  var taskQ = Seq[SuspendableTask]()
  var consumeQ = Seq[SuspendableTask]()
  // execute queue is already there inside the actual thread pool.
  private var tracingEnabled = false
  //---------------------
  // managers/trackers
  //---------------------
  // futures and which agents are blocked by them
  var blockingMgr = new BlockedTasksManager
  // timed actions
  var timedActionsTracker = new TimedActionsTracker
  // traces
  var traceManager = new TraceManager

  val randomizer = Random
  //==================
  // Interface methods
  //==================

  def enableTracing: Unit = {
    //    this.synchronized {
    tracingEnabled = true
    //    }
  }

  def disableTracing: Unit = {
    //    this.synchronized {
    tracingEnabled = false
    //    }
  }

  def isTracingEnabled: Boolean = {
    //    this.synchronized {
    tracingEnabled
    //    }
  }

  def copyState: SchedulerState = {
    snapshot
  }

  def restoreStateTo(state: SchedulerState): Unit = {
    require(null != state, "Scheduler.restoreStateTo(state) - can't restore to a null 'state' argument")
    state.restore(this)
  }

  /**
   * This method advances the scheduler's clock and manages the timed actions.
   */
  def tick: Unit = {
    //    traceManager.synchronized {
    val te = new TraceEntry
    val ev = new Tick
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = ev
    }

    //    this.synchronized {
    clk += 1
    val triggered = timedActionsTracker.tick

    if (isTracingEnabled) {
      if (None != triggered)
        ev.triggered = triggered.get.map { x => x.traceCopy }
      else
        ev.triggered = Set()
    }

    if (triggered != None)
      triggered.get.map {

        x =>
          {
            // Older non strict timed actions execution, left for history.

            //            val task = new SuspendableTask(x.agent, x.action)
            //            task.isTimed = true
            //                schedule(task)

            // Changing the "timed actions" execution to be "strictly timed".
            // that is, doesn't wait to be scheduled then consumed. It gets highest
            // priority in the Operational Semantics with respect to execution time
            // compared to anything else.
            execute(x, 1)
          }
      }
    //    }

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += te
      traceManager.current += TraceEntryEnd(te)
    }
    //    }
  }

  def pickAll(a: Agent): Seq[SuspendableTask] = {
    // the power of functional!
    /*
       * Explanation:
       * 1- a.q map all of those (instead of counting and for-loop)
       * 2- pick a task from a.q each time
       * 3- splice the resulting collection
       * 4- make a mutable sequence out of them
       * 
       */
    scala.collection.mutable.Seq((a.q.map { _ => pick(a) }): _*)
  }

  /**
   * Picks a task from the specific agent in the parameter.
   * This method doesn't advance the scheduler's clock.
   *
   * @param a the agent whose queue from which a task is dequeued.
   * @return the task.
   */
  def pick(a: Agent): SuspendableTask = {
    require(ds.hasWork.contains(a), "Scheduler.pick(a) method - Agent doesn't have work, can't pick a task from its queue")
    require(ds != null, "Distributed System is null!")
    require(a != null, "Agent to pick a task from is null!")
    require(ds.agents.contains(a), "Agent " + a.name + "isnt part of the distributed system")
    require(ds.hasWork.contains(a), "Agent " + a.name + "dowsn't have tasks")
    require(a.consuming, "Agent " + a.name + "isn't enabled for consuming.")
    //    traceManager.synchronized {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      traceManager.current += te
    }

    val m = a.q.head

    // dummy picked task (scala restriction to init all local vars...)
    var pickedTask: SuspendableTask = new SuspendableTask(a, new Action)

    // Special reactions
    pickedTask =
      if (m.isInstanceOf[ResolveDummyFuture]) {
        //          a.synchronized {
        val msg = m.asInstanceOf[ResolveDummyFuture]
        val resolvedFuture = msg.payload.head.asInstanceOf[DummyFuture]
        // Create an action that resolves the future
        val act = new Action
        act.a = a // this action will act primarily on the destination agent state.
        act.m = m
        val stmt = Statement((message: Message, agent: Agent) => {
          ds.resolve(resolvedFuture, resolvedFuture.value)
          blockingMgr.setResolved(resolvedFuture)
          // then when the resolvedFuture.waitingFor calls get, he gets the value
        })
        stmt.a = act.a // statement acts on the destination agent's state 
        stmt.m = act.m
        act +(stmt)
        new SuspendableTask(a, act)
        //          }
      }
      else if (m in a.specialReactions) //          a.synchronized 
      {
        // does implicit action copy
        val action = a.specialReactions(m)
//        val actCopy = action.runtimeCopy
        action.setMessage(m)
        //          action.setAgent(a) // this is not set here, this is set earlier (who will execute this action)
        new SuspendableTask(a, action)
      }
      else // generic case
      //          a.synchronized 
      {
        // does implicit action copy
        val action = a.reactions(m)
//        val actCopy = action.runtimeCopy
        action.setMessage(m)
        new SuspendableTask(a, action)
      }

    if (isTracingEnabled) {
      te.event = new Pick(a.copy, pickedTask)
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }

    // now get rid of older message from the agent queue
    //      a.q.synchronized {
    a.q = a.q.tail
    //      }

    pickedTask
    //    }
  }

  /**
   * Picks a task from a random agent's task queue and returns it.
   */
  def pick: SuspendableTask = {
    require(ds != null, "Scheduler.pick() - Distributed System is null!")
    require(!ds.hasWork.isEmpty, "Scheduler.pick() - There is no work in the distributed system.")

    //    traceManager.synchronized {
    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      traceManager.current += te
    }

    // pick a random agent
    val a = ds.hasWork.toVector(randomizer.nextInt(ds.hasWork.size))

    val pickedTask = pick(a)

    if (isTracingEnabled) {
      te.event = new PickRandom(pickedTask)
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }

    pickedTask
    //    }
  }

  /**
   * An execute method specialized for RuntimeTimedActions
   * @param timedAction the runtime timed action already triggered and need be executed.
   * @param threadID the id of the thread that will execute the timed action
   */
  def execute(timedAction: RuntimeTimedAction, threadID: Long): Unit = {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new ExecuteTimed(timedAction.traceCopy, threadID)
      //      traceManager.synchronized {
      traceManager.current += te
      //      }
    }

    val a = timedAction.agent
    val action = timedAction.action
    val task = new SuspendableTask(a, action)

    val t = StandardExecutionThread(task, threadID)
    t.start
    t.join

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      //      traceManager.synchronized {
      traceManager.current += TraceEntryEnd(te)
      //      }
    }
  }

  //  /**
  //   * Suspend a currently running task
  //   *
  //   * @param task the task that will be suspended (i.e. blocked)
  //   */
  //  def suspend(task: SuspendableTask): Unit = {
  //    task.suspend
  //    // blocking is done by the DS.get
  //  }
  //
  //  /**
  //   * Resume a suspended task (i.e. a blocked task)
  //   * @param task the suspended task to be resumed
  //   */
  //  def resume(task: SuspendableTask): Unit = {
  //    task.resume
  //    schedule(task)
  //  }

  //  /**
  //   * Picks any agent (using <code>pick()</code>), and executes it out of
  //   * order. That is without respecting the scheduling scheme. That is
  //   * basically equivalent to submitting it right-away for execution.
  //   *
  //   * This provides fast forward ability if we are NOT interested in
  //   * analyzing that agent, but its final effect on the overall
  //   * distributed system needs to be reflected.
  //   * @param threadID the specific thread to execute the task.
  //   */
  //  def executeAny(threadID: Long): Unit = {
  //    require(threadID >= 0 && threadID < numThreads, "Thread ID is out of range")
  //
  //    val te = new TraceEntry
  //    if (isTracingEnabled) {
  //      te.prior = ds.deepCopy
  //      traceManager.synchronized {
  //        traceManager.current += te
  //      }
  //    }
  //
  ////    val task = pick()
  ////    // scheduler, skipping all:
  ////    tp.submit(StandardExecutionThread(task, threadID))
  //    val task = pick
  //    execute(task.a, threadID)
  //
  //    if (isTracingEnabled) {
  //      te.event = new ExecuteAny(task, threadID)
  //      te.posterior = ds.deepCopy
  //      traceManager.synchronized {
  //        traceManager.current += TraceEntryEnd(te)
  //      }
  //    }
  //  }
  /**
   * Picks the agent specified (using <code>pick(Agent)</code>),
   * and executes it. Skipping the scheduling scheme.That is
   * basically equivalent to submitting it right-away for execution.
   *
   * This provides fast forward ability if we are NOT interested in
   * analyzing that agent, but its final effect on the overall
   * distributed system needs to be reflected.
   *
   * @param threadID the specific thread to execute the task.
   * @param a the agent from which a task is stepped.
   */
  def execute(a: Agent, threadID: Long): Unit = {
    require(a != null && ds.hasWork.contains(a))
    require(threadID >= 0 && threadID < numThreads)

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      //      traceManager.synchronized {
      traceManager.current += te
      //      }
    }

    val task = pick(a)
    val m = task.action.m
    if (m in a.specialReactions) {
      m match {
        case x: edu.utah.cs.gauss.ds2.core.ir.datastructures.Start => a.onStart(m)
        case x: edu.utah.cs.gauss.ds2.core.ir.datastructures.Stop => a.onStop(m)
        case x: Join => a.onJoin(m)
        case x: ReJoin => a.onReJoin(m)
        case x: Demise => a.onDemise(m)
        case x: Leave => a.onLeave(m)
        case x: LinkFailed => a.onLinkeFailure(m)
      }
      tick
    }
    else // scheduler, skipping all other steps
    {
      val t = StandardExecutionThread(task, threadID)
      t.start
      t.join
    }

    if (isTracingEnabled) {
      te.event = new ExecuteSpecific(a.copy, task, threadID)
      te.posterior = ds.traceCopy
      //      traceManager.synchronized {
      traceManager.current += TraceEntryEnd(te)
      //      }
    }
  }

  def schedule: Unit = {
    schedule(pick)
  }

  def scheduleAll(tasks: Seq[SuspendableTask]): Unit = {
    tasks.map { x => schedule(x) }

  }

  /**
   * Adds a task to the scheduler's taskQ for consumption and then execution.
   */
  def schedule(task: SuspendableTask): Unit = {
    //    this.synchronized {
    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new DoSchedule(task.traceCopy)
      traceManager.current += te
    }

    if (!task.isSuspended)
      taskQ = taskQ :+ task

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }
    //    }
  }

  /**
   * Schedule a timed action that keeps triggering at least once in its life time.
   * @param timedAction the timed action that triggers its action each time its timer times-out
   */
  def schedulePeriodic(timedAction: RuntimeTimedAction): Unit = {
    //    this.synchronized {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new DoSchedulePeriodic(timedAction)
      traceManager.current += te
    }

    timedActionsTracker + timedAction

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }
    //    }
  }

  /**
   * Remove from the taskQ and adding tasks to consumeQ.
   */
  def consume: Unit = {
    require(!taskQ.isEmpty, "Scheduler.consume method - Can't consume from an empty taskQ")

    //    consumeQ.synchronized {
    //      taskQ.synchronized {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new Consume(taskQ.head.traceCopy)
      traceManager.current += te
    }

    consumeQ = consumeQ :+ taskQ.head
    taskQ = taskQ.tail

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }
    //      }
    //    }
  }

  def consumeAll: Unit = {
    taskQ.map { x => consumeQ = consumeQ :+ x; taskQ = taskQ.tail }
  }

  /**
   * Submits ONE task to be executed in the thread pool using the
   * StandardExecutionThread.
   *
   * The task is taken from the consumeQ if there exists one.
   */
  def execute(threadID: Long = 1): Unit = {
    require(!consumeQ.isEmpty)

    //    consumeQ.synchronized {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new ExecuteAny(consumeQ.head.traceCopy, threadID)
      traceManager.current += te
    }

    val task = consumeQ.head
    consumeQ = consumeQ.tail

    val t = StandardExecutionThread(task, threadID)
    t.start
    t.join

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }
    //    }
  }

  /**
   * calls execute() as many times as there are tasks in consumeQ to execute.
   */
  def executeAll(threadID: Long = 1): Unit = {
    //    this.synchronized {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new ExecuteAll(consumeQ.map { x => x.traceCopy })
      traceManager.current += te
    }

    // I want to execute them sequentially (one after the other, not in parallel).
    consumeQ foreach { x => val t = new StandardExecutionThread(x, threadID); t.start; t.join }

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }

    //    }
  }

  /**
   * Shuts down the thread pool backing this scheduler in an orderly
   * manner. That is after executing all submitted tasks.
   */
  def shutdown = {
    //    this.synchronized {
    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = ds.traceCopy
      te.event = new ShutdownScheduler
    }

    clk = 0 // abstract clock
    ds = null
    taskQ = Seq[SuspendableTask]()
    consumeQ = Seq[SuspendableTask]()
    // execute queue is already there inside the actual thread pool.
    tracingEnabled = false
    // futures and which agents are blocked by them (note in this case it isn't useable since ds is null)
    blockingMgr = new BlockedTasksManager
    // timed actions
    timedActionsTracker = new TimedActionsTracker
    // all that remains are traces in the traceMgr.

    //      tp.shutdown

    if (isTracingEnabled) {
      te.posterior = ds.traceCopy
      traceManager.current += TraceEntryEnd(te)
    }

    //    }
  }

  def isShudown: Boolean = {
    //    tp.isShutdown() &&
    clock() == 0 &&
      null == ds &&
      taskQ.isEmpty &&
      consumeQ.isEmpty &&
      tracingEnabled == false &&
      blockingMgr.blockedTasks.isEmpty &&
      timedActionsTracker.timedActions.isEmpty
  }

  /**
   * Automatically explores all schedules for a distributed
   * system associated with the used scheduler. As this is a
   * default and is only manual implementation, the method is
   * to be overridden by schedulers extending this one.
   * @return a sequence of multiple traces (each trace is a
   * sequence of TraceEntry objects)
   */
  def explore: Seq[Trace]

  override def toString: String = {
    "TaskQ = " + taskQ.toString + "\n" +
      "ConsumeQ = " + consumeQ.toString + "\n" +
      "Blocked Tasks = " + blockingMgr.blockedTasks.toString + "\n" +
      "Timed Actions = " + timedActionsTracker.toString
  }
  //=======================================
  // Tracing
  //=======================================
  def traceCopy: Scheduler = {
    Scheduler.fromJson(toJson)
  }

  def toJson: JValue = {
    (getClass.getSimpleName ->
      ("ds" -> ds.toJson) ~
      ("numThreads" -> numThreads) ~
      ("scheduingAlgorithm" -> schedulingAlgorithm.getClass.getSimpleName) ~
      ("clock" -> clk) ~
      ("taskQ" -> taskQ.map { x => x.toJson }) ~
      ("consumeQ" -> consumeQ.map { x => x.toJson }) ~
      ("blockingMgr" -> blockingMgr.toJson) ~
      ("timedActionsTracker" -> timedActionsTracker.toJson) ~
      ("traceManager" -> traceManager.toJson))
  }

  def snapshot: SchedulerState = {
    SchedulerState.snapshotState(this)
  }

  // Notes to developer
  /*
   * NOTES for developer:
   * 1- Figure how the scheduler will know that this scheduled task/stmt has to wait for a future to be resolved
   * 2- make use of the blockedAgentsManager to simplify the logic
   * 3- when a blocked agent is encountered:
   *  - pre-empt this agent
   *  - should all agents finished their processing except blocked agents, report a deadlock.
   *  - Should the blocked agents have no more agents, guarantee no-deadlocks
   *  - Should re-ordering of send operations cause different order of any agent's state mutation, report a data race.
   *  - other properties need more elaborate analyses, for later.
   * 
   */

}

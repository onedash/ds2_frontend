package edu.utah.cs.gauss.ds2.core.ir.datastructures

import java.util.UUID
import java.util.concurrent.Callable

import net.liftweb.json.DefaultFormats
import net.liftweb.json.JArray
import net.liftweb.json.JString
import net.liftweb.json.JValue
import net.liftweb.json.JsonDSL.jobject2assoc
import net.liftweb.json.JsonDSL.pair2Assoc
import net.liftweb.json.JsonDSL.pair2jvalue
import net.liftweb.json.JsonDSL.seq2jvalue
import net.liftweb.json.JsonDSL.string2jvalue

/**
 * @author
 *        	Mohammed S. Al-Mahfoudh <p>
 * 		   	mahfoudh@cs.utah.edu <p>
 * 		   	Gauss Group - SoC <p>
 * 		   	The University of Utah <p>
 *
 *
 * Note that this Action gets pupolated by the front-end with
 * send/ask calls as well as control flow encapsulating them.
 *
 * The control flow statements are to be developed soon.
 */
@SerialVersionUID(100)
class Action extends Callable[Unit] with Serializable {

  // read only payload and message to act upon, includes the sender
  var m: Message = new Message("dummy TWO VR Sets please")

  // the receiver, the agent whose state may change upon executing this action
  var a: Agent = _

  // original stmts seq non-modifiable
  var stmts = Seq[Statement]()

  // mutable sequences of statements to represent program counter
  var executed = Seq[Statement]()
  var toExecute = Seq[Statement]()

  var id = UUID.randomUUID

  var agentName: String = if (null == a) null else a.name

  reset

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {

    val mHash = if (null == m) 0 else m.hashCode

    //    agent isn't terminal, so I will avoid cyclic calls if removed from hashcode
    //    val aHash = if(null == a) 0 else a.hashCode
    val agentNameHash = if (null == agentName) 0 else agentName.hashCode

    mHash +
      stmts.hashCode +
      executed.hashCode +
      toExecute.hashCode +
      id.hashCode +
      agentNameHash
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }

  private def refreshAgentName: Unit = {
    if (null != a) agentName = a.name
    else if (agentName != null) agentName = agentName; // do nothing
    else agentName = null
  }
  //==============================

  def execute: Unit = {
    // all statements as one transaction
    stmts.synchronized {
      if (hasMore) {
        stmts foreach { _ => executeOne } // execute all statements              
      }
    }
  }

  /**
   * If action has more statements to execute, returns <code>true</code>. Otherwise, returns <code>false</code>
   *
   * @return <code>true</code> if there are more statements to execute, <code>false</code> otherwise
   */
  def hasMore: Boolean = {
    !toExecute.isEmpty
  }

  /**
   * Executes one statement of the action only, then stops. This method
   * needs to be called as many times as there are statements in the
   * Action.
   */
  def executeOne: Unit = {
    // no more than ONE stmt from this action to execute at a time
    this.synchronized {
      /*
       * 1- call the stmt function on the parameters
       * 2- advance the program counter
       */
      toExecute.head.apply
      advancePC
    }
  }

  /**
   * Careful NOT to use this one in any situation. This is only made public
   * to enable the basic scheduler's implementation.
   *
   * Before using this one, make sure you REALLY need to use it and there is
   * no way to "skip" the statement execution sitting in front of the <code>toExecute</code>
   * queue.
   */
  def advancePC: Unit = {
    this.synchronized {
      executed = executed :+ toExecute.head
      toExecute = toExecute.tail
    }
  }

  /**
   * Resets the <code>executed</code> and <code>toExecute</code>, but not <code>stmts</code>.
   *
   * This is a runtime reset, not a a compile time one. Good for simulating an agent crashing
   * in the middle of processing things, then restarting and executing again.
   */
  def reset: Unit = {
    executed = Seq[Statement]()
//    toExecute = stmts
    toExecute = stmts map { x => x }
    
//    stmts map { x => toExecute = toExecute :+ x }
  }

  def +(stmt: Statement): Action = {
//    val stmtCopy = stmt.copy // maybe we want to use the same statement in another action
//    stmtCopy.m = m
//    stmtCopy.a = a
//    
//    println("StmtCopy: "+ stmtCopy) 
//    stmtCopy.link(stmt.ds)
		  stmts = stmts :+ stmt
    stmt.m = m
    stmt.a = a
    
    
    stmt.parent = id // and this is why we need the copy, different parent actions
    reset
    this
  }

  def ++(stmts: Seq[Statement]): Action = {
//    this.stmts = this.stmts ++ stmts
    stmts map { x => this + x }
    this
  }
  
  /**
   * Removes the last statement if there is any from the action.
   *
   * @return the action without the last statment.
   */
  def -(): Action = {
    if (!stmts.isEmpty)
      stmts = stmts.slice(0, stmts.size - 1)

    this
  }

  def current: Statement = {
    toExecute.head
  }
  
  //=============================
  // Cloning
  //=============================
  /**
   * Used to get a copy of the "prototypes" defined in reactions. (please see the prototype
   * object-oriented design pattern to learn more). This method is critical for
   * suspending(blocking) then resuming an action. Without it, it is not possible to block
   * then resume an action.
   * @return the copy of the current action along with its current state
   */
  def runtimeCopy: Action = {

    val newAc = new Action
    // Message is a constant anyways but will copy
    newAc.m = m.copy

    newAc.a = a

    newAc.agentName = agentName

    newAc.executed = executed.map { x => x.setAction(this); x }
    newAc.toExecute = toExecute.map { x => x.setAction(this); x }
    newAc.stmts = stmts.map { x => x.setAction(this); x }

    // referenced since doesn't change since creation
    newAc.id = id // it is the same action, just a clone of it to keep the origin untouched
    
    newAc
  }

  def copy: Action = {

    val newAc = new Action
    // Message is a constant anyways but will copy
    newAc.m = m.copy

    newAc.a = a

    newAc.agentName = agentName

    newAc.stmts = stmts.map { x => x.copy }
    
    
    newAc.executed = newAc.stmts.slice(0,executed.size)
    newAc.toExecute = newAc.stmts.slice(executed.size, stmts.size)
    
//    newAc.executed = executed.map { x => x.copy }
//    newAc.toExecute = toExecute.map { x => x.copy }

    // referenced since doesn't change since creation
    newAc.id = id // it is the same action, just a clone of it to keep the origin untouched

    newAc
  }
  

  /**
   * Utility method to be used by the freshly made distributed system.
   *
   * Not intended to be used by any user code.
   *
   * @param ds the fresh copy of the distributed system
   */
  def link(ds: DistributedSystem): Unit = {
    /*
     * this may seem cryptic, but it only disconnects the old agent 
     * "reference" and replaces it with the new copy of the fresh 
     * distributed system copy
     * 
     */
    a =
      if (null != a && ds.contains(a.name)) ds.get(a.name)
      else if (null != agentName && ds.contains(agentName)) ds.get(agentName)
      else null

    // does the same for sender of the message
    if (null != m) m.link(ds)

    stmts map { x => x.link(ds); x.setAction(this) }
    executed map { x => x.link(ds); x.setAction(this) }
    toExecute map { x => x.link(ds); x.setAction(this) }

    setAgent(a)
    setMessage(m)
  }

  //=============================
  // Tracing support
  //=============================

  def traceCopy: Action = {
    Action.fromJson(toJson)
  }

  def toJson: JValue = {
    val mField = if (null == m) null else m.toJson
    val aName = if (null == a) null else a.name

    (getClass.getSimpleName ->
      ("id" -> id.toString) ~
      ("m" -> mField) ~
      ("a" -> null) ~
      ("stmts" -> stmts.map { x => x.toJson }) ~
      ("executed" -> executed.map { x => x.toJson }) ~
      ("toExecute" -> toExecute.map { x => x.toJson }) ~
      ("agentName" -> agentName))
  }

  //=============================
  // Utility methods
  //=============================

  def in(actions: Set[Action]): Boolean = {
    require(actions != null, "Action.in() method - doesn't accept null arguments")
    actions.find { x => x.id == id } != None
  }

  def in(behavior: Behavior): Boolean = {
    require(null != behavior, "Action.in(behavior) method - doesn't accept null arguments")
    //    behavior.reactions.values.toSet.contains(this)
    behavior.reactions.values.find { x => x.id == id } != None
  }

//  def in(ds: DistributedSystem): Boolean = {
//    require(null != ds, "Action.in(DS) method - doesn't accept null arguments")
//    ds.actions.find { x => x.id == id } != None
//  }

  def is(that: Action): Boolean = {
    id.equals(that.id)
  }

  def setAgent(a: Agent): Unit = {
    this.a = a
    stmts.map { x => x.a = a }
    executed.map { x => x.a = a }
    toExecute.map { x => x.a = a }
  }
  def setMessage(m: Message): Unit = {
    this.m = m
    stmts.map { x => x.m = m }
    executed.map { x => x.m = m }
    toExecute.map { x => x.m = m  }
  }

  override def toString: String = {
    "Action - " + id.toString + ": on message " + m + ", to execute = " + toExecute.size + " Statement(s)"
  }

  def call: Unit = {
    execute
  }

}

object Action {

  def apply: Action = {
    new Action
  }

  def fromJson(js: JValue): Action = {
    implicit val formats = DefaultFormats
    //    val act = js.extract[Action]
    val act = new Action

    act.id = js \ "Action" \ "id" match {
      case JString(x) => UUID.fromString(x)
      case _          => throw new Error("Action.fromJson - can't extract 'id'")
    }

    js \ "Action" \ "m" match {
      case null => act.m = null
      case x    => act.m = Message.fromJson(x)
    }

    js \ "Action" \ "agentName" match {
      case null       => act.agentName = null
      case JString(x) => act.agentName = x
      case _          => throw new Error("Action.fromJson - can't extract 'agentName'")
    }

    act.a = null // we can restore it from the completely restored DS by using its name

    js \ "Action" \ "stmts" match {
      case JArray(x) => act.stmts = x map { x => Statement.fromJson(x) }
      case _         => throw new Error("Action.fromJson - can't extract 'stmts'")
    }

    js \ "Action" \ "toExecute" match {
      case JArray(x) => act.toExecute = x map { x => Statement.fromJson(x) }
      case _         => throw new Error("Action.fromJson - can't extract 'stmts'")
    }

    js \ "Action" \ "executed" match {
      case JArray(x) => act.executed = x map { x => Statement.fromJson(x) }
      case _         => throw new Error("Action.fromJson - can't extract 'stmts'")
    }

    act
  }
}

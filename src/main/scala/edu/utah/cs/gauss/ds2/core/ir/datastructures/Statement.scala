package edu.utah.cs.gauss.ds2.core.ir.datastructures
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import java.util.concurrent.Callable
import java.util.UUID
import com.sun.corba.se.spi.protocol.LocalClientRequestDispatcher
import scala.Function2
import java.io._
import edu.utah.cs.gauss.serialization.IO.{ fromBytes, toBytes }
import scala.collection.parallel.ParSet
import scala.collection.parallel.ParSeq

/**
 * @author <br>
 * 	Mohammed S. Al-Mahfoudh <br/>
 * 	mahfoudh@cs.utah.edu <br/>
 * 	SoC - Gauss Group <br/>
 *
 * The user needs to override <code>code,m and a</code> of this class to place the body of the statement
 */
@SerialVersionUID(900)
class Statement(var code: Function2[Message, Agent, Unit] = (msg: Message, agnt: Agent) => {}) extends Serializable {
  import Statement.Kind._
  var startLineNo = BigInt(-1)
  var endLineNo = BigInt(-1)

  var m: Message = _ // scheduler
  var a: Agent = _ // static
  var agentName: String = if (null == a) null else a.name // static

  var id = UUID.randomUUID // static
  var parent: UUID = _ // static
  var askStmtID: UUID = _ // static or dynamic?
  var askParentID: UUID = _ // static or dynamic?

  var kind: Kind = NONE // default kind is free form statement (that is not DS one)
  var msgOut: Message = _ // dynamic

  var srcAgent: Agent = _ // dynamic
  var dstAgent: Agent = _ // dynamic
  var srcAgentName: String = _
  var dstAgentName: String = _

  var future: DummyFuture = _ // handled
  var timeout: BigInt = 0 // dynamic
  var behaviorName: String = _ // static
  var remember: Boolean = false // dynamic
  // for boot straping one or more
  var bootStrapped = Set[String]() // dynamic

  var variable: String = _ // static
  var value: Any = _ // dynamic
  // only set for get/timed-get statements, otherwise it is always null
  // also, no need to serialize/deserialize, we got its UUID, its the "parent"
  // only to support block/resume of the get/timed-get.
  var action: Action = _ // TAKEN-CARE_OF

  // Assigned on either call to DS.refresh or DS.link (called by DS.copy)
  var ds: DistributedSystem = _ // TAKEN-CARE_OF

  // by default, code is considered not instrumented till one of the instrumentation code is called
  var instrumented = false // static

  var args: Seq[String] = Seq()
  //TODO add control flow additional attributes: TWO sequences of statements, a boolean condition

  //==============================
  // future references updates
  // Observer OO Design Pattern
  // (i.e. subscribe, notify)
  //==============================
  //  var subscribers = Set[Statement]()
  //  def setFuture(future: DummyFuture) = {
  //    this.future = future;
  //    announce;
  //    setLocalState
  //  }
  //
  //  def setLocalState: Unit = {
  //    if (variable != null) {
  //
  //      var result: Option[DummyFuture] = None
  //
  //      var behaviors = Set[Behavior]()
  //      behaviors = behaviors + a.defaultBehavior
  //      behaviors = behaviors + a.reactions
  //      behaviors = behaviors + a.specialReactions
  //      a.behaviors.map { x => behaviors = behaviors + x._2 }
  //
  //      var theBehavior = behaviors filter { x =>
  //        x.reactions.values.exists {
  //          act =>
  //            act.id ==
  //              action.id
  //        }
  //      } head
  //
  //      a.localState(variable) = theBehavior.name + "[]" + m.name + "[]" + id.toString
  //    }
  //  }
  //
  //  def announce = {
  //    subscribers map { x =>
  //      x.future = future
  //      x.askParentID = parent
  //      x.askStmtID = id
  //    }
  //  }
  //  def subscribe(st: Statement) = { subscribers = subscribers + st }
  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    // I will ignore "code" and agents references for now to avoid cyclic hashCode calls (like in toJson)

    val mHash = if (null == m) 0 else m.hashCode

    val agentNameHash = if (null == agentName) 0 else agentName.hashCode

    val parentHash = if (null == parent) 0 else parent.hashCode
    val msgOutHash = if (null == msgOut) 0 else msgOut.hashCode

    val srcAgentNameHash = if (null == srcAgentName) 0 else srcAgentName.hashCode
    val dstAgentNameHash = if (null == dstAgentName) 0 else dstAgentName.hashCode
    val futureHash = if (null == future) 0 else future.hashCode
    val behaviorNameHash = if (null == behaviorName) 0 else behaviorName.hashCode

    kind.hashCode +
      startLineNo.hashCode +
      endLineNo.hashCode +
      id.hashCode +
      mHash +
      agentNameHash +
      parentHash +
      msgOutHash +
      srcAgentNameHash +
      dstAgentNameHash +
      futureHash +
      behaviorNameHash
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }

  def findFuture(agent: Agent): Option[DummyFuture] = {

    var result: Option[DummyFuture] = None

    var behaviors = Set[Behavior]()
    behaviors = behaviors + agent.defaultBehavior
    behaviors = behaviors + agent.reactions
    behaviors = behaviors + agent.specialReactions
    agent.behaviors.map { x => behaviors = behaviors + x._2 }

    var actions: ParSet[Action] = behaviors.par map { x => x.reactions.values } flatMap { x => x }

    var statements: ParSet[Statement] = actions map { x => x.stmts } flatMap { x => x } filter { x => x.kind == ASK }

    var theStatement: Option[Statement] = statements find { x => x.id == askStmtID }

    result = Some(theStatement.get.future)

    result
  }

  //==============================

  /**
   * Generates the code and assigns it to "code" field for static statements.
   */
  private def generateStaticCode: Unit = {

    kind match {
      case SEND =>
        code = (m: Message, a: Agent) => ds.send(srcAgent, msgOut, dstAgent)
      case ASK =>

        code = (m: Message, a: Agent) => {
          future = ds.ask(srcAgent, msgOut, dstAgent);
          srcAgent.setVariable(variable, future)
        }
      // case RESOLVE =>
      //   code = (m: Message, a: Agent) => {
      //     future = a.getVariable(variable).asInstanceOf[DummyFuture]
      //     ds.resolve(future, value)
      //   }
      case CREATE =>
        code = (m: Message, a: Agent) => ds.create(srcAgent, dstAgentName)
      case START =>
        code = (m: Message, a: Agent) => ds.start(srcAgent, dstAgent)
      case STOP =>
        code = (m: Message, a: Agent) => ds.stop(srcAgent, dstAgent)
      case KILL =>
        code = (m: Message, a: Agent) => ds.kill(srcAgent, dstAgent)
      case LOCK =>
        code = (m: Message, a: Agent) => ds.lock(srcAgent)
      case UNLOCK =>
        code = (m: Message, a: Agent) => { ds.unlock(srcAgent) }
      case STOP_CONSUME =>
        code = (m: Message, a: Agent) => ds.stopConsuming(srcAgent)
      case RESUME_CONSUME =>
        code = (m: Message, a: Agent) => ds.resumeConsuming(srcAgent)
      case BECOME =>
        code = (m: Message, a: Agent) => ds.become(srcAgent, behaviorName, remember)
      case UNBECOME =>
        code = (m: Message, a: Agent) => ds.unbecome(srcAgent)
      case STASH =>
        code = (m: Message, a: Agent) => ds.stash(srcAgent, msgOut)
      case UNSTASH =>
        code = (m: Message, a: Agent) => ds.unstash(srcAgent)
      case UNSTASH_ALL =>
        code = (m: Message, a: Agent) => ds.unstashAll(srcAgent)

      case GET =>
        //        future = findFuture(a).get // if it is None, there must be a mistake somewhere
        code = (m: Message, a: Agent) => {
          //DEBUG
          //          println("Statement.variable: " + variable)
          future = a.getVariable(variable).asInstanceOf[DummyFuture]
          ds.get(srcAgent, future, action)
        }
      case GET_TIMED =>
        //        future = findFuture(a).get // if it is None, there must be a mistake somewhere
        code = (m: Message, a: Agent) => {
          //DEBUG
          //          println("Statement.variable: " + variable)
          future = a.getVariable(variable).asInstanceOf[DummyFuture]
          ds.get(srcAgent, future, timeout, action)
        }

      case BOOT_STRAP =>
        code = (m: Message, a: Agent) => ds.bootStrap(dstAgent)
      case BOOT_STRAP_ALL =>
        code = (m: Message, a: Agent) => { val agents = bootStrapped map { x => ds.get(x) } toSet; ds.bootStrap(agents) }
      case MODIFY_STATE =>
        code = (m: Message, a: Agent) => dstAgent.localState(variable) = value
      case MODIFY_STATE_REF =>
        code = (m: Message, a: Agent) => dstAgent.localState(variable) = value // value is a string
      case NONE => ; // simply do nothing!
      // FIXME
      /*
       * We need to have a custom instrumentation constructor even for NONE statement kind:
       * - takes: m:Message , a:Agent, code: => Unit to execute.
       * - sets newStatement.m to m above, sets newStatement.a to a above, and sets code to
       * a new (m: Message, a:Agent) => code
       * 
       * yet the good news is that the "apply" method of that statement will refer to newStatement.m
       * and newStatement.a. So we keep the actual message+agent intact yet we can copy+link the statement
       * to the new distributed system!!!! YAY!
       */
      case x    => throw new Error("Statement.generateCode - can't generate this type of statements = " + x)
    }
  }

  /**
   * Generates the code and assigns it to "code" field for dynamic statements.
   */
  private def generateDynamicCode: Unit = {
    kind match {

      case SEND  =>

        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        msgOut = a.getVariable(args(1)).asInstanceOf[Message]
        dstAgent = a.getVariable(args(2)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.send(srcAgent, msgOut, dstAgent)
      case ASK =>

        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        msgOut = a.getVariable(args(1)).asInstanceOf[Message]
        dstAgent = a.getVariable(args(2)).asInstanceOf[Agent]
//        variable = a.getVariable(args(3)).asInstanceOf[String]
        
        code = (m: Message, a: Agent) => {
          future = ds.ask(srcAgent, msgOut, dstAgent);
          srcAgent.setVariable(args(3), future)
        }
//      case RESOLVE if (args.size == 2) =>
//        
//        variable = args(0)
//        
//        code = (m: Message, a: Agent) => {
//          future = a.getVariable(variable).asInstanceOf[DummyFuture]
//          ds.resolve(future, value)
//        }
      case CREATE =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        dstAgent = a.getVariable(args(2)).asInstanceOf[Agent]        
        
        code = (m: Message, a: Agent) => ds.create(srcAgent, dstAgentName)
      case START  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        dstAgent = a.getVariable(args(2)).asInstanceOf[Agent]        
        
        
        code = (m: Message, a: Agent) => ds.start(srcAgent, dstAgent)
      case STOP  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        dstAgent = a.getVariable(args(2)).asInstanceOf[Agent]        
        
        
        code = (m: Message, a: Agent) => ds.stop(srcAgent, dstAgent)
      case KILL  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        dstAgent = a.getVariable(args(2)).asInstanceOf[Agent]        
        
        
        code = (m: Message, a: Agent) => ds.kill(srcAgent, dstAgent)
      case LOCK  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]        
        
        code = (m: Message, a: Agent) => ds.lock(srcAgent)
      case UNLOCK  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => { ds.unlock(srcAgent) }
      case STOP_CONSUME  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.stopConsuming(srcAgent)
      case RESUME_CONSUME  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.resumeConsuming(srcAgent)
      case BECOME  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        behaviorName = a.getVariable(args(1)).asInstanceOf[String]
        if(args.size == 3)
          remember = a.getVariable(args(2)).asInstanceOf[Boolean]
        
        code = (m: Message, a: Agent) => ds.become(srcAgent, behaviorName, remember)
      case UNBECOME  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.unbecome(srcAgent)
      case STASH  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        msgOut = a.getVariable(args(1)).asInstanceOf[Message]
        
        code = (m: Message, a: Agent) => ds.stash(srcAgent, msgOut)
      case UNSTASH  =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.unstash(srcAgent)
      case UNSTASH_ALL =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.unstashAll(srcAgent)

      case GET =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        future = a.getVariable(args(1)).asInstanceOf[DummyFuture]
        
        
        code = (m: Message, a: Agent) => {
          future = a.getVariable(variable).asInstanceOf[DummyFuture]
          ds.get(srcAgent, future, action)
        }
      case GET_TIMED =>
        
        srcAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        future = a.getVariable(args(1)).asInstanceOf[DummyFuture]
        timeout = a.getVariable(args(2)).asInstanceOf[BigInt]
        
        code = (m: Message, a: Agent) => {
          future = a.getVariable(variable).asInstanceOf[DummyFuture]
          ds.get(srcAgent, future, timeout, action)
        }

      case BOOT_STRAP  =>
        
        dstAgent = a.getVariable(args(0)).asInstanceOf[Agent]
        
        code = (m: Message, a: Agent) => ds.bootStrap(dstAgent)
      case BOOT_STRAP_ALL  =>
        
        bootStrapped = a.getVariable(args(0)).asInstanceOf[Set[String]]
        
        code = (m: Message, a: Agent) => { val agents = bootStrapped map { x => ds.get(x) } toSet; ds.bootStrap(agents) }
      case MODIFY_STATE  =>
        
        variable = a.getVariable(args(0)).asInstanceOf[String]
        value = a.getVariable(args(1))
        
        code = (m: Message, a: Agent) => dstAgent.localState(variable) = value
      case MODIFY_STATE_REF  =>
        
        variable = a.getVariable(args(0)).asInstanceOf[String]
        value = a.getVariable(args(1))
        
        code = (m: Message, a: Agent) => dstAgent.localState(variable) = value // value is a string

      //TODO move IF_ELSE and WHILE to a new apply method
      //      case IF_ELSE =>
      //      case WHILE   =>
      case NONE => ; // simply do nothing!
      case x    => throw new Error("Statement.generateCode - can't generate this type of statements = " + x)
    }

  }

  def apply: Unit = {
    if (instrumented && args.size == 0)
      generateStaticCode
    else if (instrumented)
      generateDynamicCode

    code.apply(m, a)

  }

  def copy: Statement = {

    // here i assumed that primitive types of java passed by copy 
    // (although are objects of classes in scala), I observed some 
    // behavior like this!

    // So, unless it doesn't work, we assume it will work for now, 
    // otherwise i have a workaround

    //Nevertheless, a "Statement" is always static! executes but carries no runtime-state.
    // all the state it carries is meta data about static time analysis
    val newOne = Statement(code)
    newOne.ds = ds

    newOne.startLineNo = startLineNo
    newOne.endLineNo = endLineNo

    newOne.m = if (null == m) null else m.copy
    newOne.a = a
    newOne.agentName = agentName

    newOne.id = id
    newOne.parent = parent
    newOne.askStmtID = askStmtID
    newOne.askParentID = askParentID

    newOne.kind = kind
    //    newOne.generateCode // this won't have any effect if the "kind" is "NONE".

    newOne.msgOut = if (null != msgOut) msgOut.copy else null
    newOne.srcAgent = srcAgent
    newOne.dstAgent = dstAgent
    newOne.srcAgentName = srcAgentName
    newOne.dstAgentName = dstAgentName

    newOne.future = if (null == future) null else future.copy
    newOne.timeout = timeout
    newOne.behaviorName = behaviorName
    newOne.remember = if (remember) true else false
    newOne.bootStrapped = bootStrapped map { x => x }
    newOne.instrumented = if (instrumented) true else false

    newOne.variable = variable
    newOne.value = fromBytes[Any](toBytes(value))
    //    newOne.action = if (null != action) action.copy else null

    //    if (kind == ASK)
    //      subscribers map { x => newOne.subscribers = newOne.subscribers + x }

    newOne
  }

  def link(ds: DistributedSystem): Unit = {

    /*
     * disconnects the old agents 
     * "references" and replaces it with the new copy of the fresh 
     * distributed system copy
     * 
     */

    this.ds = ds

    a =
      if (null != a && ds.contains(a.name)) ds.get(a.name)
      else if (null != agentName && ds.contains(agentName)) ds.get(agentName)
      else null

    if (null != m) m.link(ds)

    if (null != msgOut) msgOut.link(ds)

    // disconnect old agent, reconnect new
    srcAgent =
      if (null != srcAgent && ds.contains(srcAgent.name)) ds.get(srcAgent.name)
      else if (srcAgentName != null && ds.contains(srcAgentName)) ds.get(srcAgentName)
      else null

    // disconnect old agent, reconnect new
    dstAgent =
      if (null != dstAgent && ds.contains(dstAgent.name)) ds.get(dstAgent.name)
      else if (dstAgent != null && ds.contains(dstAgentName)) ds.get(dstAgentName)
      else null

    //    if (kind == GET || kind == GET_TIMED || kind == RESOLVE)
    //      future = findFuture.get // if it is None, there must be a mistake somewhere  

    if (null != future) future.link(ds)

    //    generateCode // this won't have any effect if the "kind" is "NONE".

    //      def updateSubscribersLis: Unit = {
    //        if (kind == ASK) { // Only ASK statement needs to do this when copied then linked
    //          /*
    //         * NOTE: By link time, all statements should have been copied 
    //         * (not necessarily linked but we don't care about that 
    //         * during link, since its the link job to do so)
    //         *
    //         * STEPS:
    //         * 1- search each subscribing statement by ID (in ALL agents' behaviors)
    //         * 		1.1 - collect all behaviors, in a parallel set
    //         * 		1.2 - isolate all their actions
    //         * 		1.3 - flatten the actions to statements collection
    //         * 		1.4 - filter statements by "id", if "id" is in set of "id"s of "subscribers"
    //         * 2- update subscriber's list to contain the up to date copy of statements subscribed
    //         * 		2.1 - get rid of old subscribers, add those statements to subscribers.
    //         */
    //
    //          var behaviors = Seq[Behavior]().par
    //          ds.agents map {
    //            x =>
    //              behaviors = behaviors :+ x.reactions
    //              behaviors = behaviors :+ x.defaultBehavior
    //              behaviors = behaviors :+ x.specialReactions
    //              behaviors = behaviors ++ x.behaviors.values
    //          } // first map
    //
    //          val actions: ParSet[Action] = (behaviors map { x => x.reactions.values } flatten).toSet
    //          val statements: ParSet[Statement] = actions map { x => x.stmts } flatten
    //          val statementsIds: ParSet[UUID] = statements map { x => x.id }
    //
    //          val subscribersIDs = subscribers map { x => x.id } par
    //
    //          //        val newSubscribers: ParSet[Statement] = statements filter { x => subscribersIDs.forall { x.id == _ } }
    //          val newSubscribers: ParSet[Statement] = statements filter { x => subscribersIDs.contains(x.id) }
    //
    //          subscribers = newSubscribers.seq.toSet
    //        } // if
    //      } // method
    //
    //    updateSubscribersLis // the call of the method that will ONLY execute if kind = ASK

  } // link

  def setAction(act: Action): Unit = {
    if (null != future)
      action = act
  }

  def traceCopy: Statement = {
    Statement.fromJson(toJson)
  }
  def toJson: JValue = {
    val mField = if (null == m) null else m.toJson

    val stmtParent = if (null == parent) null else parent.toString

    //    val codeBytes = List((toBytes(code)  map { x => JInt(x) }))

    val mOut = if (null == msgOut) null else msgOut.toJson
    val theFuture = if (null == future) null else future.toJson

    val srcAgntName = if (null == srcAgent) null else srcAgent.name
    val dstAgntName = if (null == dstAgent) null else dstAgent.name
    val ASKStmtID = if (null == askStmtID) null else askStmtID.toString
    val ASKParentID = if (null == askParentID) null else askParentID.toString

    agentName = if (null == a) null else a.name

    (getClass.getSimpleName ->
      ("m" -> mField) ~
      ("agentName" -> agentName) ~
      //      ("codeBytes" -> codeBytes) ~
      ("startLineNo" -> startLineNo) ~
      ("endLineNo" -> endLineNo) ~
      ("id" -> id.toString) ~
      ("parent" -> stmtParent) ~
      ("askStmtID" -> ASKStmtID) ~
      ("askParentID" -> ASKParentID) ~
      ("kind" -> kind.toString) ~
      ("msgOut" -> mOut) ~
      ("srcAgentName" -> srcAgntName) ~
      ("dstAgentName" -> dstAgntName) ~
      ("future" -> theFuture) ~
      ("timeout" -> timeout) ~
      ("behaviorName" -> behaviorName) ~
      ("remember" -> remember) ~
      ("bootStrapped" -> bootStrapped) ~
      ("variable" -> variable) ~
      ("value" -> (toBytes(value) map { x => JInt(x) })) ~
      ("instrumented" -> instrumented))
  }

  def in(act: Action): Boolean = {
    require(null != act, "Statement.in(action) method - doesn't accept null action argument")
    require(null != parent, "Statement.in(action) method - doesn't accept null parent")
    parent == act
  }

  def in(stmts: Seq[Statement]): Boolean = {
    require(stmts != null, "Statement.in(stmts) method - doesn't accept null arguments")
    stmts.exists { x => x == this }
  }

  override def toString: String = {
    val agentStr = if (null == a) null else a.name
    val msgStr = if (null == m) null else m.name

    "\n=> STATEMENT - " + kind + " - id = " + id.toString + "\n" +
      "Message = " + msgStr +
      ", Agent = " + agentStr +
      ", start line = " + startLineNo +
      ", end line = " + endLineNo
  }

  def is(that: Statement): Boolean = {
    id == that.id && parent == that.parent
  }

}

// TODO add control-flow support after testing the basic implementation, not to complicate things now.
// IDEA: put nice functions inside "Action" class to model control flow based on some expression eval, Expr passed by name.
//class IfElse[T <: Message](cond: => Boolean, var then: Statement[T], var els: Statement[T]) extends Statement
//class While[T <: Message](cond: => Boolean, var body: Statement[T], var els: Statement[T]) extends Statement

object Statement {

  object Kind extends Enumeration {
    type Kind = Value
    val NONE, SEND, ASK, // RESOLVE,
        CREATE, START, STOP, KILL, LOCK, UNLOCK, STOP_CONSUME, RESUME_CONSUME, BECOME, UNBECOME, STASH, UNSTASH, UNSTASH_ALL, GET, GET_TIMED, BOOT_STRAP, BOOT_STRAP_ALL, MODIFY_STATE, MODIFY_STATE_REF, IF_ELSE, WHILE = Value
  }

  import Kind._

  /**
   * Generic non-instrumented statement that is NOT one of our distributed system's specification methods.
   * @param code the code to execute, must NOT contain any DistributedSystem methods listed in Kind object.
   * @return
   */
  @Deprecated
  def apply(code: => (Message, Agent) => Unit): Statement = {
    val stmt = new Statement
    stmt.code = code
    stmt.instrumented = false
    stmt
  }

  //  /**
  //   * This is the new recommended way of creating a NONE type statement.
  //   * @param m The message on which the <code>code</code> is applied on.
  //   * @param a
  //   * @param code
  //   * @return
  //   */
  //  def apply(m: Message, a: Agent, code: => (Message, Agent) => Unit): Statement = {
  //    val stmt = new Statement
  //    stmt.m = m
  //    stmt.a = a
  //    stmt.code = code
  //    stmt.instrumented = false
  //    stmt
  //  }

  /**
   * This statement constructs a send/ask statements
   * @param kind SEND or ASK to specify what statement should be returned.
   * @param src
   * @param m
   * @param dst
   * @param varName 
   * @return the constructed and instrumented statement with a future (if the statement is ASK), with None if its SEND.
   */
  def apply(kind: Kind, src: Agent, m: Message, dst: Agent, varName: String = null): Statement = {
    require(kind == SEND || kind == ASK, "Statement.apply - this method doesn't accept kind = " + kind)

    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = kind
    stmt.srcAgent = src
    stmt.srcAgentName = stmt.srcAgent.name
    stmt.msgOut = m
    stmt.dstAgent = dst
    stmt.dstAgentName = stmt.dstAgent.name

    if (kind == ASK)
      stmt.variable = varName

    stmt
  }

  /**
   * This apply method, creates ANY kind statements
   * with dynamically determined arguments based on
   * variable name.
   *
   * Each String states where the entity is stored in the
   * agent local-state that contains this statement.
   *
   * Depending on the kind argument, the number of
   * variable length argument list should agree with
   * the following specification:
   * <ul>
   * <li>SEND: src-agent, msg, dst-agent </li>
   * <li>ASK: src-agent, msg, dst-agent, future-var-name</li>
   * <li>CREATE: creator-agent, created-agent-name</li>
   * <li>START: starter-agent, started-agent</li>
   * <li>STOP: stopper-agent, stopped-agent</li>
   * <li>KILL: killer-agent, killed-agent</li>
   * <li>LOCK: agent-to-lock</li>
   * <li>UNLOCK: agent-to-unlock</li>
   * <li>STOP_CONSUME: agent-to-stop-consuming</li>
   * <li>RESUME_CONSUME: agent-to-resume-consuming</li>
   * <li>BECOME: agent-changing-behavior, behavior-name, remember</li>
   * <li>UNBECOME: agent-changing-behavior</li>
   * <li>STASH: agent-stashing-a-msg</li>
   * <li>UNSTASH: agent-unstashing-a-message</li>
   * <li>UNSTASH_ALL: agent-unstashing-all-msgs</li>
   * <li>GET: future-var-name, var-name-where-value-is-stored</li>
   * <li>GET_TIMED: future-var-name, var-name-where-value-is-stored, and time-out-var-name</li>
   * <li>BOOT_STRAP_ALL: var-name where a sequence of agent-names is</li>
   * <li>MODIFY_STATE: var-name-to-modify, var-name-of-value</li>
   * <li>MODIFY_STATE_REF: var-name-to-store-reference, another-var-name-to-reference</li>
   * <li>IF_ELSE: cond-var-name, then-seq-stmts-var-name, else-seq-stmts-var-name</li>
   * <li>WHILE: cond-var-name, seq-stmts-var-name</li>
   * <li>FUNCTION: var-name-dictionary-params</li>
   * <ul>
   *
   *
   * @param kind the kind of statement as stated above.
   * @param args the arguments of the statement as stated above
   * @return
   */
  def apply(kind: Kind, args: String*): Statement = {
    require(Kind.values.exists { x => x == kind }, "Statement.apply-dynamic-args - kind is not defined: " + kind)
    val size = args.size
    if (size == 1 && Set(
      LOCK,
      UNLOCK,
      STOP_CONSUME,
      RESUME_CONSUME,
      UNBECOME,
      UNSTASH,
      UNSTASH_ALL,
      BOOT_STRAP,
      BOOT_STRAP_ALL).contains(kind)) { ; }
    else if (size == 2 && Set(
      CREATE,
      START,
      STOP,
      KILL,
      BECOME,
      STASH,
      GET,
      MODIFY_STATE,
      MODIFY_STATE_REF).contains(kind)) { ; }
    else if (size == 3 && Set(
      SEND,
      GET_TIMED,
      BECOME).contains(kind)) { ; }
    else if (size == 4 & ASK == kind) { ; }
    else
      throw new Error("Statement.apply-dynamic - no statement with such number of args: " + args.size + " with kind: " + kind)

    var stmt = new Statement()
    stmt.args = args
    stmt.kind = kind
    stmt
  }

  // /**
  //  * a resolve OR modify-state with value statement
  //  * @param f
  //  * @param value
  //  * @return
  //  */
  // def apply(kind: Kind, variableName: String, value: Any): Statement = {

  //   val stmt = new Statement
  //   stmt.instrumented = true
  //   stmt.kind = kind
  //   stmt.variable = variableName
  //   stmt.value = value
  //   //    askStmt.subscribe(stmt)

  //   stmt
  // }

  /**
   * MODIFY_STATE with reference
   * @param variableName the variable to set
   * @param referencedVarName the variable we reference
   * @return
   */
  def apply(variableName: String, referencedVarName: String): Statement = {

    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = MODIFY_STATE_REF
    stmt.variable = variableName
    stmt.value = referencedVarName
    //    askStmt.subscribe(stmt)

    stmt
  }

  /**
   * creates a GET/CREATE statements
   * @param kind accepts GET or CREATE
   * @param agent getting the future or creating a new agent
   * @param name of future to get, or agent to create
   * @return
   */
  def apply(kind: Kind, src: Agent, variableNameORNewAgentNAme: String): Statement = {

    kind match {
      case GET => createGetStatement(src, variableNameORNewAgentNAme)
      case CREATE =>
        val stmt = new Statement
        stmt.instrumented = true
        stmt.kind = CREATE
        stmt.srcAgent = src
        stmt.dstAgentName = variableNameORNewAgentNAme

        stmt
      case _ => throw new Error("Statement.apply(GET/CREATE) - accepts only GET or CREATE kind, found: " + kind)
    }

  }

  /**
   * Start/stop/kill statement, depends on what you pass for the first argument.
   * @param kind can be START/STOP/KILL
   * @param doer starter/stopper
   * @param subject started/stopped
   * @return
   */
  def apply(kind: Kind, doer: Agent, subject: Agent): Statement = {
    require(kind == START || kind == STOP || kind == KILL, "Statement.apply - this method doesn't accept kind = " + kind)

    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = kind
    stmt.srcAgent = doer
    stmt.dstAgent = subject

    stmt
  }

  /**
   * Lock/Unlock/stop-consuming/resume/consuming/Unbecome/Unstash/Unstash-all/boostrap statement.
   * @param kind
   * @param agent
   * @return
   */
  def apply(kind: Kind, agent: Agent): Statement = {
    require(
      kind == LOCK ||
        kind == UNLOCK ||
        kind == STOP_CONSUME ||
        kind == RESUME_CONSUME ||
        kind == UNBECOME ||
        kind == UNSTASH ||
        kind == UNSTASH_ALL ||
        kind == BOOT_STRAP, "Statement.apply - this method doesn't accept kind = " + kind)

    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = kind
    stmt.srcAgent = agent

    stmt
  }

  /**
   * Become statement
   * @param kind
   * @param agent
   * @param behavior
   * @param remember
   */
  def apply(kind: Kind, agent: Agent, behavior: String, remember: Boolean): Statement = {
    require(kind == BECOME, "Statement.apply - this method doesn't accept kind = " + kind)

    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = kind
    stmt.srcAgent = agent
    stmt.behaviorName = behavior
    stmt.remember = remember

    stmt
  }

  /**
   * Bootstrap all
   * @param agents agents to be boot strapped
   * @return
   */
  def apply(agents: Set[Agent]): Statement = {
    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = BOOT_STRAP_ALL
    stmt.bootStrapped = agents map { x => x.name }

    stmt
  }

  /**
   * Modify local state statement. Any modification of actor's local state must be encapsulated in this statement type.
   * @param agent
   * @param variable
   * @param value
   * @return
   */
  def apply(agent: Agent, variable: String, value: Any): Statement = {
    val stmt = new Statement
    stmt.instrumented = true
    stmt.dstAgent = agent
    stmt.variable = variable
    stmt.value = value

    stmt
  }

  //FIXME Add  with computation constructor.
  /*
   * A statement that does ONE modification for Agent.localState BUT
   * has computation inside of it, possibly done to that changed local 
   * state var.
   */

  /**
   * Get statement
   * @param asker
   * @param f
   * @return
   */
  private def createGetStatement(asker: Agent, variableName: String): Statement = {
    //  def apply(asker: Agent, askStmtID: UUID): Statement = {
    // FIXME the action we get is a fresh one from the reactions, how can we let the "execution thread"
    // to pass thet to statement?
    // Oh i think i answered that already! let the "execution thread" set the action in the 'action' field of the statement!

    /*
   * How to set the action?
   * 
   * - get the "parent" ID
   * - get that action with "parent" ID from the Agent's "current behavior"
   * - That is the action to be associated with this.
   * 
   * How is this done during runtime?!! We have to get the action instance
   * that is TO BE RESUMED, so must be set only ON-CALL of the function, as 
   * well as to be noted in one of this stetement's instrumentation variables.
   */
    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = GET
    stmt.srcAgent = asker
    stmt.variable = variableName
    //    askStmt.subscribe(stmt)
    //    stmt.askStmtID = askStmt.id
    //    stmt.askParentID = askStmt.parent

    // this must be set by the "execution thread" ==> StandardExecutionThread needs modification to do that.
    //    stmt.action = stmt.srcAgent.reactions(stmt.m)

    stmt
  }

  /**
   * Get-timed statement
   * @param asker
   * @param f
   * @param timeout
   * @return
   */
  def apply(asker: Agent, variableName: String, timeout: BigInt): Statement = {
    //    val stmt = apply(asker, variableName)
    val stmt = new Statement
    stmt.instrumented = true
    stmt.kind = GET
    stmt.srcAgent = asker
    stmt.variable = variableName
    stmt.kind = GET_TIMED
    stmt.timeout = timeout

    stmt
  }

  def fromJson(json: JValue): Statement = {

    // STATEMENT
    implicit val formats = DefaultFormats
    //    val stmt = Statement(code)
    val stmt = Statement((m: Message, a: Agent) => {})

    //MESSAGE
    json \ "Statement" \ "m" match {
      case x: JObject => stmt.m = Message.fromJson(x)
      case null       => stmt.m = null
      case _          => throw new Error("Statement.fromJson - Couldn't extract message")
    }

    // AGENT
    stmt.a = null

    // agentName
    json \ "Statement" \ "agentName" match {
      case JString(x) => stmt.agentName = x
      case null       => stmt.agentName = null
      case _          => throw new Error("Statement.fromJson - Couldn't extract agentName")
    }

    // start Line No
    json \ "Statement" \ "startLineNo" match {
      case JInt(x) => stmt.startLineNo = x
      case _       => throw new Error("Statement.fromJson - Couldn't extract startLineNo")
    }

    // end line no
    json \ "Statement" \ "endLineNo" match {
      case JInt(x) => stmt.endLineNo = x
      case _       => throw new Error("Statement.fromJson - Couldn't extract endLineNo")
    }

    // ID
    json \ "Statement" \ "id" match {
      case JString(x) => stmt.id = UUID.fromString(x)
      case _          => throw new Error("Statement.fromJson - there can't be a statement without a UUID")
    }

    // parent action
    json \ "Statement" \ "parent" match {
      case null          => stmt.parent = null
      case JString(null) => stmt.parent = null
      case JString(x)    => stmt.parent = UUID.fromString(x)
      case _             => throw new Error("Statement.fromJson - couldn't extract the statement's parent UUID")
    }

    // ask statment id that returns the future I am GET-ing
    json \ "Statement" \ "askStmtID" match {
      case null          => stmt.askStmtID = null
      case JString(null) => stmt.askStmtID = null
      case JString(x)    => stmt.askStmtID = UUID.fromString(x)
      case _             => throw new Error("Statement.fromJson - couldn't extract the statement's ASK statment UUID")
    }

    // ask statment parent id that returns the future I am GET-ing
    json \ "Statement" \ "askParentID" match {
      case null          => stmt.askParentID = null
      case JString(null) => stmt.askParentID = null
      case JString(x)    => stmt.askParentID = UUID.fromString(x)
      case _             => throw new Error("Statement.fromJson - couldn't extract the statement's ASK Parent UUID")
    }

    val kinds = Kind.values map { x => x.toString }
    json \ "Statement" \ "kind" match {
      case JString(x) if (kinds.contains(x)) => stmt.kind = Kind.withName(x)
      case _                                 => throw new Error("Statement.fromJson - can't extract 'kind'")
    }

    json \ "Statement" \ "msgOut" match {
      case null       => stmt.msgOut = null
      case x: JObject => stmt.msgOut = Message.fromJson(x)
      case _          => throw new Error("Message.fromJson - can't extract 'msgOut'")
    }

    json \ "Statement" \ "future" match {
      case null       => stmt.future = null
      case x: JObject => stmt.future = DummyFuture.fromJson(x)
      case _          => throw new Error("Message.fromJson - can't extract 'resolvedFuture'")
    }

    json \ "Statement" \ "timeout" match {
      case JInt(x) => stmt.timeout = x
      case _       => throw new Error("Message.fromJson - can't extract 'timeout'")
    }

    json \ "Statement" \ "srcAgentName" match {
      case JString(x) => stmt.srcAgentName = x
      case null       => stmt.srcAgentName = null
      case _          => throw new Error("Statement.fromJson - can't extract 'srcAgentName'")
    }

    json \ "Statement" \ "dstAgentName" match {
      case JString(x) => stmt.dstAgentName = x
      case null       => stmt.dstAgentName = null
      case _          => throw new Error("Statement.fromJson - can't extract 'dstAgentName'")
    }

    json \ "Statement" \ "behaviorName" match {
      case JString(x) => stmt.behaviorName = x
      case null       => stmt.behaviorName = null
      case _          => throw new Error("Statement.fromJson - can't extract 'behaviorName'")
    }

    json \ "Statement" \ "remember" match {
      case JBool(x) => stmt.remember = x
      case _        => throw new Error("Statement.fromJson - can't extract 'remember'")
    }

    json \ "Statement" \ "bootStrapped" match {
      case JArray(x) => stmt.bootStrapped = x map { y => y.toString } toSet
      case _         => throw new Error("Statement.fromJson - can't extract 'bootStrapped'")
    }

    json \ "Statement" \ "variable" match {
      case JString(x) => stmt.variable = x
      case _          => throw new Error("Statement.fromJson - can't extract 'variable'")
    }

    json \ "Statement" \ "value" match {
      case null => stmt.value = null
      case JArray(x) => stmt.value = fromBytes(x map { x =>
        x match {
          case JInt(x) => x.toByte
          case _       => throw new Error("Statement.fromJson - can't be the case that a non JInt is in the seq!")
        }
      })
      case _ => throw new Error("Statement.fromJson - can't extract 'value'")
    }

    json \ "Statement" \ "instrumented" match {
      case JBool(x) => stmt.instrumented = x
      case _        => throw new Error("Statement.fromJson - can't extract 'instrumented'")
    }

    stmt
  }

}

package edu.utah.cs.gauss.ds2.core.ir.datastructures

import scala.concurrent.duration.Duration
import scala.concurrent.Future
import java.util.concurrent.Executor
import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import edu.utah.cs.gauss.ds2.core.schedulers._
import scala.collection.mutable.Stack
import scala.collection.parallel.ParSet
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import edu.utah.cs.gauss.ds2.core.tracing._
import scala.collection.parallel.ParSeq
import java.util.UUID

/**
 * @author
 *        Mohammed S. Al-Mahfoudh <p>
 * 		   	mahfoudh@cs.utah.edu <p>
 * 		   	Gauss Group - SoC <p>
 * 		   	The University of Utah <p>
 */
@SerialVersionUID(500)
class DistributedSystem(var name: String = "ds") extends Serializable {
  // A
  var agents = Set[Agent]()
  // M
  var messages = Set[Message]()
  // Gamma
  //  var actions = Set[Action]()
  // Delta 
  //  var behaviors = Map[String, Behavior]()
  // Chi, encoded in a finer grain manner in each statement object
  //  var actionToMessageSent: ActionsToSends = new ActionsToSends

  var scheduler: Scheduler = Scheduler(this, Scheduler.Basic) // default but can be changed

  private var tracingEnabled = false

  //==============================
  // Future Handling additions
  //==============================
  var temporaries: ParSet[String] = ParSet()

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    name.hashCode +
      agents.hashCode +
      messages.hashCode +
      temporaries.hashCode
    //      + actions.hashCode 
    //      + behaviors.hashCode
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }

  //===================
  // init code
  //===================
  val bootStraper = new Agent("boot")
  bootStraper.locked = false

  //===================
  // implicits for cleaner code
  //===================

  implicit val ds = this

  implicit val clock: Function0[BigInt] = () => scheduler.clock()

  //===================
  // Populating special Messages
  //===================

  messages += new Start
  messages += new Stop
  messages += new Leave
  messages += new Demise
  messages += new LinkFailed
  messages += new Join
  messages += new ReJoin
  messages += new PoisonPill
  messages += new Message

  // ResolveDummyFuture is hard to add and verify since it has a dynamic field, so it is a special case anyways

  //==========================
  // Special Commands 
  // (communication)
  //==========================

  def send(src: Agent, m: Message, dst: Agent, special: Boolean = false): Unit = {
    this.synchronized {

      require(src != null, "Send method - src agent is null")
      require(dst != null, "Send method - dst agent is null")
      require(m != null, "Send method - message to send is null")

      require((src in this) || src.name == bootStraper.name, "Send method - the src agent is not in the distributed system")
      require((dst in this) || src.name == bootStraper.name, "Send method - the dst agent is not in the distributed system")
      // note that messages can be created on the fly so no 
      // guarantees the are in messages or not. We only have 
      // to make sure all "types"+"names" of messages are in "messages'

      if (!m.isInstanceOf[ResolveDummyFuture])
        require(m in this, "Send method - Message (" + m.name + ") is not known for this distributed system")

      if (!special)
        require(!dst.locked, "Send Method - the destination agent incoming queue is locked.")

      val te = new TraceEntry
      if (isTracingEnabled) {
        src.name match {
          case "boot" => te.event = new Send(bootStraper.traceCopy, m, dst.traceCopy, special)
          case _ => te.event = new Send(src.traceCopy, m, dst.traceCopy, special)
        }

        te.prior = traceCopy
      }

      // Read the note at the bottom to know why is this necessary.
      if (src.name == bootStraper.name)
        //        m.sender = bootStraper
        m.setSender(bootStraper)
      else {
        //        m.sender = src
        m.setSender(src)
      }

      // Error handling
      if (dst.locked && !special)
        throw new Error("Send-method - locked agent - Communication error, can't send to agent: " + dst.name)

      //actual work done
      if (temporaries.contains(dst.name)) // a resolving send
      {

        /*
       * STEPS:
       * 1- First check the name of the "dst" actor against "temporaries"
       * 		1.1- if it is NOT there, then it is simply a non-resolving send. Proceed normally
       * 		1.2- if it is there:
       * 					a - call "resolve" method on the actual destination: ds.get(dst.name.split("\\[\\]")(0))
       * 						  setting the resolver to the "real" src, and future to the future found from the real 
       * 						  destination's "waitingFor" futures using the dst.name.split("\\[\\]")(1).
       * 					b - remove the temporary from "temporaries", it has done its life-task.
       * 2- Things should proceed normally from both Scheduler and dst.
       * 
       * NOTE: This send() can either act as RESOLVE or as regular SEND as before, DO NOT assume it 
       * works as both at the same time. Make sure it takes either path, not both, in one run.
       * 
       * Why? because a resolve calls send, and if send calls resolve, then infinite loop. Also, to avoid
       * duplicating a message in the dst buffer, in case no infinite loops.
       */

        // STEP 1.2

        val segments = dst.name.split("\\[\\]")
        val agent = get(segments(0))
        val future = agent.futuresWaitingFor(UUID.fromString(segments(1)))
        temporaries = temporaries - (dst.name) // step 1.2.b

        // We assume the value is at the head of the payload of the message
        resolve(future, m.payload.head) // step 1.2.a

        agents = agents - dst // send and ask checks if agents are there (temporary agents for futures)

        /*
         * NOTE:
         * "future" contains both real dst and real src. This is done by ask(...)
         */
      }
      else // non-resolving, normal, send
        // STEP 1.1
        m match {
          case x: ResolveDummyFuture => // system message treated specially
            if (dst.q.lastIndexWhere { x => x.isInstanceOf[ResolveDummyFuture] } == -1)
              dst.q = m +: dst.q // insert in front of the queue since this is a system msg
            else {
              val slice1 = dst.q.slice(0, dst.q.lastIndexWhere(_.isInstanceOf[ResolveDummyFuture]))
              val slice2 = dst.q.slice(dst.q.lastIndexWhere(_.isInstanceOf[ResolveDummyFuture]), dst.q.size - 1)
              dst.q = slice1 :+ m // add message to first slice
              dst.q = dst.q ++ slice2 // add the remaining elements
            }
          case _ => dst.q = dst.q :+ m
        }

      if (isTracingEnabled) {
        te.posterior = traceCopy
        scheduler.traceManager.current += te
        scheduler.traceManager.current += TraceEntryEnd(te)
      }
    }

    /*
     * Some explanation:
     * Why is the methods here use get(agent.name) instead of the passed agent?
     * 
     * This has to do with Copying.
     * 
     * Note that after we copy a DistributedSystem, yet keep the same code in the statements, it will refer to
     * older entities, except for the DS itself. Entities include src and dst agents for example in the send().
     * 
     * Other methods of course need to be aware of this too, and possibly more entities from old vs new 
     * distributed system and keep referring to what is in the "current" distributed system.
     * 
     */

  }

  def ask(src: Agent, m: Message, dst: Agent, special: Boolean = false): DummyFuture = {
    this.synchronized {

      require(src != null && dst != null && m != null, "Ask method - the src is null")
      require(dst != null && m != null, "Ask method - the dst is null")

      require(src in this, "Ask method - One of the src agent is not in the distributed system")
      require(dst in this, "Ask method - One of the dst agent is not in the distributed system")
      // note that messages can be created on the fly so no 
      // guarantees the are in messages or not. We only have 
      // to make sure all "types" of messages are in "messages'

      require((m in this), "Ask method - Message (" + m + "is not known for this distributed system")

      val te = new TraceEntry
      if (isTracingEnabled) {
        te.prior = traceCopy
        scheduler.traceManager.current += te
      }

      /* ADDING temporaries support for the ASK pattern
       * 
       * STEPS:
       * 1- take note of: src.name and future.id
       * 	1.1- in order to make temp = src.name + "[]" + future.id
       * 	1.2- add it to "temporaries"
       * 2- Set the src of the "send()" to: new Agent(temp), and set its locked field to false.
       *  2.1- Also, make the promiser to be the original "dst", in the dst.promise() method
       *  2.2- Now, if the temp-agent receives a message from original "dst",
       *  			temp-agent has both the REAL asker name, and the id of the 
       *  			future to resolve.
       * 3- any one that replies to temp-agent, send() should be able to detect that, and do the
       * 		necessary of resolving the future by invoking the ds.resolve() on the appropriate future.
       * 4- things should proceed normally as they were before.
       * 
       * NOTE: note that now with the new implementation, we really don't need the 
       * 			"Agent.futuresPromised" any more. Things should be transparent from the promiser point 
       * 			of view.
       */

      m.sendMethod = true // It indicates ask (as opposed to send), I forgot why we need it, but will keep it to avoid bugs.
      m.sender = src
      val result = dst.promise(m) // step 2.1
      val temp = new Agent(src.name + "[]" + result.id) // step 1.1
      temporaries = temporaries + temp.name // step 1.2
      agents = agents + temp // send and ask checks if agents are there (temporary agents for futures)

//      temp.futuresWaitingFor += (result.id -> result)
      
      send(temp, m, dst) // step 2

      if (!result.resolved) {
        dst.futuresPromised += (result.id -> result)
        src.futuresWaitingFor = src.futuresWaitingFor + (result.id -> result)
      }

      if (isTracingEnabled) {
        te.posterior = traceCopy
        te.event = new Ask(src.traceCopy, m, dst.traceCopy, result, special)
        scheduler.traceManager.current += TraceEntryEnd(te)
      }
      result
    }

  }

  def resolve(df: DummyFuture, value: Any = None): Unit = {
    require(df.promisedBy != null, "Resolve method - future.promisedBy is null")
    require(df.waitingFor != null, "Resolve method - future.waitingFor is null")
    require(df != null, "Resolve method - future is null")

    //some one could wait on behalf of someone !
    //    require(agents.contains(df.promisedBy) && agents.contains(df.waitingFor), "Resolve method - One of the resolver/asker agents are not in the distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Resolve(df.traceCopy)
      scheduler.traceManager.current += te
    }

    //    df.resolve(df.promisedBy, value)
    // Inlining the above here
    df.value = value
    df.resolved = true
    df.promisedBy.futuresPromised -= df.id
    df.waitingFor.futuresWaitingFor -= df.id
    df.waitingFor.localState += (df.id.toString -> value) 
    df.waitingFor.blocked = false

    //    send(df.promisedBy, new ResolveDummyFuture(df), df.waitingFor)
    send(df.promisedBy, new ResolveDummyFuture(df), df.waitingFor)

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += new TraceEntryEnd
    }

  }

  def create(p: Agent, nameOfNewOne: String): Agent = {
    require(p != null && nameOfNewOne != null && nameOfNewOne != "", "Create method One of create paramaters is null or name is empty string")
    // no two agents have same id, name is id here.
    require(!agents.exists { x => x.name == nameOfNewOne }, "Create method - name conflicts with another existing agent's name")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
    }

    // FIXME
    /*
     * need to do:
     * - use copy.link for all objects inside the copy of the agent
     * - set new random UUID for ALL objects inside the copy of the agent (one function takes Agent, returns it again)
     */

    val agentCreated = new Agent(nameOfNewOne)
    agentCreated.q = Seq[Message]()
    agentCreated.defaultBehavior = p.defaultBehavior.traceCopy
    agentCreated.stash = Seq[Message]()
    agentCreated.reactions = p.defaultBehavior.traceCopy
    agentCreated.behaviors = p.behaviors.map {
      x =>
        val beh = x._2.traceCopy;
        (x._1 -> beh)
    }
    agentCreated.specialReactions = new Behavior("special")
    //    agentCreated.timedActions = Set[RuntimeTimedAction]()
    agentCreated.oldBehaviors = Stack[String]()
    agentCreated.name = nameOfNewOne
    agentCreated.scheduler = scheduler
    agentCreated.refresh

    agents + agentCreated

    if (isTracingEnabled) {
      te.event = new Create(p.traceCopy, nameOfNewOne, agentCreated.traceCopy)
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
    agentCreated
  }

  def start(starter: Agent, started: Agent): Unit = {

    require(starter != null && started != null, "Start method - One of start parameters is null")
    require((starter in this) && (started in this) || starter.name == bootStraper.name, "Start method - At least one of agents isn't present in the distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy

      starter.name match {
        case "boot" => te.event = new edu.utah.cs.gauss.ds2.core.tracing.Start(bootStraper.traceCopy, started.traceCopy)
        case _ => te.event = new edu.utah.cs.gauss.ds2.core.tracing.Start(starter.traceCopy, started.traceCopy)
      }

      scheduler.traceManager.current += te
    }

    // actual work done
    starter.name match {
      case "boot" => send(bootStraper, new Start, started, true)
      case _ => send(starter, new Start, started, true)
    }

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def stop(stopper: Agent, stopped: Agent): Unit = {

    // FIXME this is a messy method, the messafe once in stopped agant queue, scheduler will pick it, why the scheduling is still needed there?
    // HEATH: can you make sure why this is here? (thanks!)

    require(stopper != null && stopped != null, "Stop method - One of stop parameters is null")
    require((stopper in this) && (stopped in this), "Stop method - At least one of agents isn't present in the distributed system")
    stopped.synchronized {

      val te = new TraceEntry
      if (isTracingEnabled) {
        te.prior = traceCopy
        te.event = new edu.utah.cs.gauss.ds2.core.tracing.Stop(stopper.traceCopy, stopped.traceCopy)
        scheduler.traceManager.current += te
      }
      send(stopper, new Stop, stopped, true) // takes care of new references
      lock(stopped) // takes care of new references
      val ac = new Action
      ac.m = new Stop
      ac.a = stopped
      stopped.q map {
        msg =>
          val ac1 = stopped.reactions(msg).copy
          ac1.m = msg
          ac1.a = stopped
          scheduler.schedule(new SuspendableTask(stopped, ac1))
      }
      if (isTracingEnabled) {
        te.posterior = traceCopy
        scheduler.traceManager.current += TraceEntryEnd(te)
      }
    }

    // stay locked forever, or till a start message arrives again (i.e. this is resume)
  }

  def kill(killer: Agent, victim: Agent): Unit = {

    require(killer != null && victim != null, "Kill method - One of kill parameters is null")
    require((killer in this) && (victim in this), "Kill method - At least one of the agents to kill isn't present in the distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Kill(killer.traceCopy, victim.traceCopy)
    }
    // not the no synchronization and messages in-flight will be lost
    send(killer, new PoisonPill, victim)
    // now we wait till possibly bad things happen due to scheduler

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def lock(a: Agent): Unit = {

    require(a != null, "Lock method - agent to lock is null")
    require((a in this), "Lock method - agent to lock isn't present in distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Lock(a.traceCopy)
    }

    a.synchronized { a.locked = true }

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def unlock(a: Agent): Unit = {
    require(a != null, "Unlock method - agent to lock is null")
    require((a in this), "Unlock method - agent to lock isn't present in distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Unlock(a.traceCopy)
    }

    a.locked = false

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def stopConsuming(a: Agent): Unit = {
    require(a != null, "StopConsuming method - agent to lock is null")
    require((a in this), "StopConsuming method - agent to lock isn't present in distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new StopConsuming(a.traceCopy)
    }

    a.synchronized { a.consuming = false }

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)

    }
  }

  def resumeConsuming(a: Agent): Unit = {
    require(a != null, "ResumeConsuming method - agent to lock is null")
    require((a in this), "ResumeConsuming method - agent to lock isn't present in distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new ResumeConsuming(a.traceCopy)
    }

    a.synchronized { a.consuming = true }

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def become(a: Agent, b: String, remember: Boolean = false): Unit = {
    require(a != null && b != null, "Become method - One of become parameters is null")
    require(a.behaviors.contains(b), "Become method - Unknown behavior to become")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Become(a.traceCopy, b, remember)

      // this is added so that any thing after happens after, stop/resume consuming.
      scheduler.traceManager.current += te
    }

    // actual work done
    stopConsuming(a)
    if (remember)
      a.oldBehaviors.push(a.reactions.name) // remember old behavior
    a.reactions = a.behaviors(b)
    resumeConsuming(a)

    if (isTracingEnabled) {
      // this should still reflect in the trace entry added above
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def unbecome(a: Agent): Unit = {
    require(a.oldBehaviors != null, "Unbecome method - agent to unbecome is null")
    require((a in this), "Unbecome method - agent to unbecome doesn't exist in the distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Unbecome(a.traceCopy)
      scheduler.traceManager.current += te
    }

    // actual work done
    // here we don't care about synchronization, msgs can still flow in
    if (a.oldBehaviors.size == 0) {
      //      println("Unbecome and empty oldBehaviors, restoring to default behavior")
      stopConsuming(a) // takes care of new references
      a.reactions = a.defaultBehavior
      resumeConsuming(a) // takes care of new references
    }
    else {
      stopConsuming(a) // takes care of new references
      a.reactions = a.behaviors(a.oldBehaviors.pop)
      resumeConsuming(a) // takes care of new references
    }

    if (isTracingEnabled)
      te.posterior = traceCopy
    scheduler.traceManager.current += TraceEntryEnd(te)
  }

  def stash(a: Agent, m: Message): Unit = {
    require(a != null && m != null, "Stash method - one of parameters is null")
    require((a in this), "Stash method - agent to stash msg from doesn't exist in the distributed system")
    require(a.stash != null, "Stash method - stash is null!")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Stash(a.traceCopy, m)
    }
    // imagine if scheduler takes a message to consume by force ...
    //stopConsuming(a)
    a.stash = a.stash :+ m
    //resumeConsuming(a)

    /*
     *  I don't think stopping consumption is any problem.
     *  the scheduler will keep consuming no matter what, during
     *  executing of the action ,,, things will get stashed!
     *  
     *  It, however, matters when it comes to unstashing.
     */

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += te
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def unstash(a: Agent): Unit = {
    require(a != null && a.q != null, "Unstash method - agent and/or its queue is null")
    require((a in this), "Unstash method - agent to stash msg from doesn't exist in the distributed system")
    require(a.stash != null, "Unstash method - stash is null!")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Unstash(a.traceCopy)
      scheduler.traceManager.current += te
    }

    /*
     *  not necessary to lock it, incoming msgs will be added at the back
     *  in constrast to unstashing (to the front)
     */
    //lock(a) // lock the queue to prevent concurrent mods
    stopConsuming(a) // also stop consuming since we will unstash to front of q
    if (a.stash.size >= 1) {

      a.q.lastIndexWhere { x => x.isInstanceOf[ResolveDummyFuture] } match {
        case -1 => // base case
          a.q = (a.stash.head) +: a.q // prepend the first message on stash infront of q
          a.stash = a.stash.tail // remove the first element from stash
        case x => // system messages are in front of the queue
          val slice1 = a.q.slice(0, x)
          val slice2 = a.q.slice(x, a.q.size - 1)
          a.q = slice1 ++ Seq(a.stash.head) ++ slice2
          a.stash = a.stash.tail
      }

    }

    // else do nothing
    resumeConsuming(a)
    //unlock(a)

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def unstashAll(a: Agent): Unit = {
    require(a != null && a.q != null, "UnstashAll method - agent and/or its queue is null")
    require((a in this), "UnstashAll method - agent to stash msg from doesn't exist in the distributed system")
    require(a.stash != null, "UnstashAll method - stash is null!")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new Unstash(a.traceCopy)
      scheduler.traceManager.current += te
    }

    //lock(a)
    stopConsuming(a) // takes care of new references

    if (a.stash.size >= 1) {

      a.q.lastIndexWhere { x => x.isInstanceOf[ResolveDummyFuture] } match {
        case -1 => // base case
          a.q = a.stash ++ a.q
          a.stash = Seq[Message]()
        case x => // system messages are in front of the queue
          val slice1 = a.q.slice(0, x)
          val slice2 = a.q.slice(x, a.q.size - 1)
          a.q = slice1 ++ a.stash ++ slice2
          a.stash = Seq[Message]()
      }
    }

    resumeConsuming(a) // takes care of new references
    //unlock(a)

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  // thread safe
  def hasWork: Set[Agent] = {

    // QUESTION: should I or should I NOT log hasWork in the trace?
    // not much of requirements
    agents.filter { x => x.synchronized { x.q.size > 0 && x.consuming && (!x.blocked || x.q.head.isInstanceOf[ResolveDummyFuture]) } }
  }

  def bootStrap(a: Agent): Unit = {
    require(a != null, "BootStrap method - The agent to boot strap is null")
    require((a in this), "Bootstrap method - The agent to boot strap doesn't exist in the distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new BootStrap(a.traceCopy)
      scheduler.traceManager.current += te
    }

    start(bootStraper, a) // takes care of new references

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def bootStrap(agnts: Set[Agent]): Unit = {
    require(agnts != null, "BootStrap Many method - the set of agents to boot strap is null")
    require(agnts.size >= 1, "BootStrap Many method - the agents-set to boot strap doesn't have agents!")
    require(agnts.forall { x => (x in this) }, "BootStrap Many method - NOT all agents exist in the distributed system")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new BootStrapAll(agnts.map { x => x.traceCopy })
      scheduler.traceManager.current += te
    }

    // initialize them simultaneously as a transaction.
    agnts.synchronized {
      agnts.map { x => bootStrap(x) } // bootStrap(Agent) takes care of new references
    }

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  /**
   * Checks if the future is resolved then it returns its value.
   * Otherwise it returns <code>None</code>.
   *
   * There is NO return value of this method!! a question arrizes, where is the value
   * of that future go then?
   * Ans: in the agents local state mapping that future's UUID.toString to the future's value
   *
   * @param src the asking agent that is trying to get the value of the future.
   * @param f the future involved
   * @param act the action that is possible to be suspended because of this future being unresolved.
   *
   */
  def get(asker: Agent, f: DummyFuture, act: Action): Unit = {
    require(null != asker, "Blocking Get method - asker agent parameter is null!")
    require(null != f, "Blocking Get method - future parameter is null!")
    require(null != act, "Blocking Get method - action parameter is null!")
    require(asker.name == f.waitingFor.name, "Blocking Get method - the agent trying to get the future is not the same one that created it")
    require(asker.futuresWaitingFor.contains(f.id), "Blocking Get method - The src agent doesn't have the future in its futuresWaitingFor collection")
    require(f.promisedBy.futuresPromised.contains(f.id), "Blocking Get method - The dst agent says it doesn't know if it promised some other aget with a future like that.")

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      scheduler.traceManager.current += te
    }

    val result = scheduler.synchronized {
      if (f.resolved) {
        f.waitingFor.localState += (f.id.toString -> f.value) // FIXME remove this one later, nothing should be added this way to local-state
        scheduler.blockingMgr.unblock(f.waitingFor) // it reschedules whatever is unblocked
        Some(f.value)
      }
      else {
        /*
         * Steps:
         * 1- make a new task
         * 2- suspend it
         * 3- block it in the blockingMgr
         * 4- make the agent also blocked and on the future
         */
        asker.blocked = true // gets blocked in the StandardExecutionThread
        asker.blockedOn = f

        // suspending the task and the rest such as blocking the 
        // task will be taken care of in the standard execution
        // thread.

        None
      }
    }

    if (isTracingEnabled) {
      te.event = new Get(asker.traceCopy, f.traceCopy, act.traceCopy, result)
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  /**
   * Checks if the future is resolved, if not it blocks till <code>timeout</code> reaches ZERO.
   * Then, it reschedules resuming from the statement after the get.
   *
   * @param src The asking agent
   * @param f the future on which the agent may/not block
   * @param timeout the time out in terms of scheduler ticks.
   * @param act the action to be suspended, and later resumed when resolved/timeout
   * @return The value of the future if it has been resolved within the timeout, or
   * <code>None</code> if it didn't.
   */
  def get(asker: Agent, f: DummyFuture, timeout: BigInt, act: Action): Unit = {
    require(null != asker, "Timed Get method - asker agent is null")
    require(null != f, "Timed Get method - future is null")
    require(null != act, "Timed Get method - Action is null")
    require(timeout >= 0, "Timed Get method - timeout is negative?!!")
    require(asker.name == f.waitingFor.name, "Timed Get method - the agent trying to get the future is not the same one that created it")
    require(asker.futuresWaitingFor.contains(f.id), "Timed Get method - The src agent doesn't have the future in its futuresWaitingFor collection")
    require(f.promisedBy.futuresPromised.contains(f.id), "Timed Get method - The dst agent says it doesn't know if it promised some other aget with a future like that.")

    /*
     * STEPS:
     * 1- At the time of calling, check if resolved then return if yes.
     * 
     * 2- Otherwise, add the agent to blockingManager, and associate 
     * it with its currently running Action (to keep the PC state). And
     * schedule a timed action to timeout and resume from there.
     * 
     * 3- Either it will be unblocked by another agent meanwhile, resolving
     * the future.
     * 
     * 4- OR, the timed action YOU schedule to execute ONLY ONCE will time 
     * out and check again:
     * 
     * IF resolved:
     *   1. update the Agent's local state
     *   2. And mark the future as resolved
     *   3. trigger the action (which is the suspended action, because we 
     *   need to advance its current state - its PC)
     *   4- "Reschedule" the action in its current state using the same 
     *   message that triggered it (the action itself stores the message 
     *   and agent.
     * ELSE
     *   Keep blocked until another agent may resolve, or it will stay 
     *   blocked forever. In which case we report a deadlock.
     */

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      scheduler.traceManager.current += te
    }

    // STEP 1
    val result = scheduler.synchronized {
      if (f.resolved) {
        scheduler.synchronized {
          f.waitingFor.localState += (f.id.toString -> f.value)
          scheduler.blockingMgr.unblock(f.waitingFor) // it reschedules all unblocked
          Some(f.value)
        }
      } // STEP 2 + 3
      else {
        /*
         * Steps:
         * 1- make a timed action
         * 2- schedule it
         * 3- make the agent also blocked and on the future 
         *    (temporarily as the action triggers should 
         *    unblock and resume normal operation)
         */
        asker.blocked = true
        asker.blockedOn = f

        /*
         * NOTE:
         * Now this is tricky. If the action has been blocking on a timed-get-future call,
         * this means that even when it get resolved, a recall to that method is re-done because
         * the PC didn't advance. This is fine only when another agent resolves the future. Now 
         * the big question comes:
         * 
         * - What happens if it was not resolved by another future, yet the timed-get timed out?
         * 
         * - Answer: we "skip" that timed-get statement and nothing happens, we just continue
         * executing the remaining statements of the action!
         * 
         * - How to "skip" a statement?
         * - Call Action.advancePC method!
         */

        // because asker is blocked and we need someone to unblock asker
        var askerSurrogate = new Agent(asker.name + "-timed-surrogate")

        // this is the statement to be executed by the timed action (executes on timeout of the get-future)
        val code = (m: Message, a: Agent) => {
          if (!f.resolved) {
            act.advancePC // skip the timed-get
            asker.localState = asker.localState + (f.id.toString -> None)
          }
          else {
            // update the agent's local state with the value of the future
            asker.localState = asker.localState + (f.id.toString -> f.value)
          }
          asker.blocked = false
          scheduler.blockingMgr.unblock(asker) // reschedules the blocked tasks
        }
        val newAction = new Action

        newAction + Statement(code)
        newAction.setAgent(askerSurrogate) // TODO what happens if this isn't in current DS copy timedActionsTracker?!
        newAction.setMessage(act.m)
        newAction.reset // has to reset the newAction.toExecute to newAction.stmts, otherwise no code to execute!

        val timedAction = new RuntimeTimedAction(timeout, timeout, newAction, askerSurrogate, 1, scheduler.clock())
        // when the timed action happens, it has to execute the stmt above
        scheduler.schedulePeriodic(timedAction)
        None
      }
    }

    if (isTracingEnabled) {
      te.event = new GetTimeout(asker.traceCopy, f.traceCopy, timeout, act.traceCopy, result)
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def stopSystem: Unit = {
    ???

    /*
     * This is a research task! (corner case worth research) (and we have ActionsToSends which will simplify it)
     * 
     * How can we guarantee an orderly termination that ACTUALLY terminates?!!...
     * 
     * Imagine this: if the currently remaining tasks to execute keep sending and generating messages,
     * then all receiving agents will either one of the following:
     * - not able to receive them if their queues are locked
     * - keep processing and generating more tasks that may, in turn, generate more messages flying around, if their 
     *   queues are not locked. 
     *   
     * How to prove termination of a distributed system so that we say: ok, the distributed system is stopped in an orderly manner? 
     */

    /*
     * GANESH: find any algorithm to implement here, do not do any termination things. it is difficult. 
     */
  }

  def shutdownSystem: Unit = {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new ShutdownAll
    }

    agents = Set()
    messages = Set()
    //    actions = Set()
    //    behaviors = Map()
    //    actionToMessageSent = new ActionsToSends
    scheduler.shutdown

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }
  }

  def isShutdown: Boolean = {
    agents.isEmpty &&
      messages.isEmpty &&
      //      actions.isEmpty &&
      //      behaviors.isEmpty &&
      //      actionToMessageSent.a2s.isEmpty &&
      scheduler.isShudown
  }
  //=======================================
  // Utility functions
  //=======================================

  def refresh: Unit = {
    agents.map { x => x.scheduler = scheduler; x.refresh }
  }

  def contains(agentName: String): Boolean = {
    agents.find { x => x.name == agentName } != None
  }

  /**
   * Adds an agent to the distributed system and populates all related fields of
   * the distributed system by the respective Agent's data.
   * @param a
   */
  def +(a: Agent): DistributedSystem = {

    val te = new TraceEntry
    if (isTracingEnabled) {
      te.prior = traceCopy
      te.event = new AddAgent(a.traceCopy)
      scheduler.traceManager.current += te
    }

    // add it to agents
    agents = agents + a
    // default behavior
    //    behaviors += (a.name + ".default" -> a.defaultBehavior)

    // update the behaviors set 
    //    a.behaviors.map { x => if (!behaviors.contains(x._1)) behaviors += (x._1 -> x._2) }

    // Messages
    a.reactions.reactions.map { x => messages += x._1 }
    a.defaultBehavior.reactions.map { x => messages += x._1 }
    a.behaviors.map { x => x._2.reactions.map { y => messages += y._1 } }
    // special reactions messages are identical for all agents, no need to add.

    // Actions
    //    a.reactions.reactions.map { x => actions + x._2 }
    //    a.defaultBehavior.reactions.map { x => actions + x._2 }
    //    a.behaviors.map { x => x._2.reactions.map { y => actions += y._2 } }
    // special actions however, change from one agent to another.
    //    a.specialReactions.reactions.map { x => actions + x._2 }

    // also we need to link all statements of agent's behaviors to this DS 
    //      for{
    //        b <- behaviors
    //        a <- b._2.reactions.values
    //        s <- a.stmts
    //      }{
    //        s.ds = this
    //      }
    a.link(this)

    // all remaining state we are NOT supposed to mess up with

    if (isTracingEnabled) {
      te.posterior = traceCopy
      scheduler.traceManager.current += TraceEntryEnd(te)
    }

    this
  }

  def get(agentName: String): Agent = {
    val a = agents.find { x => x.name == agentName }
    if (a == None)
      throw new Error("There is no agent with the specified name in this distributed system")
    else
      a.get
  }

  //  /**
  //   * used to pass scheduler state from this distributed system cop() to 
  //   * another distributed system link(). So that the scheduler state is 
  //   * linked properly to the new distributed system.
  //   */
  //  private var schedulerState: SchedulerState = _

  def copy: DistributedSystem = {

    val ds = new DistributedSystem(name)

    ds.agents = agents map { x => x.copy }

    ds.messages = messages map { x => x.copy }

    // TODO make sure this doesn't lead to data races on temporaries
    temporaries map { x => ds.temporaries = ds.temporaries + x }

    ds.tracingEnabled = if (tracingEnabled) true else false

    //    ds.actions = actions map { x => x.copy }

    //    ds.behaviors = behaviors map { x => (x._1 -> x._2.copy) }

    // scheduler is ONLY referenced, not copied.
    ds.scheduler = scheduler

    ds
  }

  /**
   * This is to be used ONLY from inside the copy() method of the distributed system.
   *
   * This isn't a user level method, so any use of it is out to be wrong.
   */
  def link: Unit = {
    /*
     * Things to do:
     * - refresh agents
     * - refresh Actions
     * - refresh Messages
     * - refresh behaviors
     * 
     */

    // agents
    agents map { x => x.link(this) }

    // Actions
    //    actions map { x => x.link(this) }

    // Messages
    messages map { x => x.link(this) }

    // Behaviors (behavior names are copied already)
    //    behaviors map { x => x._2.link(this) }

  }

  /**
   * A deep copy using json utilities.
   * @return a copy of the calling DistributedSystem object.
   */
  def traceCopy: DistributedSystem = {
    DistributedSystem.fromJson(toJson)
  }

  def is(that: DistributedSystem): Boolean = {
    name == that.name
  }

  override def toString: String = {
    "DistributedSystem '" + name + "': \n===============\n" + agents.toString + "\n" +
      "Scheduler: \n===============\n" + scheduler
  }
  //=======================================
  // Tracing
  //=======================================
  def toJson: JValue = {
    this.synchronized {
      (getClass.getSimpleName ->
        ("name" -> name) ~
        ("agents" -> agents.map { x => x.toJson }) ~
        ("messages" -> messages.map { x => x.toJson }) //        ("actions" -> actions.map { x => x.toJson }) ~
        //        ("behaviors" -> behaviors.map { x => // x._1 is behavior name, x._2 Behavior object 
        //          (x._1 -> x._2.toJson)
        //        })
        )
      //        ~
      //        ("scheduler" -> scheduler.toJson))
    }
  }

  def enableTracing: Unit = {
    this.synchronized {
      tracingEnabled = true
    }
  }

  def disableTracing: Unit = {
    this.synchronized {
      tracingEnabled = false
    }
  }

  def isTracingEnabled: Boolean = {
    this.synchronized {
      tracingEnabled
    }
  }
}

object DistributedSystem {
  def fromJson(js: JValue): DistributedSystem = {
    implicit val formats = DefaultFormats
    //        js.extract[DistributedSystem]

    val name = js \ "DistributedSystem" \ "name" match {
      case JString(x) => x
      case _ => throw new Error("DistributedSystem.fromJson - can't extract 'name'")
    }

    val agents = js \ "DistributedSystem" \ "agents" match {
      case JArray(x) => x map { z => Agent.fromJson(z) }
      case _ => throw new Error("DistributedSystem.fromJson - can't extract 'agents'")
    }

    val messages = js \ "DistributedSystem" \ "messages" match {
      case JArray(x) => x map { z => Message.fromJson(z) }
      case _ => throw new Error("DistributedSystem.fromJson - can't extract 'messages'")
    }

    //    val actions = js \ "DistributedSystem" \ "actions" match {
    //      case JArray(x) => x map { z => Action.fromJson(z) }
    //      case _ => throw new Error("DistributedSystem.fromJson - can't extract 'actions'")
    //    }
    //
    //    var listOfBehaviorsPairs = Seq[JValue]()
    //    js \ "DistributedSystem" \ "behaviors" match {
    //      case JObject(x) => x map {
    //        y =>
    //          listOfBehaviorsPairs = listOfBehaviorsPairs :+ y
    //      }
    //      case _ =>
    //        println(js \ "DistributedSystem" \ "behaviors")
    //        throw new Error("DistributedSystem.fromJson - can't extract (name,Behavior) pairs")
    //    }
    //
    //    var behaviors = Map[String, Behavior]()
    //    listOfBehaviorsPairs map { x =>
    //      x match {
    //        case JField(y, z) =>
    //          behaviors += (y -> Behavior.fromJson(z))
    //        case _ => throw new Error("Behavior.fromJson - What a disaster! couldn't match behavior pair!")
    //      }
    //    }

    // Why are we not (de)serializing scheduler here (instead we actually serialize DS from scheduler)?
    // Answer: scheduler and Traces have instances of DS, so that would create infinite loop to serialize
    // starting from DS (you know, DS.fromJson{ ... scheduler.fromJson {... traceManager.toJson}}) and then
    // inside the traceManager.toJson, there are couple of traces, whose each TraceEntry has two instances 
    // of DS (prior and posterior). Calling prior.toJson and/or posterior.toJson, will lead to calling 
    // DS.toJson again!

    //    var scheduler = js \ "DistributedSystem" \ "scheduler" match{
    //      case x:JObject => Scheduler.fromJson(x)
    //      case _         => throw new Error("DistributedSystem.fromJson - can't extract 'scheduler'")
    //    }
    //    

    val ds = new DistributedSystem(name)

    // populate the DS fields from extracted js values
    ds.agents = agents.toSet
    //    ds.actions = actions.toSet
    ds.messages = messages.toSet
    //    ds.behaviors = behaviors

    ds
  }
}

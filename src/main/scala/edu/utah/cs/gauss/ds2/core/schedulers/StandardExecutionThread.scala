package edu.utah.cs.gauss.ds2.core.schedulers
import edu.utah.cs.gauss.ds2.core.ir.datastructures._
import edu.utah.cs.gauss.ds2.core.ir.datastructures.Statement.Kind._

/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 *
 * The StandardExecutionThread factory.
 *
 * Used to create instances of {@link StandardExecutionThread}.
 */

object StandardExecutionThread {
  def apply(task: SuspendableTask, threadID: Long)(implicit scheduler: Scheduler): StandardExecutionThread = {
    val t = new StandardExecutionThread(task, threadID)
    t.scheduler = scheduler
    t
  }
}

/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 * This thread implementation is used to execute a whole action on a message+agent pair
 */
class StandardExecutionThread(task: SuspendableTask, threadId: Long) extends Thread {

  var scheduler: Scheduler = _

  override def getId(): Long = {
    threadId
  }

  override def run(): Unit = {

    this.synchronized {
      task.synchronized {
        val a = task.a
        val m = task.action.m
        var action: Action = task.action
        while (action.hasMore && (!a.blocked || task.isTimed)) {
          //          if(action.toExecute.head.kind == GET || action.toExecute.head.kind == GET_TIMED)
          action.toExecute.head.action = action
          action.executeOne
          scheduler.tick // tick with each statement executed          
        }

        if (a.blocked && !m.isInstanceOf[ResolveDummyFuture]) {
          if (!action.executed.isEmpty) // at least one statement has to execute to block the agent!
            action.executed.last.action = action // makes sense for instrumented GET/GET_TIMED Statements.
          task.suspend
          scheduler.blockingMgr.block(task)
        }

        if (m.isInstanceOf[ResolveDummyFuture]) {
          scheduler.blockingMgr.unblock(a) // this will scheduler whatever is unblocked
          a.blockedOn = null
        }
      }
    }
  }

  override def toString: String = {
    "On Thread - " + threadId + ": agent '" + task.a.name + "' processing message '" + task.action.m + "'"
  }
}

/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 *
 * Used to create instances of {@link StatementExecutionThread}.
 *
 */
object StatementExecutionThread {
  def apply(task: SuspendableTask, threadID: Long)(implicit scheduler: Scheduler): StandardExecutionThread = {
    val t = new StatementExecutionThread(task, threadID)
    t.scheduler = scheduler
    t
  }
}

/**
 * @author <br>
 * Mohammed S. Al-Mahfoudh <br/>
 * mahfoudh@cs.utah.edu <br/>
 * SoC - Gauss Group <br/>
 *
 * This thread implementation is used to be executed as many times as there are statements in an action
 * associated with the task to be executed. This is an analysis thread for stepping through statements.
 *
 * Important to note that the SAME thread instance MUST be used several times till exhaustion of the action's
 * statements. Multiple instances will cause semantic errors and wrong analyses.
 *
 * That is, use the same instance and call run() as many times as there are statements in the action.
 */
class StatementExecutionThread(task: SuspendableTask, threadID: Long) extends StandardExecutionThread(task, threadID) {
  val a = task.a
  val action = task.action
  override def run(): Unit = {
    // this is necessary since a statement can mutate the agent's internal state.
    // mean while
    task.synchronized {
      if (action.hasMore && !task.isSuspended && !task.a.blocked) {
        // apply only one statement
        action.executeOne

        // since we are using statement execution thread, 
        // this means we ARE concerned about interleaving 
        // due to blocking on a future. so we tick anyways.
        scheduler.tick
      }

      if (task.isSuspended && task.a.blocked)
        scheduler.blockingMgr.block(task)
    }
  }

  override def toString(): String = {
    "On Thread - " + getId() + ": agent (" + task.a.name + ") executing statement ( " + action.toExecute.head + "), while processing message ( " + action.m + " )"
  }

}

///**
// * @author <br>
// * 	Mohammed S. Al-Mahfoudh <br/>
// * 	mahfoudh@cs.utah.edu <br/>
// * 	SoC - Gauss Group <br/>
// *
// * The companion object use to create TimedActionExecutionThread's.
// *
// */
//object TimedActionExecutionThread {
//  def apply(ta: RuntimeTimedAction, threadID: Long): TimedActionExecutionThread = {
//    val t = new TimedActionExecutionThread(threadID)
//    t.action = ta.action
//    t
//  }
//}
//
///**
// * @author <br>
// * 	Mohammed S. Al-Mahfoudh <br/>
// * 	mahfoudh@cs.utah.edu <br/>
// * 	SoC - Gauss Group <br/>
// *
// * A thread implementation that specializes in executing TimedActions
// */
//class TimedActionExecutionThread(threadID: Long) extends StandardExecutionThread(null, threadID) {
//  var action: Action = _
//
//  override def run(): Unit = {
//    action.execute
//    // no scheduler.tick() here as it will generate an infinite loop
//  }
//
//  override def toString: String = {
//    "On Thread - " + getId() + ": timed action (" + action + ") was triggered"
//  }
//
//}
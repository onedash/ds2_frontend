package edu.utah.cs.gauss.ds2.core.ir.datastructures

import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import edu.utah.cs.gauss.ds2.core.schedulers.BlockedTasksManager

/**
 * @author <br>
 * 	Mohammed S. Al-Mahfoudh <br/>
 * 	mahfoudh@cs.utah.edu <br/>
 * 	SoC - Gauss Group <br/>
 * This class, from its name, represents any agent with a task waiting
 * for a future to be resolved. Waiting is blocking, and blocking means
 * it has to be suspended till the future it is waiting it resolves, or
 * it will deadlock for ever.
 *
 */

@SerialVersionUID(1000)
class SuspendableTask(var a: Agent, var action: Action) extends Serializable {

  private var suspended = false
  var isTimed = false

  var agentName = if (null == a) null else a.name

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {
    val aHash = if (a == null) 0 else a.hashCode
    val agentNameHash = if (agentName == null) 0 else agentName.hashCode

    aHash +
      //    action.hashCode +
      suspended.hashCode +
      isTimed.hashCode +
      agentNameHash
  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }
  //==============================

  def suspend: Unit = {
    this.synchronized {
      suspended = true
    }
  }

  def isSuspended: Boolean = {
    this.synchronized {
      suspended
    }
  }

  def resume: Unit = {
    this.synchronized {
      suspended = false
      a.blocked = false
    }
  }

  def copy: SuspendableTask = {
    // action has to be copied (has runtime state that changes)
    // agent, we can't copy, since it encloses actions (otherwise infinite cycles)

    val task = new SuspendableTask(a, action.copy)
    // copied (changes in runtime)
    task.suspended = if (suspended) true else false
    task.isTimed = if (isTimed) true else false
    task.agentName = agentName

    task
  }

  def link(ds: DistributedSystem): Unit = {
    /*
     * disconnects the old agent 
     * "reference" and replaces it with the new copy of the fresh 
     * distributed system copy
     * 
     */

    a =
      if (null != a && ds.contains(a.name)) ds.get(a.name)
      else if (null != agentName && ds.contains(agentName)) ds.get(agentName)
      else null

    // then calls the action's link to do the rest of the job on it.
    action.link(ds)
  }

  def toJson: JValue = {
    this.synchronized {

      agentName = if (null == a && agentName == null) null
      else if (null == a && agentName != null) agentName
      else a.name

      (getClass.getSimpleName ->
        ("a" -> JNothing) ~
        ("action" -> action.toJson) ~
        ("suspended" -> suspended) ~
        ("isTimed" -> isTimed) ~
        ("agentName" -> agentName))
    }
  }

  def traceCopy: SuspendableTask = {
    SuspendableTask.fromJson(toJson)
  }

  def in(tasks: Set[SuspendableTask]): Boolean = {
    require(tasks != null, "SuspendableTask.in(Set[tasks]) method - doesn't accept null arguments")
    tasks.contains(this)
  }
  def in(tasks: Seq[SuspendableTask]): Boolean = {
    require(tasks != null, "SuspendableTask.in(Seq[tasks]) method - doesn't accept null arguments")
    tasks.contains(this)
  }

  def in(blockedMgr: BlockedTasksManager): Boolean = {
    require(blockedMgr != null, "SuspendableTask.in(BlockedTasksManager) method - doesn't accept null arguments")
    blockedMgr.blockedTasks.contains(this)
  }

  def is(that: SuspendableTask): Boolean = {
    val aCondition = if (null == a) null == that.a else a == that.a
    val actCondition = if (null == action) null == that.action else action == that.action

    aCondition && actCondition
  }

  override def toString: String = {
    val act = if (null == action) null else action

    agentName = if (null == a && agentName == null) null
    else if (null == a && agentName != null) agentName
    else a.name

    "Task: from '" + agentName + "' with action = " + act
  }
}

object SuspendableTask {
  def fromJson(js: JValue): SuspendableTask = {
    implicit val formats = DefaultFormats
    //    js.extract[SuspendableTask]
    val a: Agent = null
    val action = js \ "SuspendableTask" \ "action" match {
      case x: JObject => Action.fromJson(x)
      case _          => throw new Error("SuspendableTask.fromJson - can't extract 'action'")
    }
    val agentName = js \ "SuspendableTask" \ "agentName" match {
      case JString(x) => x
      case _          => throw new Error("SuspendableTask.fromJson - can't extract 'agentName'")
    }
    val suspended = js \ "SuspendableTask" \ "suspended" match {
      case JBool(x) => x
      case _        => throw new Error("SuspendableTask.fromJson - can't extract 'suspended'")
    }

    val isTimed = js \ "SuspendableTask" \ "isTimed" match {
      case JBool(x) => x
      case _        => throw new Error("SuspendableTask.fromJson - can't extract 'isTimed'")
    }

    val task = new SuspendableTask(a, action)
    task.agentName = agentName
    task.suspended = suspended
    task.isTimed = isTimed

    task
  }
}

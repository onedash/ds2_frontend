package edu.utah.cs.gauss.ds2.core.ir.datastructures
import net.liftweb.json._
import net.liftweb.json.JsonDSL._
import java.util.UUID

/**
 * @author <br>
 * 	Mohammed S. Al-Mahfoudh <br/>
 * 	mahfoudh@cs.utah.edu <br/>
 * 	SoC - Gauss Group <br/>
 *
 */

@SerialVersionUID(400)
class Behavior(var name: String) extends Serializable {

  var reactions = Map[Message, Action]()
  var a: Agent = _
  var agentName: String = if (null == a) null else a.name

  //==============================
  // Equality handling
  //==============================
  override def hashCode: Int = {

    val agntName = if (null != a) a.name
    else if (agentName != null) agentName; // do nothing
    else null

    val agentNameHash = if (null == agntName) 0 else agntName.hashCode

    name.hashCode +
      reactions.keys.hashCode +
      //      reactions.values.hashCode + // this makes it fail why?!!
      agentNameHash
    //    aHash +

  }

  override def equals(that: Any): Boolean = {
    hashCode == that.hashCode
  }

  //==============================

  def apply(m: Message): Action = {
    val found = reactions.find(p => p._1.name == m.name)
    if (found == None)
      throw new Error("Message not found in behavior")

    val pair = found.get
    pair._2.runtimeCopy
    //    reactions(m).copy
  }

  def toJson: JValue = {
    val agntName = if (null != a) a.name
    else if (agentName != null) agentName; // do nothing
    else null
    //      reactions.synchronized {
    (getClass.getSimpleName ->
      ("name" -> name) ~
      ("reactions" -> reactions.map { x => ("message" -> x._1.toJson) ~ ("action" -> x._2.toJson) }) ~
      ("a" -> null) ~
      ("agentName" -> agntName))
    //      }
  }

  // ===============================
  //  Utility
  // ===============================

  override def toString: String = {

    "Behavior '" + name + "': \n" + reactions.mkString("(", ",", ")")

  }

  def is(that: Behavior): Boolean = {
    name == that.name
  }

  def !=(that: Behavior): Boolean = {
    !(this == that)
  }

  def +=(reaction: (Message, Action)): Behavior = {

    // old version (over writing the sequence)
//    reactions = reactions + (reaction._1 -> reaction._2)
    
    
    
    // new version, appending to the sequence in case the key exists)
    val msg = reaction._1
    val act = reaction._2
    
    if(reactions.contains(msg))
    {
      val oldAction = reactions(msg)
      oldAction.stmts = oldAction.stmts ++ act.stmts // added the stmts to the old action, retaining oldAction id, a, m ... etc
    }
    else
      reactions = reactions + (msg -> act)
    
    this
  }
  
  def +(m:Message,st:Statement): Behavior = {
    if(reactions.contains(m))
    {
      val oldAction = reactions(m)
      val newAct = oldAction + st
      reactions = reactions + (m -> newAct)
    }
    else
    {
      val newAct = new Action
      newAct + st
    	reactions = reactions + (m -> newAct)      
    }
    
    this
  }
  
//  def +=(m:Message , st:Statement): Behavior = 
//  {
//  
//    if(reactions.contains(m))
//    {
//      val oldAction = reactions(m)
//      val newAct = oldAction + st
//      reactions = reactions + (m -> newAct)
//    }
//    else
//    {
//      val newAct = new Action
//      newAct + st
//    	reactions = reactions + (m -> newAct)      
//    }
//    
//    this
//  }
  
  
  def -=(msg: Message): Behavior = {
    reactions.synchronized { reactions - msg }
    this
  }

  def in(behaviors: Set[Behavior]): Boolean = {
    require(behaviors != null, "Behavior.in() method - doesn't accept null arguments")
    behaviors.find { x => x.name == name } != None
  }

//  def in(ds: DistributedSystem): Boolean = {
//    require(ds != null, "Behavior.in(ds) method - doesn't accept null arguments")
//    ds.behaviors.values.find { x => x.name == name } != None
//  }

  def setAgent(a: Agent): Unit = {
    reactions.map { x => x._2.setAgent(a) }
  }

  def resetActions: Unit = {
    val actions = reactions.values.map { x => x.reset }
  }

  def copy: Behavior = {

    //    val agntName = if (null != a) a.name
    //    else if (agentName != null) agentName; // do nothing
    //    else null

    val copy = new Behavior(name)

    // NOTE: i changed x._2 (the action) to x._2.copy (actions do 
    // change in a TINY way from one agent to another, the owner 
    // agent changes by setAgent(). 
    copy.reactions = reactions map { x => (x._1.copy -> x._2.copy) }
    copy.a = a //Behavior is not responsible about copying it
    copy.agentName = agentName
    copy
  }

  def link(ds: DistributedSystem): Unit = {

    /*
     * disconnects the old agent 
     * "reference" and replaces it with the new copy of the fresh 
     * distributed system copy
     * 
     */
    a =
      if (null != a && ds.contains(a.name)) ds.get(a.name)
      else if (null != agentName && ds.contains(agentName)) ds.get(agentName)
      else null

    reactions map { x => x._1.link(ds);x._2.link(ds) }
  }

  def traceCopy: Behavior = {
    Behavior.fromJson(toJson)
  }

}

object Behavior {
  def fromJson(js: JValue): Behavior = {
    implicit val formats = DefaultFormats
    //    js.extract[Behavior]
    val name = js \ "Behavior" \ "name" match {
      case JString(x) => x
      case _          => throw new Error("Behavior.fromJson - can't extract 'name'")
    }

    val b = new Behavior(name)

    b.a = null

    js \ "Behavior" \ "agentName" match {
      case JString(x) => b.agentName = x
      case JNothing   => b.agentName = null
      case _          => throw new Error("Behavior.fromJson - can't extract 'agentName'")
    }

    val jsReactions = js \ "Behavior" \ "reactions"
    // DEBUG
    //      println("\n\n"+ pretty(render((jsReactions \ "message")(0))) +"\n\n")
    //    println("\n\n" + jsReactions + "\n\n")

    var listOfReactionPairs = Seq[JValue]()
    jsReactions match {
      case JArray(x) => x map {
        y => listOfReactionPairs = listOfReactionPairs :+ y
        // DEBUG
        //        x => println(x + "\n\n")
      }
      case _ => throw new Error("Behavior.fromJson - can't extract reaction pairs")
    }

    //    if (listOfReactionPairs.isEmpty)
    //      throw new Error("HOW?!")

    listOfReactionPairs map { x =>
      x match {
        case JObject(List(JField("message", y), JField("action", z))) =>
          //        println("MSG = "+x)
          //        println("ACT = "+y)
          b += (Message.fromJson(y), Action.fromJson(z))
        case _ => throw new Error("Behavior.fromJson - What a disaster! couldn't match reaction pair!")
      }
    }

    b
  }
}
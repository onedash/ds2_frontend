organization := "edu.utah.cs.gauss"


name := "DS2-FrontEnd"


version := "0.1.0-beta"


scalaVersion := "2.11.7"


mainClass in Compile := Some("edu.utah.cs.gauss.ds2.core.frontend.akka.ParsingTest")
exportJars := true

test in assembly := {}


//scaladoc

scalacOptions in (Compile,doc) ++= Seq("-groups", "-implicits")



// Dependencies

//libraryDependencies += "net.liftweb" % "lift-json_2.11" % "3.0-M6"



libraryDependencies += "org.scala-lang" % "scala-reflect" % "2.11.7"



// graph for scala

libraryDependencies += "com.assembla.scala-incubator" % "graph-core_2.11" % "1.9.4"



//json (net.liftweb.json)

libraryDependencies += "com.assembla.scala-incubator" % "graph-json_2.11" % "1.9.2"



// dot (graph4s to dot)

libraryDependencies += "com.assembla.scala-incubator" % "graph-dot_2.11" % "1.10.0"



libraryDependencies += "org.ow2.asm" % "asm-all" % "5.0.4"



libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.5"



libraryDependencies += "org.scalacheck" % "scalacheck_2.11" % "1.12.4"



// Use this neat library to parse command line options, best library I used

libraryDependencies += "commons-cli" % "commons-cli" % "1.3.1"



// Bokeh (charting from scala, binding from python's bokeh library)

libraryDependencies += "io.continuum.bokeh" %% "bokeh" % "0.6"
